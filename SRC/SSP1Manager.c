//******************************************************************************
//
//  SSP1Manager.c:
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-07-12      JK     Initial Implementation
//
//******************************************************************************


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "target.h"
#include "typedefs.h"
#include "SSP1Manager.h"
#include "HardwareAbstraction.h"
#include "SSP.h"


//******************************************************************************
//
//******************************************************************************

#define SSP1_SSP_LOOPBACK_EN   FALSE
#define SSP1_SSP_IS_MASTER     TRUE
#define SSP1_SSP_SLAVE_DISABLE FALSE
#define SSP1_SPI_FRAME_SIZE    16



//******************************************************************************
//
//******************************************************************************
void SSP1Init( void )
{

    BOOL  bInitPass;
    BYTE  byPinEnables;
    DWORD dwIntEnable;

    // set the SPI chip select for the DDS modulator itself
    PIN_SET( DDS_FSYNC );

    // set the SPI chip select for the modulator DC offset DAC
    PIN_SET( LD_SYNCN );

    // set the gain control element (a volume controller) chip select
    PIN_SET( VOL_CSN );



    byPinEnables = SSP_SCK_EN_BIT | SSP_MOSI_EN_BIT; //  | SSP_SSEL_EN_BIT
    dwIntEnable = 0;


    bInitPass =  SSPInit(  1,
                           SSP1_SPI_FRAME_SIZE,
	                       SSP_FORMAT_SPI,
	                       SSP_CPOL_INTERFRAME_HIGH,
	                       SSP_CPHA_1ST_EDGE,
	                       SSP1_SSP_LOOPBACK_EN,
	                       SSP1_SSP_IS_MASTER,
	                       SSP1_SSP_SLAVE_DISABLE,
	                       dwIntEnable,
	                       SSP1_FREQUENCY,
	                       byPinEnables,
	                       SSP1_FREQUENCY,
	                       NULL );

    if( bInitPass )
	    SSPEnable( 1 );

}


//******************************************************************************
//
//******************************************************************************
void SSPPort1Write( WORD wData, BYTE byTarget )
{

	// DDS Modulator notes:
	// The SCLK can be continuous, or it can idle high or low between write operations.
    // In either case, it MUST be high when FSYNC goes low (t12).



    // adjust the SSP port parameters for this specific target
    // and, assert the chip select for the appropriate target
	switch( byTarget )
    {
        case SSP1_TARGET_MODULATOR_GAIN_CNTRL:
        	SSPDisable(SSP1_PORTNUM);
            SSPSetClockPolarity(  SSP1_PORTNUM, SSP_CPOL_INTERFRAME_LOW );
        	SSPEnable(SSP1_PORTNUM);
        	// SSPSetClockPhase(  SSP1_PORTNUM, SSP_CPHA_1ST_EDGE );
            inlineDelay(20);
        	// assert chip select for the gain volume control
        	PIN_CLR( VOL_CSN );

            break;

        case SSP1_TARGET_MODULATOR_DDS:
        	SSPDisable(SSP1_PORTNUM);
            SSPSetClockPolarity(  SSP1_PORTNUM, SSP_CPOL_INTERFRAME_HIGH );
        	SSPEnable(SSP1_PORTNUM);
        	// SSPSetClockPhase(  SSP1_PORTNUM, SSP_CPHA_1ST_EDGE );
            inlineDelay(20);
        	// assert chip select for the DDS
        	PIN_CLR( DDS_FSYNC );
        	break;

        case SSP1_TARGET_MODULATOR_DC_OFFSET:
        	SSPDisable(SSP1_PORTNUM);
            SSPSetClockPolarity(  SSP1_PORTNUM, SSP_CPOL_INTERFRAME_LOW );
        	SSPEnable(SSP1_PORTNUM);
        	// SSPSetClockPhase(  SSP1_PORTNUM, SSP_CPHA_1ST_EDGE );
            inlineDelay(20);
        	// assert chip select for the gain volume control
        	PIN_CLR( LD_SYNCN );
        	break;

        default:
          break;
    }


    inlineDelay(200);
    // write the data to the port
	SSPWriteData( SSP1_PORTNUM, wData );

	// NOTE!!! the chip select is not managed by the peripheral - therefore,
    // we need a delay to ensure the peripheral gets its data out before
    // we de-assert the chip select
    inlineDelay(200);

    // de-assert the chip select for the appropriate target
	switch( byTarget )
    {

        case SSP1_TARGET_MODULATOR_GAIN_CNTRL:
        	// de-assert chip select for the gain volume control
            PIN_SET( VOL_CSN );
            break;

        case SSP1_TARGET_MODULATOR_DDS:
        	// de-assert chip select for the dds
        	PIN_SET( DDS_FSYNC );

            break;

        case SSP1_TARGET_MODULATOR_DC_OFFSET:
        	// de-assert chip select for the dds
        	PIN_SET( LD_SYNCN );
            break;

    }


}



