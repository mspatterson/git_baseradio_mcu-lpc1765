//******************************************************************************
//
//  UART.c: LPC UART Access and control module
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  Provides setup and control of the LPC UART serial ports.  Currently the
//  following features are not supported - DMA, Autobaud, IrDA and Baud Rate
//  divisor calculations using the fractional divider register.
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2011-05-03      John     Initial Implementation 
//
//******************************************************************************


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "UART.h"
#include "HardwareAbstraction.h"


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------

// default for pclk is sysclk/4
#define ENABLE_UART0_POWER      LPC_SC->PCONP |= (1<<3)
#define ENABLE_UART1_POWER      LPC_SC->PCONP |= (1<<4)
#define ENABLE_UART2_POWER      LPC_SC->PCONP |= (1<<24)
#define ENABLE_UART3_POWER      LPC_SC->PCONP |= (1<<25)
#define ENABLE_UART4_POWER      LPC_SC->PCONP |= (1<<8)


// these I/O mappings need to be re-arranged; they have changed  for the
// 177x/178x implementation

// #if (!(TARGET == LPC177x_78x ) )

     // pin select macros, modify to select pins used for application
     // pin select P0.2, function 01 for TXD0
     #define ENABLE_UART0_TXD        LPC_PINCON->PINSEL0 &= ~(1<<5); LPC_PINCON->PINSEL0 |= (1<<4)
     // pin select P0.3, function 01 for RXD0
     #define ENABLE_UART0_RXD        LPC_PINCON->PINSEL0 &= ~(1<<7); LPC_PINCON->PINSEL0 |= (1<<6)
//     // pin select P2.0, function 10 for TXD1
//     #define ENABLE_UART1_TXD        LPC_PINCON->PINSEL4 |= (1<<1); LPC_PINCON->PINSEL4 &= ~(1<<0)
	// pin select P1.15, function 01 for TXD1
     #define ENABLE_UART1_TXD        LPC_PINCON->PINSEL0 |= (1<<30); LPC_PINCON->PINSEL0 &= ~(1<<31)
//     // pin select P2.1, function 10 for RXD1
//     #define ENABLE_UART1_RXD        LPC_PINCON->PINSEL4 |= (1<<3); LPC_PINCON->PINSEL4 &= ~(1<<2)
     // pin select P0.16, function 01 for RXD1
     #define ENABLE_UART1_RXD        LPC_PINCON->PINSEL1 |= (1<<0); LPC_PINCON->PINSEL1 &= ~(1<<1)

//     // pin select P2.8, function 10 for TXD2
//     #define ENABLE_UART2_TXD        LPC_PINCON->PINSEL4 |= (1<<17); LPC_PINCON->PINSEL4 &= ~(1<<16)
//     // pin select P2.9, function 10 for RXD2
//     #define ENABLE_UART2_RXD        LPC_PINCON->PINSEL4 |= (1<<19); LPC_PINCON->PINSEL4 &= ~(1<<18)
//     // pin select P4.28, function 11 for TXD3
//     #define ENABLE_UART3_TXD        LPC_PINCON->PINSEL9 |= (1<<25); LPC_PINCON->PINSEL9 |= (1<<24)
//     // pin select P4.29, function 11 for RXD3
//     #define ENABLE_UART3_RXD        LPC_PINCON->PINSEL9 |= (1<<27); LPC_PINCON->PINSEL9 |= (1<<26)

     // pin select P0.10, function 01 for TXD2
     #define ENABLE_UART2_TXD        LPC_PINCON->PINSEL0 |= (1<<20); LPC_PINCON->PINSEL0 &= ~(1<<21)
     // pin select P0.11, function 01 for RXD2
     #define ENABLE_UART2_RXD        LPC_PINCON->PINSEL0 |= (1<<22); LPC_PINCON->PINSEL0 &= ~(1<<23)
     // pin select P0.0, function 10 for TXD3
     #define ENABLE_UART3_TXD        LPC_PINCON->PINSEL0 |= (1<<1); LPC_PINCON->PINSEL0 &= ~(1<<0)
     // pin select P0.1, function 10 for RXD3
     #define ENABLE_UART3_RXD        LPC_PINCON->PINSEL0 |= (1<<3); LPC_PINCON->PINSEL0 &= ~(1<<2)

/*
#else

    #define PIN_0_0_FUNC_UART3_TXD  0x00000002
    #define PIN_0_1_FUNC_UART3_RXD  0x00000002

    #define PIN_0_2_FUNC_UART0_TXD  0x00000001
    #define PIN_0_3_FUNC_UART0_RXD  0x00000001

    #define PIN_0_10_FUNC_UART2_TXD 0x00000001
    #define PIN_0_11_FUNC_UART2_RXD 0x00000001

    #define PIN_0_15_FUNC_UART1_TXD 0x00000001
    #define PIN_0_16_FUNC_UART1_RXD 0x00000001

    #define PIN_1_29_FUNC_UART4_TXD 0x00000005
    #define PIN_2_9_FUNC_UART4_RXD  0x00000003


// pin select macros, modify to select pins used for application

    // Uart 0
    // pin select P0.2, function 001 for TXD0
    #define ENABLE_UART0_TXD        LPC_IOCON->P0_2 |= PIN_0_2_FUNC_UART0_TXD
    // pin select P0.3, function 001 for RXD0
    #define ENABLE_UART0_RXD        LPC_IOCON->P0_3 |= PIN_0_3_FUNC_UART0_RXD

    // Uart 1
    // pin select P0.15, function 001 for TXD1
    #define ENABLE_UART1_TXD    LPC_IOCON->P0_15 |= PIN_0_15_FUNC_UART1_TXD
    // pin select P0.16, function 001 for RXD1
    #define ENABLE_UART1_RXD    LPC_IOCON->P0_16 |= PIN_0_16_FUNC_UART1_RXD;

    // Uart 2
    // pin select P0.10, function 001 for TXD2
    #define ENABLE_UART2_TXD    LPC_IOCON->P0_10 |= PIN_0_10_FUNC_UART2_TXD
    // pin select P0.11, function 001 for RXD2
    #define ENABLE_UART2_RXD    LPC_IOCON->P0_11 |= PIN_0_11_FUNC_UART2_RXD

    // Uart 3
    // pin select P0.0, function 010 for TXD3
    #define ENABLE_UART3_TXD            LPC_IOCON->P0_0 |= PIN_0_0_FUNC_UART3_TXD
    // pin select P0.1, function 010 for RXD3
    #define ENABLE_UART3_RXD            LPC_IOCON->P0_1 |= PIN_0_1_FUNC_UART3_RXD

    // Uart 4
    // pin select
    #define ENABLE_UART4_TXD    LPC_IOCON->P1_29 |= PIN_1_29_FUNC_UART4_TXD
    // pin select
    #define ENABLE_UART4_RXD    LPC_IOCON->P2_9  |= PIN_2_9_FUNC_UART4_RXD

#endif
*/

#define LCR_DLAB_BIT        7
#define FDR_RESET_VALUE     0x10
#define TER_TXEN_BIT        7

#define INT_ID_MASK         0x0f
#define LINE_STATUS_IRQ_ID  0x06
#define RX_DATA_IRQ_ID      0x04
#define RX_TIMEOUT_IRQ_ID   0x0c
#define TX_EMPTY_IRQ_ID     0x02
#define MODEM_STATUS_IRQ_ID 0x00


//------------------------------------------------------------------------------
//  GLOBAL DECLARATIONS
//------------------------------------------------------------------------------

// array of pointers to the UART base addresses
// NOTE: UART1 structure is used here as it is a super set of the other UARTs
// with the exception of the IrDA register (which is not supported in this module)
LPC_UART1_TypeDef *pLpcUartRegs[NUM_OF_LPC_UARTS] =
        {
                (LPC_UART1_TypeDef*) LPC_UART0_BASE,
                (LPC_UART1_TypeDef*) LPC_UART1_BASE,
                (LPC_UART1_TypeDef*) LPC_UART2_BASE,
                (LPC_UART1_TypeDef*) LPC_UART3_BASE

        };

static BOOL UartInitialized[NUM_OF_LPC_UARTS] =
        {
                FALSE,
                FALSE,
                FALSE,
                FALSE,

        };


//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

// callback prototypes (modem status only applies to UART1)
void (*Uart0LineStatusCallBack)( BYTE LineStatusRegValue ) = NULL;
void (*Uart0RxDataCallBack)( BOOL FifoTimeout ) = NULL;
void (*Uart0TxDataCallBack)( void ) = NULL;
void (*Uart1LineStatusCallBack)( BYTE LineStatusRegValue ) = NULL;
void (*Uart1RxDataCallBack)( BOOL FifoTimeout );
void (*Uart1TxDataCallBack)( void ) = NULL;
void (*Uart1ModemStatusCallBack)( BYTE ModemStatusRegValue ) = NULL;
void (*Uart2LineStatusCallBack)( BYTE LineStatusRegValue ) = NULL;
void (*Uart2RxDataCallBack)( BOOL FifoTimeout ) = NULL;
void (*Uart2TxDataCallBack)( void ) = NULL;
void (*Uart3LineStatusCallBack)( BYTE LineStatusRegValue ) = NULL;
void (*Uart3RxDataCallBack)( BOOL FifoTimeout ) = NULL;
void (*Uart3TxDataCallBack)( void ) = NULL;
void (*Uart4LineStatusCallBack)( BYTE LineStatusRegValue ) = NULL;
void (*Uart4RxDataCallBack)( BOOL FifoTimeout ) = NULL;
void (*Uart4TxDataCallBack)( void ) = NULL;



//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS
//------------------------------------------------------------------------------


//******************************************************************************
//
//  Function: UartInit
//
//  Arguments: UartNum - UART number (0-3) to initialize
//             UartInitData - pointer to structure containing initialization data
//
//  Returns: TRUE if successful, else FALSE
//
//  Description: This function will configure and enable the selected UART.
//               Interrupt will only be registered with NVIC if one or more
//               interrupt sources are enabled in the interrupt enable register.
//               NOTE: this function does not enable any of the modem pins.
//               Verify that the desired Rx/Tx pins are selected in macros.
//
//******************************************************************************
BOOL UartInit( BYTE UartNum, LPC_UART_INIT_DATA *UartInitData )
{
    DWORD DivisorValue;

    // enable power to selected UART, set pclk and enable pins
    switch( UartNum )
    {
        case 0:
            // enable power to UART
            ENABLE_UART0_POWER;

            // set UART pclk to sysclk/4
            // DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
            // SET_UART0_PCLK;

            // enable RXD and TXD pin functions
            ENABLE_UART0_RXD;
            ENABLE_UART0_TXD;
            break;

        case 1:
            // enable power to UART
            ENABLE_UART1_POWER;

            // set UART pclk to sysclk/4
            // DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
            // SET_UART1_PCLK;

            // enable RXD and TXD pin functions
            ENABLE_UART1_RXD;
            ENABLE_UART1_TXD;
            break;

        case 2:
            // enable power to UART
            ENABLE_UART2_POWER;

            // set UART pclk to sysclk/4
            // DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
            // SET_UART2_PCLK;

            // enable RXD and TXD pin functions
            ENABLE_UART2_RXD;
            ENABLE_UART2_TXD;
            break;

        case 3:
            // enable power to UART
            ENABLE_UART3_POWER;

            // set UART pclk to sysclk/4
            // DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
            // SET_UART3_PCLK;

            // enable RXD and TXD pin functions
            ENABLE_UART3_RXD;
            ENABLE_UART3_TXD;
            break;

/*
#if ( TARGET == LPC177x_78x )
        case 4:

            // enable power to UART
            ENABLE_UART4_POWER;

            // set UART pclk to sysclk/4
            // DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
            // SET_UART3_PCLK;

            // enable RXD and TXD pin functions
            ENABLE_UART4_RXD;
            ENABLE_UART4_TXD;
            break;
#endif
*/
        default:
            // invalid port number
            return( FALSE );
            break;
    }

    // Configure Baud Rate Divisor
    if( UartInitData->BaudRate != 0 )
    {
        // since BaudRate is non-zero, use it to calculate the divisor value
        DivisorValue = PeripheralClock / UartInitData->BaudRate;

        // still need to divide by 16 to get divisor value, but first check if need to round up
        if( DivisorValue & 0x08 )
        {
            // need to round up
            DivisorValue = (DivisorValue >> 4) + 1;
        }
        else
        {
            // no need to round up, just div by 16
            DivisorValue >>= 4;
        }

        if( DivisorValue > 0xffff )
        {
            // divisor value is out of range
            return( FALSE );
        }

        // write calculated divisor value to init structure
        UartInitData->DivisorLatchValue = DivisorValue;

        // fractional divider is not supported for calculated divisor, so make sure it is reset
        pLpcUartRegs[UartNum]->FDR = FDR_RESET_VALUE;
    }
    else
    {
        // BaudRate is zero, so use DivisorLatchValue directly
        DivisorValue = UartInitData->DivisorLatchValue;

        // fractional divider register can also be used in this case
        pLpcUartRegs[UartNum]->FDR = UartInitData->FractionalDividerRegValue;
    }

    // write divisor value to selected UART
    // must first set DLAB bit in LCR register (will clear when LCR value written)
    pLpcUartRegs[UartNum]->LCR = (1 << LCR_DLAB_BIT);

    // write LSB to DLL register
    pLpcUartRegs[UartNum]->DLL = LOBYTE( DivisorValue );

    // write MSB to DLM register
    pLpcUartRegs[UartNum]->DLM = HIBYTE( DivisorValue );

    // calculate actual Baud Rate and write to init structure
    UartInitData->BaudRate = PeripheralClock / (DivisorValue * 16);

    // Reset FIFOs
    pLpcUartRegs[UartNum]->FCR = 0x07;

    // write FIFO control register
    pLpcUartRegs[UartNum]->FCR = UartInitData->FifoCtrlRegValue;

    // write line control register (with DLAB bit cleared)
    pLpcUartRegs[UartNum]->LCR = UartInitData->LineCtrlRegValue & ~(1<<LCR_DLAB_BIT);

    // write Modem Control Register (UART1 only, ignored by others)
    pLpcUartRegs[UartNum]->MCR = UartInitData->ModemCtrlRegValue;

    // write RS485 Control Register (UART1 only, ignored by others)
    pLpcUartRegs[UartNum]->RS485CTRL = UartInitData->RS485CtrlRegValue;

    // write RS485 Address Match Register (UART1 only, ignored by others)
    pLpcUartRegs[UartNum]->ADRMATCH = UartInitData->RS485AddrMatchRegValue;

    // write RS485 Delay Register (UART1 only, ignored by others)
    pLpcUartRegs[UartNum]->RS485DLY = UartInitData->RS485DelayRegValue;

    // write interrupt enable register
    pLpcUartRegs[UartNum]->IER = UartInitData->IrqEnableRegValue;

    // register ISR callbacks and register interrupt with NVIC if any interrupt sources are enabled
    if( UartInitData->IrqEnableRegValue != 0 )
    {
        switch( UartNum )
        {
            case 0:
                Uart0LineStatusCallBack = UartInitData->LineStatusCallBack;
                Uart0RxDataCallBack = UartInitData->RxDataCallBack;
                Uart0TxDataCallBack = UartInitData->TxDataCallBack;
                NVIC_SetPriority( UART0_IRQn, UartInitData->IntPriority );
                NVIC_EnableIRQ( UART0_IRQn );
                break;

            case 1:
                Uart1LineStatusCallBack = UartInitData->LineStatusCallBack;
                Uart1RxDataCallBack = UartInitData->RxDataCallBack;
                Uart1TxDataCallBack = UartInitData->TxDataCallBack;
                Uart1ModemStatusCallBack = UartInitData->ModemStatusCallBack;
                NVIC_SetPriority( UART1_IRQn, UartInitData->IntPriority );
                NVIC_EnableIRQ( UART1_IRQn );
                break;

            case 2:
                Uart2LineStatusCallBack = UartInitData->LineStatusCallBack;
                Uart2RxDataCallBack = UartInitData->RxDataCallBack;
                Uart2TxDataCallBack = UartInitData->TxDataCallBack;
                NVIC_SetPriority( UART2_IRQn, UartInitData->IntPriority );
                NVIC_EnableIRQ( UART2_IRQn );
                break;

            case 3:
                Uart3LineStatusCallBack = UartInitData->LineStatusCallBack;
                Uart3RxDataCallBack = UartInitData->RxDataCallBack;
                Uart3TxDataCallBack = UartInitData->TxDataCallBack;
                NVIC_SetPriority( UART3_IRQn, UartInitData->IntPriority );
                NVIC_EnableIRQ( UART3_IRQn );
                break;

/*
            case 4:
                Uart4LineStatusCallBack = UartInitData->LineStatusCallBack;
                Uart4RxDataCallBack = UartInitData->RxDataCallBack;
                Uart4TxDataCallBack = UartInitData->TxDataCallBack;
                NVIC_SetPriority( UART4_IRQn, UartInitData->IntPriority );
                NVIC_EnableIRQ( UART4_IRQn );
                break;
*/
        }
    }

    UartInitialized[UartNum] = TRUE;

    return( TRUE );
}


//******************************************************************************
//
//  Function: UartResetFifos
//
//  Arguments: UartNum - UART number (0-3) selected
//             RstRxFifo - if TRUE, Rx fifo will be reset
//             RstTxFifo - if TRUE, Tx fifo will be reset
//
//  Returns: void
//
//  Description: This function will reset the Rx and/or Tx FIFOs as requested.
//
//******************************************************************************
void UartResetFifos( BYTE UartNum, BOOL RstRxFifo, BOOL RstTxFifo )
{
    if( RstRxFifo )
    {
        pLpcUartRegs[UartNum]->FCR |= (1<<1);
    }

    if( RstTxFifo )
    {
        pLpcUartRegs[UartNum]->FCR |= (1<<2);
    }
}

//******************************************************************************
//
//  Function: UartLineStatus
//
//  Arguments: UartNum - UART number (0-3) selected
//
//  Returns: Line status register value for selected UART
//
//  Description: This function will read the line status register value for the
//               selected UART.  Status bit mask macros are define in UART.h.
//
//******************************************************************************
BYTE UartLineStatus( BYTE UartNum )
{
    if( UartNum >= NUM_OF_LPC_UARTS )
    {
        return( 0 );
    }
    else
    {
        return( pLpcUartRegs[UartNum]->LSR );
    }
}


//******************************************************************************
//
//  Function: UartReadByte
//
//  Arguments: UartNum - UART number (0-3) selected
//
//  Returns: Byte from top of receive FIFO
//
//  Description: This function will read a byte from the receive FIFO.  Calling
//               function should only call this function if there is data in the
//               FIFO (Rx interrupt occurred or line status register checked).
//
//******************************************************************************
BYTE UartReadByte( BYTE UartNum )
{
    return( pLpcUartRegs[UartNum]->RBR );
}


//******************************************************************************
//
//  Function: UartWriteByte
//
//  Arguments: UartNum - UART number (0-3) selected
//             Data - byte to write to transmit FIFO
//
//  Returns: void
//
//  Description: This function writes one byte to the transmit FIFO.  Calling
//               function is responsible to track how much data can be written
//               to the transmit FIFO.
//
//******************************************************************************
void UartWriteByte( BYTE UartNum, BYTE Data )
{
    if( UartNum < NUM_OF_LPC_UARTS )
    {
        pLpcUartRegs[UartNum]->THR = Data;
    }
}


//******************************************************************************
//
//  Function: UartTransmitEnable
//
//  Arguments: UartNum - UART number (0-3) selected
//
//  Returns: void
//
//  Description: Calling this function will re-enable the transmitter of the
//               selected UART if it was previously disable.
//
//******************************************************************************
void UartTransmitEnable( BYTE UartNum )
{
    if( UartNum < NUM_OF_LPC_UARTS )
    {
        pLpcUartRegs[UartNum]->TER = (1<<TER_TXEN_BIT);
    }
}


//******************************************************************************
//
//  Function: UartTransmitDisable
//
//  Arguments: UartNum - UART number (0-3) selected
//
//  Returns: void
//
//  Description: Calling this function will disable the transmitter of the selected
//               UART after the current character transmission is complete.
//
//******************************************************************************
void UartTransmitDisable( BYTE UartNum )
{
    if( UartNum < NUM_OF_LPC_UARTS )
    {
        pLpcUartRegs[UartNum]->TER = 0;
    }
}


//******************************************************************************
//
//  Function: UartIsTransmitEnabled
//
//  Arguments: UartNum - UART number (0-3) selected
//
//  Returns: void
//
//  Description: Calling this function returns status of whether the transmit
//               is enabled or disabled
//
//******************************************************************************
BOOL UartIsTransmitEnabled( BYTE UartNum )
{
    if( UartNum < NUM_OF_LPC_UARTS )
    {
        if( (pLpcUartRegs[UartNum]->TER) & (1<<TER_TXEN_BIT) )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }

    }

    return FALSE;
}


//******************************************************************************
//
//  Function: UartModemStatus
//
//  Arguments: UartNum - UART number selected (only supported for UART1)
//
//  Returns: byte containing modem status register value
//
//  Description: This function will read the modem status register.  This is
//               only valid for UART1.
//
//******************************************************************************
BYTE UartModemStatus( BYTE UartNum )
{
    if( UartNum == 1 )
    {
        return( pLpcUartRegs[UartNum]->MSR );
    }

    // invalid port
    return( 0 );
}


//******************************************************************************
//
//  Function: UartDtrCtrl
//
//  Arguments: UartNum - UART number selected (only supported for UART1)
//             DtrEnable - DTR is set active if TRUE, else inactive
//
//  Returns: void
//
//  Description: This function allows control of the DTR modem signal output.
//               This is only supported for UART1.
//
//******************************************************************************
void UartDtrCtrl( BYTE UartNum, BOOL DtrEnable )
{
    if( UartNum == 1 )
    {
        if( DtrEnable )
        {
            pLpcUartRegs[UartNum]->MCR |= 1;
        }
        else
        {
            pLpcUartRegs[UartNum]->MCR &= ~1;
        }
    }
}


//******************************************************************************
//
//  Function: UartRtsCtrl
//
//  Arguments: UartNum - UART number selected (only supported for UART1)
//             RtsEnable - RTS is set active if TRUE, else inactive
//
//  Returns: void
//
//  Description: This function allows control of the RTS modem signal output.
//               This is only supported for UART1.  Will also have no effect
//               if auto-RTS flow control is enabled.
//
//******************************************************************************
void UartRtsCtrl( BYTE UartNum, BOOL RtsEnable )
{
    if( UartNum == 1 )
    {
        if( RtsEnable )
        {
            pLpcUartRegs[UartNum]->MCR |= 2;
        }
        else
        {
            pLpcUartRegs[UartNum]->MCR &= ~2;
        }
    }
}



//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS
//------------------------------------------------------------------------------


//******************************************************************************
//
//  Function: UART0_IRQHandler
//
//  Arguments: void
//
//  Returns: void
//
//  Description: UART0 interrupt service routine.  Will determine which interrupt
//               source is pending and call the appropriate callback function.
//
//******************************************************************************
extern void UART0_IRQHandler( void )
{
    BYTE InterruptID;
    BYTE StatusValue;

    // read interrupt identification register
    InterruptID = (BYTE) (pLpcUartRegs[0]->IIR & INT_ID_MASK );

    switch( InterruptID )
    {
        case LINE_STATUS_IRQ_ID:
            // line status interrupt active, read line status register to clear
            StatusValue = pLpcUartRegs[0]->LSR;

            // call callback function if not NULL
            if( Uart0LineStatusCallBack != NULL )
            {
                Uart0LineStatusCallBack( StatusValue );
            }

            break;

        case RX_DATA_IRQ_ID:
            // receive data ready/FIFO threshold interrupt
            // callback function is responsible for reading Rx data to clear interrupt
            if( Uart0RxDataCallBack != NULL )
            {
                // set parameter FALSE to indicate no timeout
                Uart0RxDataCallBack( FALSE );
            }
            else
            {
                // no callback registered, so just read Rx data to clear interrupt
                pLpcUartRegs[0]->RBR;
            }

            break;

        case RX_TIMEOUT_IRQ_ID:
            // receive character timeout interrupt (with data in FIFO)
            // callback function is responsible for reading Rx data to clear interrupt
            if( Uart0RxDataCallBack != NULL )
            {
                // set parameter TRUE to indicate timeout
                Uart0RxDataCallBack( TRUE );
            }
            else
            {
                // no callback registered, so just read Rx data to clear interrupt
                pLpcUartRegs[0]->RBR;
            }

            break;

        case TX_EMPTY_IRQ_ID:
            // transmit holding register empty interrupt
            // interrupt should be cleared already by reading the interrupt identification register
            // callback function can write more Tx data if required
            if( Uart0TxDataCallBack != NULL )
            {
                Uart0TxDataCallBack();
            }

            break;

        default:
            break;
    }
}


//******************************************************************************
//
//  Function: UART1_IRQHandler
//
//  Arguments: void
//
//  Returns: void
//
//  Description: UART1 interrupt service routine.  Will determine which interrupt
//               source is pending and call the appropriate callback function.
//
//******************************************************************************
extern void UART1_IRQHandler( void )
{
    BYTE InterruptID;
    BYTE StatusValue;

    // read interrupt identification register
    InterruptID = (BYTE) (pLpcUartRegs[1]->IIR & INT_ID_MASK );

    switch( InterruptID )
    {
        case LINE_STATUS_IRQ_ID:
            // line status interrupt active, read line status register to clear
            StatusValue = pLpcUartRegs[1]->LSR;

            // call callback function if not NULL
            if( Uart1LineStatusCallBack != NULL )
            {
                Uart1LineStatusCallBack( StatusValue );
            }

            break;

        case RX_DATA_IRQ_ID:
            // receive data ready/FIFO threshold interrupt
            // callback function is responsible for reading Rx data to clear interrupt
            if( Uart1RxDataCallBack != NULL )
            {
                // set parameter FALSE to indicate no timeout
                Uart1RxDataCallBack( FALSE );
            }
            else
            {
                // no callback registered, so just read Rx data to clear interrupt
                pLpcUartRegs[1]->RBR;
            }

            break;

        case RX_TIMEOUT_IRQ_ID:
            // receive character timeout interrupt (with data in FIFO)
            // callback function is responsible for reading Rx data to clear interrupt
            if( Uart1RxDataCallBack != NULL )
            {
                // set parameter TRUE to indicate timeout
                Uart1RxDataCallBack( TRUE );
            }
            else
            {
                // no callback registered, so just read Rx data to clear interrupt
                pLpcUartRegs[1]->RBR;
            }

            break;

        case TX_EMPTY_IRQ_ID:
            // transmit holding register empty interrupt
            // interrupt should be cleared already by reading the interrupt identification register
            // callback function can write more Tx data if required
            if( Uart1TxDataCallBack != NULL )
            {
                Uart1TxDataCallBack();
            }

            break;

        case MODEM_STATUS_IRQ_ID:
            // modem status interrupt, read modem status register
            StatusValue = pLpcUartRegs[1]->MSR;

            // call callback function if not NULL
            if( Uart1ModemStatusCallBack != NULL )
            {
                Uart1ModemStatusCallBack( StatusValue );
            }

            break;

        default:
            break;
    }
}


//******************************************************************************
//
//  Function: UART2_IRQHandler
//
//  Arguments: void
//
//  Returns: void
//
//  Description: UART2 interrupt service routine.  Will determine which interrupt
//               source is pending and call the appropriate callback function.
//
//******************************************************************************
extern void UART2_IRQHandler( void )
{
    BYTE InterruptID;
    BYTE StatusValue;

    // read interrupt identification register
    InterruptID = (BYTE) (pLpcUartRegs[2]->IIR & INT_ID_MASK );

    switch( InterruptID )
    {
        case LINE_STATUS_IRQ_ID:
            // line status interrupt active, read line status register to clear
            StatusValue = pLpcUartRegs[2]->LSR;

            // call callback function if not NULL
            if( Uart2LineStatusCallBack != NULL )
            {
                Uart2LineStatusCallBack( StatusValue );
            }

            break;

        case RX_DATA_IRQ_ID:
            // receive data ready/FIFO threshold interrupt
            // callback function is responsible for reading Rx data to clear interrupt
            if( Uart2RxDataCallBack != NULL )
            {
                // set parameter FALSE to indicate no timeout
                Uart2RxDataCallBack( FALSE );
            }
            else
            {
                // no callback registered, so just read Rx data to clear interrupt
                pLpcUartRegs[2]->RBR;
            }

            break;

        case RX_TIMEOUT_IRQ_ID:
            // receive character timeout interrupt (with data in FIFO)
            // callback function is responsible for reading Rx data to clear interrupt
            if( Uart2RxDataCallBack != NULL )
            {
                // set parameter TRUE to indicate timeout
                Uart2RxDataCallBack( TRUE );
            }
            else
            {
                // no callback registered, so just read Rx data to clear interrupt
                pLpcUartRegs[2]->RBR;
            }

            break;

        case TX_EMPTY_IRQ_ID:
            // transmit holding register empty interrupt
            // interrupt should be cleared already by reading the interrupt identification register
            // callback function can write more Tx data if required
            if( Uart2TxDataCallBack != NULL )
            {
                Uart2TxDataCallBack();
            }

            break;

        default:
            break;
    }
}


//******************************************************************************
//
//  Function: UART3_IRQHandler
//
//  Arguments: void
//
//  Returns: void
//
//  Description: UART3 interrupt service routine.  Will determine which interrupt
//               source is pending and call the appropriate callback function.
//
//******************************************************************************
extern void UART3_IRQHandler( void )
{
    BYTE InterruptID;
    BYTE StatusValue;

    // read interrupt identification register
    InterruptID = (BYTE) (pLpcUartRegs[3]->IIR & INT_ID_MASK );

    switch( InterruptID )
    {
        case LINE_STATUS_IRQ_ID:
            // line status interrupt active, read line status register to clear
            StatusValue = pLpcUartRegs[3]->LSR;

            // call callback function if not NULL
            if( Uart3LineStatusCallBack != NULL )
            {
                Uart3LineStatusCallBack( StatusValue );
            }

            break;

        case RX_DATA_IRQ_ID:
            // receive data ready/FIFO threshold interrupt
            // callback function is responsible for reading Rx data to clear interrupt
            if( Uart3RxDataCallBack != NULL )
            {
                // set parameter FALSE to indicate no timeout
                Uart3RxDataCallBack( FALSE );
            }
            else
            {
                // no callback registered, so just read Rx data to clear interrupt
                pLpcUartRegs[3]->RBR;
            }

            break;

        case RX_TIMEOUT_IRQ_ID:
            // receive character timeout interrupt (with data in FIFO)
            // callback function is responsible for reading Rx data to clear interrupt
            if( Uart3RxDataCallBack != NULL )
            {
                // set parameter TRUE to indicate timeout
                Uart3RxDataCallBack( TRUE );
            }
            else
            {
                // no callback registered, so just read Rx data to clear interrupt
                pLpcUartRegs[3]->RBR;
            }

            break;

        case TX_EMPTY_IRQ_ID:
            // transmit holding register empty interrupt
            // interrupt should be cleared already by reading the interrupt identification register
            // callback function can write more Tx data if required
            if( Uart3TxDataCallBack != NULL )
            {
                Uart3TxDataCallBack();
            }

            break;

        default:
            break;
    }
}


//******************************************************************************
//
//  Function: UART4_IRQHandler
//
//  Arguments: void
//
//  Returns: void
//
//  Description: UART4 interrupt service routine.  Will determine which interrupt
//               source is pending and call the appropriate callback function.
//
//******************************************************************************
extern void UART4_IRQHandler( void )
{
    BYTE InterruptID;
    BYTE StatusValue;

    // read interrupt identification register
    InterruptID = (BYTE) (pLpcUartRegs[4]->IIR & INT_ID_MASK );

    switch( InterruptID )
    {
        case LINE_STATUS_IRQ_ID:
            // line status interrupt active, read line status register to clear
            StatusValue = pLpcUartRegs[4]->LSR;

            // call callback function if not NULL
            if( Uart4LineStatusCallBack != NULL )
            {
                Uart4LineStatusCallBack( StatusValue );
            }

            break;

        case RX_DATA_IRQ_ID:
            // receive data ready/FIFO threshold interrupt
            // callback function is responsible for reading Rx data to clear interrupt
            if( Uart4RxDataCallBack != NULL )
            {
                // set parameter FALSE to indicate no timeout
                Uart4RxDataCallBack( FALSE );
            }
            else
            {
                // no callback registered, so just read Rx data to clear interrupt
                pLpcUartRegs[4]->RBR;
            }

            break;

        case RX_TIMEOUT_IRQ_ID:
            // receive character timeout interrupt (with data in FIFO)
            // callback function is responsible for reading Rx data to clear interrupt
            if( Uart4RxDataCallBack != NULL )
            {
                // set parameter TRUE to indicate timeout
                Uart4RxDataCallBack( TRUE );
            }
            else
            {
                // no callback registered, so just read Rx data to clear interrupt
                pLpcUartRegs[4]->RBR;
            }

            break;

        case TX_EMPTY_IRQ_ID:
            // transmit holding register empty interrupt
            // interrupt should be cleared already by reading the interrupt identification register
            // callback function can write more Tx data if required
            if( Uart4TxDataCallBack != NULL )
            {
                Uart4TxDataCallBack();
            }

            break;

        default:
            break;
    }
}


