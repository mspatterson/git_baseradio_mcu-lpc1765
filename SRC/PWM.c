////******************************************************************************
////
////  PWM.c: LPC PWM Control Module
////
////      Copyright (c) 2011, Microlynx Systems Ltd
////      ALL RIGHTS RESERVED
////
////  This module provides control of the LPC internal PWM hardware.  Currently
////  only supports single edge controlled PWMs with no interrupt support.
////
////  Date            Author      Comment
////  ----------------------------------------------------------------
////  2011-04-25      John     Initial Implementation
////
////******************************************************************************
//
//
////------------------------------------------------------------------------------
////  INCLUDES
////------------------------------------------------------------------------------
//#include "PWM.h"
//#include "target.h"
//
//
////------------------------------------------------------------------------------
////  CONSTANT & MACRO DEFINITIONS
////------------------------------------------------------------------------------
//#define PWM_IRQ_PRIORITY    28
//
//#define TCR_CNT_EN      (1<<0)
//#define TCR_RESET       (1<<1)
//#define TCR_PWM_EN      (1<<3)
//
//#define LER0_EN         (1 << 0)
//#define LER1_EN         (1 << 1)
//#define LER2_EN         (1 << 2)
//#define LER3_EN         (1 << 3)
//#define LER4_EN         (1 << 4)
//#define LER5_EN         (1 << 5)
//#define LER6_EN         (1 << 6)
//
//// pin configuration macros (uncomment appropriate pin selection)
//// pin 2.0 set to function 01 for PWM 1.1
//// #define PWM1_1_PIN_ENABLE   LPC_PINCON->PINSEL4 |= (1<<0); LPC_PINCON->PINSEL4 &= ~(1<<1)
//// pin 1.18 set to function 10 for PWM 1.1
//#define PWM1_1_PIN_ENABLE   LPC_PINCON->PINSEL3 &= ~(1<<4); LPC_PINCON->PINSEL3 |= (1<<5)
//
//// pin 2.1 set to function 01 for PWM 1.2
//// #define PWM1_2_PIN_ENABLE   LPC_PINCON->PINSEL4 |= (1<<2); LPC_PINCON->PINSEL4 &= ~(1<<3)
//// pin 1.20 set to function 10 for PWM 1.2
//#define PWM1_2_PIN_ENABLE   LPC_PINCON->PINSEL3 &= ~(1<<8); LPC_PINCON->PINSEL3 |= (1<<9)
//// pin 3.25 set to function 11 for PWM 1.2
//// #define PWM1_2_PIN_ENABLE   LPC_PINCON->PINSEL7 |= (1<<19); LPC_PINCON->PINSEL7 |= (1<<18)
//
//// pin 2.2 set to function 01 for PWM 1.3
//// #define PWM1_3_PIN_ENABLE   LPC_PINCON->PINSEL4 |= (1<<4); LPC_PINCON->PINSEL4 &= ~(1<<5)
//// pin 1.21 set to function 10 for PWM 1.3
//#define PWM1_3_PIN_ENABLE   LPC_PINCON->PINSEL3 &= ~(1<<10); LPC_PINCON->PINSEL3 |= (1<<11)
//// pin 3.26 set to function 11 for PWM 1.3
////#define PWM1_3_PIN_ENABLE   LPC_PINCON->PINSEL7 |= (1<<21); LPC_PINCON->PINSEL7 |= (1<<20)
//
//// pin 2.3 set to function 01 for PWM 1.4
//// #define PWM1_4_PIN_ENABLE   LPC_PINCON->PINSEL4 |= (1<<6); LPC_PINCON->PINSEL4 &= ~(1<<7)
//// pin 1.23 set to function 10 for PWM 1.4
//#define PWM1_4_PIN_ENABLE   LPC_PINCON->PINSEL3 |= (1<<15); LPC_PINCON->PINSEL3 &= ~(1<<14)
//
//// pin 2.4 set to function 01 for PWM 1.5
//// #define PWM1_5_PIN_ENABLE   LPC_PINCON->PINSEL4 |= (1<<8); LPC_PINCON->PINSEL4 &= ~(1<<9)
//// pin 1.24 set to function 10 for PWM 1.5
//#define PWM1_5_PIN_ENABLE   LPC_PINCON->PINSEL3 |= (1<<17); LPC_PINCON->PINSEL3 &= ~(1<<16)
//
//// pin 2.5 set to function 01 for PWM 1.6
//// #define PWM1_6_PIN_ENABLE   LPC_PINCON->PINSEL4 |= (1<<10); LPC_PINCON->PINSEL4 &= ~(1<<11)
//// pin 1.26 set to function 10 for PWM 1.6
//#define PWM1_6_PIN_ENABLE   LPC_PINCON->PINSEL3 |= (1<<21); LPC_PINCON->PINSEL3 &= ~(1<<20)
//
//
//
////------------------------------------------------------------------------------
////  GLOBAL DECLARATIONS
////------------------------------------------------------------------------------
//
//
//
////------------------------------------------------------------------------------
////  PRIVATE FUNCTION PROTOTYPES
////------------------------------------------------------------------------------
//
//
//
////------------------------------------------------------------------------------
////  PUBLIC FUNCTIONS
////------------------------------------------------------------------------------
//
//
////******************************************************************************
////
////  Function: PwmInit
////
////  Arguments: PwmChnlEnables - b0:5 set corresponding bits to enable PWM chnl 1-6
////             PwmCounterFrequency - desired counter frequency for PWM in Hz
////                                   the minimum pulse length is 1/PwmCounterFrequency
////             CounterRstValue - Value at which PWM counter is reset, this determines
////                               the PWM period as: CounterRstValue/PwmCounterFrequency
////                               (counter starts from 1)
////
////  Returns: TRUE if successful, else FALSE
////
////  Description: This function will configure and enable the selected PWM channels.
////
////
////******************************************************************************
//BOOL PwmInit( WORD PwmChnlEnables, DWORD PwmCounterFrequency, DWORD CounterRstValue )
//{
//    // enable power to PWM
//    LPC_SC->PCONP |= (1<<6);
//
//    // set PWM pclk to sysclk/4
//    // DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
//    // LPC_SC->PCLKSEL0 &= ~(3<<12);
//
//    // disable all PWM channels to start
//    LPC_PWM1->PCR = 0;
//
//    // configure required PWM pins and enable channels
//    if( PwmChnlEnables & (1<<0) )
//    {
//        PWM1_1_PIN_ENABLE;
//
//        // enable PWM channel
//        LPC_PWM1->PCR |= (1<<9);
//    }
//    if( PwmChnlEnables & (1<<1) )
//    {
//        PWM1_2_PIN_ENABLE;
//
//        // enable PWM channel
//        LPC_PWM1->PCR |= (1<<10);
//    }
//    if( PwmChnlEnables & (1<<2) )
//    {
//        PWM1_3_PIN_ENABLE;
//
//        // enable PWM channel
//        LPC_PWM1->PCR |= (1<<11);
//    }
//    if( PwmChnlEnables & (1<<3) )
//    {
//        PWM1_4_PIN_ENABLE;
//
//        // enable PWM channel
//        LPC_PWM1->PCR |= (1<<12);
//    }
//    if( PwmChnlEnables & (1<<4) )
//    {
//        PWM1_5_PIN_ENABLE;
//
//        // enable PWM channel
//        LPC_PWM1->PCR |= (1<<13);
//    }
//    if( PwmChnlEnables & (1<<5) )
//    {
//        PWM1_6_PIN_ENABLE;
//
//        // enable PWM channel
//        LPC_PWM1->PCR |= (1<<14);
//    }
//
//    // validate PwmCounterFrequency
//    if( PwmCounterFrequency > (SystemCoreClock/4) )
//    {
//        // PwmCounterFrequency is too high
//        return( FALSE );
//    }
//
//    // calculate counter prescale value (pclk = sysclk/4)
//    LPC_PWM1->PR = (SystemCoreClock/4/PwmCounterFrequency) - 1;
//
//    // reset counter and generate interrupt when match register 0 value matches counter
//    LPC_PWM1->MCR = (1<<1) | (1<<0);
//
//    // set match register 0 to count reset value
//    LPC_PWM1->MR0 = CounterRstValue;
//
//    // set all channels to 0 duty to start
//    LPC_PWM1->MR1 = 0;
//    LPC_PWM1->MR2 = 0;
//    LPC_PWM1->MR3 = 0;
//    LPC_PWM1->MR4 = 0;
//    LPC_PWM1->MR5 = 0;
//    LPC_PWM1->MR6 = 0;
//
//    // enable all match values to be latched
//    LPC_PWM1->LER = LER0_EN | LER1_EN | LER2_EN | LER3_EN | LER4_EN | LER5_EN | LER6_EN;
//
//    // reset and enable PWM
//    LPC_PWM1->TCR = TCR_RESET;
//    LPC_PWM1->TCR = TCR_CNT_EN | TCR_PWM_EN;
//
//    // enable the PWM interrupt in the NVIC
//    NVIC_EnableIRQ( PWM1_IRQn );
//    NVIC_SetPriority( PWM1_IRQn, PWM_IRQ_PRIORITY );
//
//    return( TRUE );
//}
//
//
////******************************************************************************
////
////  Function: PwmChannelSet
////
////  Arguments: PwmChannel - channel to update (1-6)
////             PwmDutyCount - sets the duty cycle to be equal to:
////                            PwmDutyCount/CounterRstValue (CounterRstValue as set in PwmInit)
////
////  Returns: void
////
////  Description: This function will update the duty cycle setting for the selected
////               PWM channel.  Note that the PWM outputs are active high by default
////               so set PwmDutyCount to (CounterRstValue - DesiredValue) for an
////               active low equivalent output.
////
////******************************************************************************
//void PwmChannelSet( BYTE PwmChannel, DWORD PwmDutyCount )
//{
//    switch( PwmChannel )
//    {
//        case 1:
//            LPC_PWM1->MR1 = PwmDutyCount;
//            break;
//
//        case 2:
//            LPC_PWM1->MR2 = PwmDutyCount;
//            break;
//
//        case 3:
//            LPC_PWM1->MR3 = PwmDutyCount;
//            break;
//
//        case 4:
//            LPC_PWM1->MR4 = PwmDutyCount;
//            break;
//
//        case 5:
//            LPC_PWM1->MR5 = PwmDutyCount;
//            break;
//
//        case 6:
//            LPC_PWM1->MR6 = PwmDutyCount;
//            break;
//
//        default:
//            break;
//    }
//
//    // set all match registers to be updated
//    // !!!NOTE!!!
//    // channel 3 was occasionally not updating properly with this method.  Latch
//    // enable bits are now written once every PWM cycle in the PWM ISR
////    LPC_PWM1->LER = LER1_EN | LER2_EN | LER3_EN | LER4_EN | LER5_EN | LER6_EN;
//}
//
////------------------------------------------------------------------------------
////  PRIVATE FUNCTIONS
////------------------------------------------------------------------------------
//
////******************************************************************************
////
////  Function: PWM1_IRQHandler
////
////  Arguments:
////
////  Returns:
////
////  Description: PWM ISR - writes to latch enable bits so that updated match
////               register values will be used.  This was required to ensure
////               new match register values reliably got latched into PWM.
////
////******************************************************************************
//extern void PWM1_IRQHandler(void)
//{
//    // clear interrupt
//    LPC_PWM1->IR = 1;
//
//    // set match register latch enable bits
//    LPC_PWM1->LER = LER1_EN | LER2_EN | LER3_EN | LER4_EN | LER5_EN | LER6_EN;
//}
//
//
//
//
//
//#define PWM_MIN_CHAN_NMBR 1
//#define PWM_MAX_CHAN_NMBR 6
//
////******************************************************************************
////
////******************************************************************************
//void setPWMChannelDutyCycle( BYTE byChannel, BYTE byDutyCycle )
//{
//
//
//	WORD wCountForDutyCycle;
//
//    if( (byChannel < PWM_MIN_CHAN_NMBR )|| (byChannel > PWM_MAX_CHAN_NMBR) )
//        return;
//
//    if( byDutyCycle > 100 )
//        return;
//
//
//    wCountForDutyCycle = byDutyCycle * 100;
//
//
//    PwmChannelSet( byChannel, wCountForDutyCycle );
//
//
//}
//
//

