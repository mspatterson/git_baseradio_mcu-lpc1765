#include <string.h>

#include "logging.h"
#include "HardwareAbstraction.h"
#include "UartManager.h"
#include "timer.h"
#include "CommUsbInterface.h"

static UART_MGR_OBJECT myUartPortUsb;
static SERIAL_CFG      cfgInfoUsbSerPort;
static TIMERHANDLE thCommUsbTimer;

// We maintain a small linear buffer in the case of back to back writes to the debug port
#define DEBUG_BUFF_LEN  256
static BYTE pbyDebugBuff[DEBUG_BUFF_LEN];

#define USB_RX_BUFF_LEN	128
static BYTE byUsbRxBuff[USB_RX_BUFF_LEN];
static WORD	wRxBuffIndex;
static WORD wRxBuffReadLen;

#define USB_RX_PKT_TIMEOUT 5	//ms

static void TxISRSerPortUsb( void )
{
    // In the 'normal' build, let the default Tx ISR handle this event
    UartTxISR( &myUartPortUsb );

}

static void RxISRSerPortUsb( BOOL bIntWasTimeOut )
{
    UartRxISR( &myUartPortUsb );

}

void InitCommUsb( void )
{
    BOOL bPortDidInit;

    clearUsbRxBuff();

    myUartPortUsb.byPortNum = UART_USB_PN;
    myUartPortUsb.UartRxDataCallBack = RxISRSerPortUsb;
    myUartPortUsb.UartTxDataCallBack = TxISRSerPortUsb;

    InitUartManager( &myUartPortUsb );

    cfgInfoUsbSerPort.dwSpeed   = 115200;
    cfgInfoUsbSerPort.wDataBits = 8;
    cfgInfoUsbSerPort.wParity   = PARITY_NONE;
    cfgInfoUsbSerPort.wStopBits = ONE_STOP_BIT;
    cfgInfoUsbSerPort.wFlowCntl = NO_FLOW_CNTL;

    bPortDidInit = OpenUartSerialPort( &myUartPortUsb, &cfgInfoUsbSerPort );

    thCommUsbTimer = RegisterTimer();
    ResetTimer(thCommUsbTimer, USB_RX_PKT_TIMEOUT );
    StartTimer(thCommUsbTimer);

}


BOOL WriteToUsbPort( const BYTE* pBuff, WORD buffLen )
{
    // If we are currently sending, we need to append what we can to the buffer.
    // If we aren't sending, just copy to the buffer and start



    // With this build option the debug port behaves as expected

    // Disable interrupts while accessing the queue
    DisableInts();

    if( myUartPortUsb.bTxQSending )
    {
        WORD bytesToCopy;

        if( myUartPortUsb.wTxQSize + buffLen <= DEBUG_BUFF_LEN )
            bytesToCopy = buffLen;
        else
            bytesToCopy = DEBUG_BUFF_LEN - myUartPortUsb.wTxQSize;

        memcpy( &(pbyDebugBuff[myUartPortUsb.wTxQSize]), pBuff, bytesToCopy );

        myUartPortUsb.wTxQSize += bytesToCopy;

        // Tack on "\r\n" if we have room
//        if( myUartPortUsb.wTxQSize < DEBUG_BUFF_LEN )
//        {
//            pbyDebugBuff[myUartPortUsb.wTxQSize]   = '\r';
//            pbyDebugBuff[myUartPortUsb.wTxQSize+1] = '\n';
//
//            myUartPortUsb.wTxQSize += 2;
//        }
    }
    else
    {
        // Copy the buffer across. We tack on "\r\n" if there is room
        WORD bytesToCopy;

        if( buffLen < DEBUG_BUFF_LEN )
            bytesToCopy = buffLen;
        else
            bytesToCopy = DEBUG_BUFF_LEN;

        memcpy( pbyDebugBuff, pBuff, bytesToCopy );

//        if( bytesToCopy + 2 <= DEBUG_BUFF_LEN )
//        {
//            pbyDebugBuff[bytesToCopy]   = '\r';
//            pbyDebugBuff[bytesToCopy+1] = '\n';
//
//            bytesToCopy += 2;
//        }

        UartPortSendBuffer( &myUartPortUsb, pbyDebugBuff, bytesToCopy );
    }

    EnableInts();

    return TRUE;

}


BOOL UsbPortSending( void )
{
    // With this build option, return the actual state of the transmit queue
    return UartTxQueueSending( &myUartPortUsb );
}


void clearUsbRxBuff(void)
{
	wRxBuffIndex = 0;
}

static BYTE byNewRxData;
void UpdateUsbComRxBuf( void  )
{
    BOOL bGotChar;



    bGotChar =  GetUartPortChar(  &myUartPortUsb, &byNewRxData );
    if( bGotChar )
    {
    	if(wRxBuffIndex<(USB_RX_BUFF_LEN-1))
    	{
    		byUsbRxBuff[wRxBuffIndex++]= byNewRxData ;
    		ResetTimer(thCommUsbTimer, USB_RX_PKT_TIMEOUT );
    	}
    }
}


BOOL IsUsbRxPacketReady(void)
{
	if(wRxBuffIndex>0)
	{
		if(TimerExpired( thCommUsbTimer))
			return TRUE;
	}
	return FALSE;
}



WORD ReadUsbRxPacket(BYTE *pByteData, WORD bufSize)
{
	for(wRxBuffReadLen=0; wRxBuffReadLen<=wRxBuffIndex; wRxBuffReadLen++)
	{
		*pByteData = byUsbRxBuff[wRxBuffReadLen];
		pByteData++;
		if(wRxBuffReadLen>=bufSize)
			break;
	}

	clearUsbRxBuff();
	return wRxBuffReadLen;
}

