#include <string.h>

#include "logging.h"
#include "HardwareAbstraction.h"
#include "UartManager.h"
#include "timer.h"
#include "CommRF1Interface.h"

static UART_MGR_OBJECT myUartPortRF1;
static SERIAL_CFG      cfgInfoRF1SerPort;
static TIMERHANDLE thCommRF1Timer;

// We maintain a small linear buffer in the case of back to back writes to the debug port
#define DEBUG_BUFF_LEN  256
static BYTE pbyDebugBuff[DEBUG_BUFF_LEN];

#define RF1_RX_BUFF_LEN	128
static BYTE byRF1RxBuff[RF1_RX_BUFF_LEN];
static WORD	wRxBuffIndex;
static WORD wRxBuffReadLen;

#define RF1_RX_PKT_TIMEOUT 5	//ms

static void TxISRSerPortRF1( void )
{
    // In the 'normal' build, let the default Tx ISR handle this event
    UartTxISR( &myUartPortRF1 );

}

static void RxISRSerPortRF1( BOOL bIntWasTimeOut )
{
    UartRxISR( &myUartPortRF1 );

}

void InitCommRF1( void )
{
    BOOL bPortDidInit;

    clearRF1RxBuff();

    myUartPortRF1.byPortNum = UART_RF1_PN;
    myUartPortRF1.UartRxDataCallBack = RxISRSerPortRF1;
    myUartPortRF1.UartTxDataCallBack = TxISRSerPortRF1;

    InitUartManager( &myUartPortRF1 );

    cfgInfoRF1SerPort.dwSpeed   = 115200;
    cfgInfoRF1SerPort.wDataBits = 8;
    cfgInfoRF1SerPort.wParity   = PARITY_NONE;
    cfgInfoRF1SerPort.wStopBits = ONE_STOP_BIT;
    cfgInfoRF1SerPort.wFlowCntl = NO_FLOW_CNTL;

    bPortDidInit = OpenUartSerialPort( &myUartPortRF1, &cfgInfoRF1SerPort );

    thCommRF1Timer = RegisterTimer();
    ResetTimer(thCommRF1Timer, RF1_RX_PKT_TIMEOUT );
    StartTimer(thCommRF1Timer);

}


BOOL WriteToRF1Port( const BYTE* pBuff, WORD buffLen )
{
    // If we are currently sending, we need to append what we can to the buffer.
    // If we aren't sending, just copy to the buffer and start



    // With this build option the debug port behaves as expected

    // Disable interrupts while accessing the queue
    DisableInts();

    if( myUartPortRF1.bTxQSending )
    {
        WORD bytesToCopy;

        if( myUartPortRF1.wTxQSize + buffLen <= DEBUG_BUFF_LEN )
            bytesToCopy = buffLen;
        else
            bytesToCopy = DEBUG_BUFF_LEN - myUartPortRF1.wTxQSize;

        memcpy( &(pbyDebugBuff[myUartPortRF1.wTxQSize]), pBuff, bytesToCopy );

        myUartPortRF1.wTxQSize += bytesToCopy;
    }
    else
    {
        // Copy the buffer across. We tack on "\r\n" if there is room
        WORD bytesToCopy;

        if( buffLen < DEBUG_BUFF_LEN )
            bytesToCopy = buffLen;
        else
            bytesToCopy = DEBUG_BUFF_LEN;

        memcpy( pbyDebugBuff, pBuff, bytesToCopy );

        UartPortSendBuffer( &myUartPortRF1, pbyDebugBuff, bytesToCopy );
    }

    EnableInts();

    return TRUE;

}


BOOL RF1PortSending( void )
{
    // With this build option, return the actual state of the transmit queue
    return UartTxQueueSending( &myUartPortRF1 );
}


void clearRF1RxBuff(void)
{
	wRxBuffIndex = 0;
}

static BYTE byNewRxData;
void UpdateRF1ComRxBuf( void  )
{
    BOOL bGotChar;



    bGotChar =  GetUartPortChar(  &myUartPortRF1, &byNewRxData );
    if( bGotChar )
    {
    	if(wRxBuffIndex<(RF1_RX_BUFF_LEN-1))
    	{
    		byRF1RxBuff[wRxBuffIndex++]= byNewRxData ;
    		ResetTimer(thCommRF1Timer, RF1_RX_PKT_TIMEOUT );
    	}
    }
}


BOOL IsRF1RxPacketReady(void)
{
	if(wRxBuffIndex>0)
	{
		if(TimerExpired( thCommRF1Timer))
			return TRUE;
	}
	return FALSE;
}



WORD ReadRF1RxPacket(BYTE *pByteData, WORD bufSize)
{
	for(wRxBuffReadLen=0; wRxBuffReadLen<wRxBuffIndex; wRxBuffReadLen++)
	{
		*pByteData = byRF1RxBuff[wRxBuffReadLen];
		pByteData++;
		if(wRxBuffReadLen>=bufSize)
			break;

	}

	clearRF1RxBuff();
	return wRxBuffReadLen;
}

