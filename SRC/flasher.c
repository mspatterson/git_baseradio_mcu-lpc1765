//******************************************************************************
//
//  Flasher.c
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
// Contains functions for accessing the ISP on-chip flash programming
// routines built into the LPC devices
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  Version 1.0     JK      July 26, 2012
//
//******************************************************************************


#include <stdio.h>
#include <stdint.h>
#include "LPC17xx.h"
#include "typedefs.h"
#include "flasher.h"
#include "timer.h"







//*****************************************************************************
// Sector     Size  Address Range
//*****************************************************************************
//  0         4      0X0000 0000 - 0X0000 0FFF 
//  1         4      0X0000 1000 - 0X0000 1FFF 
//  2         4      0X0000 2000 - 0X0000 2FFF 
//  3         4      0X0000 3000 - 0X0000 3FFF 
//  4         4      0X0000 4000 - 0X0000 4FFF 
//  5         4      0X0000 5000 - 0X0000 5FFF 
//  6         4      0X0000 6000 - 0X0000 6FFF 
//  7         4      0X0000 7000 - 0X0000 7FFF 
//  8         4      0x0000 8000 - 0X0000 8FFF
//  9         4      0x0000 9000 - 0X0000 9FFF
//  10        4      0x0000 A000 - 0X0000 AFFF
//  11        4      0x0000 B000 - 0X0000 BFFF
//  12        4      0x0000 C000 - 0X0000 CFFF
//  13        4      0x0000 D000 - 0X0000 DFFF
//  14        4      0x0000 E000 - 0X0000 EFFF
//  15        4      0x0000 F000 - 0X0000 FFFF
//  16        32     0x0001 0000 - 0X0001 7FFF
//  17        32     0x0001 8000 - 0X0001 FFFF
//  18        32     0x0002 0000 - 0X0002 7FFF
//  19        32     0x0002 8000 - 0X0002 FFFF
//  20        32     0x0003 0000 - 0X0003 7FFF
//  21        32     0x0003 8000 - 0X0003 FFFF
//  22        32     0x0004 0000 - 0X0004 7FFF
//  23        32     0x0004 8000 - 0X0004 FFFF
//  24        32     0x0005 0000 - 0X0005 7FFF
//  25        32     0x0005 8000 - 0X0005 FFFF
//  26        32     0x0006 0000 - 0X0006 7FFF
//  27        32     0x0006 8000 - 0X0006 FFFF
//  28        32     0x0007 0000 - 0X0007 7FFF
//  29        32     0x0007 8000 - 0X0007 FFFF
//*****************************************************************************
#define NUM_OF_SECTORS 27
// #define SECT	15
// #define SECT_ADDR 0x0000F000








#define BLOCK_FOR_DIAGNOSTIC_TEST 18
#define BLOCK_FOR_TEST_ADDR  0x00020000






//*****************************************************************************
//
//*****************************************************************************
BOOL SectorHasWritePermission( BYTE bySectorNum, DWORD* ptrMemAddr )
{

	BOOL bWritePermission = FALSE;

    switch( bySectorNum )
    {
        case USER_DATA_SECTOR1:
        	bWritePermission = TRUE;
        	*ptrMemAddr = USER_DATA_SECT1_ADDR;
            break;

        case USER_DATA_SECTOR2:
        	bWritePermission = TRUE;
        	*ptrMemAddr = USER_DATA_SECT2_ADDR;
            break;

        case USER_DATA_SECTOR3:
        	bWritePermission = TRUE;
        	*ptrMemAddr = USER_DATA_SECT3_ADDR;
            break;

        case BLOCK_FOR_DIAGNOSTIC_TEST:
        	bWritePermission = TRUE;
        	*ptrMemAddr = BLOCK_FOR_TEST_ADDR;
           break;




    }

    return bWritePermission;
}




//*****************************************************************************
// IAP commands codes
//*****************************************************************************

#define	PREPARE_SECTOR_FOR_WRITE_OPERATION	50	
#define	COPY_RAM_TO_FLASH			        51
#define	ERASE_SECTOR				        52
#define	BLANK_CHECK_SECTOR			        53
#define READ_PART_ID				        54
#define	READ_BOOT_CODE_VERSION			    55
#define COMPARE					            56


//*****************************************************************************
// IAP Function Call Location
//     *) Note: the '1' at the end of the address indicates to the h/w that 
//        the code to be executed is 'thumb' based
//*****************************************************************************
// LPC23xx Location
// #define IAP_CODE_LOCATION 0x7ffffff1

// LPC17xx Location
#define IAP_CODE_LOCATION 0x1FFF1FF1


// extern uint32_t SystemCoreClock;
uint32_t SystemCoreClock;

//*****************************************************************************
// IAP command table
//*****************************************************************************
static unsigned long iap_cmd[5];		
        
//*****************************************************************************
// IAP result  table
//*****************************************************************************
static unsigned long iap_res[5];

//*****************************************************************************
//  Call IAP function.
//  
//  Note: Direct XTAL mode is required
//  for IAP functions (PLL turned off)!
//  
//  Return status.
//*****************************************************************************
 
long iap(long code, long p1, long p2, long p3, long p4)
{
	iap_cmd[0] = code;				// set command code
	iap_cmd[1] = p1;				//     1st param
	iap_cmd[2] = p2;				//     2nd param
	iap_cmd[3] = p3;				//     3rd param
	iap_cmd[4] = p4;				//     4th param


	((void (*)())IAP_CODE_LOCATION)(iap_cmd, iap_res);	// IAP entry point

	return *iap_res;				        // return status
}

//*****************************************************************************
// Print IAP error message.
//*****************************************************************************

//******************************************************************************/
//
//  Function:     iap_err
//
//  Arguments:    single character - this is the flash error code
//
//  Returns:      none
//
//  Description:  used to map return codes for the in-flash IAP API provided by 
//                NXP
//
//******************************************************************************/ 
void iap_err(char *s)
{
/*
	static char *const msg[] =
	{
		"INVALID_COMMAND",
		"SRC_ADDR_ERROR",
		"DST_ADDR_ERROR",
		"SRC_ADDR_NOT_MAPPED",
		"DST_ADDR_NOT_MAPPED",
		"COUNT_ERROR",
		"INVALID_SECTOR",
		"SECTOR_NOT_BLANK",
		"SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION",
		"COMPARE_ERROR",
		"BUSY"
	};

	if (*iap_res-1 >= sizeof(msg)/sizeof(*msg)) 
        {
            // printf("%lu: No such error code\n", *iap_res);
        }

	else {
		int i;

		// printf("--ERROR: %s failed (%s)\7\n\n", s, msg[*iap_res-1]);
		for (i = 0; i < 5; i++) 
                {
                    // printf("  iap_cmd[%d] 0x%08lx (%lu)\n", i, iap_cmd[i], iap_cmd[i]);
		}
                for (i = 0; i < 3; i++)
                {
                    // printf("  iap_res[%d] 0x%08lx (%lu)\n", i, iap_res[i], iap_res[i]);
                }
	}
 */
}


//****************************************************************************
// Function name:       part_id
//****************************************************************************
// Descriptions:        gets the part ID for the LPC2378
//
// parameters:	         none
//
// Returned value:      none
//
//***************************************************************************/
void part_id(void)
{
    if (iap(READ_PART_ID, 0, 0, 0, 0)) 
    {
        // iap_err("Reading part ID");
		      return;
    }
}



//****************************************************************************
// Function name:       version
//****************************************************************************
// Descriptions:       Read boot code version.
//
// parameters:	         none
//
// Returned value:      none
//
//***************************************************************************/
void version(void)
{
    if (iap(READ_BOOT_CODE_VERSION, 0, 0, 0, 0)) 
    {
		    // iap_err("Reading boot code version");
		    return;
	   }
}


static DWORD ptrWriteAddr;

//****************************************************************************
// Function name:       s_write
//****************************************************************************
// Descriptions:       	write a sector
//
// parameters:	         none
//
// Returned value:      none
//
//***************************************************************************/
BOOL s_write(unsigned char* ptrBuf, int iBuffSize, BYTE bySector)
{

    int iIndex; 

    long lResult1;
    long lResult2;
    long lResult3;
    static char data[1024];	// data buffer

    BOOL   bIsSectorOKForWrite;


    bIsSectorOKForWrite = SectorHasWritePermission( bySector, &ptrWriteAddr );
    if(!bIsSectorOKForWrite)
        return FALSE;

    for( iIndex = 0; iIndex < 256; iIndex++ )
    {
        data[iIndex] = ptrBuf[iIndex];
    }



    //*********************************************************************
    // If not blank, we do not write
    //*********************************************************************
    if (iap(BLANK_CHECK_SECTOR, bySector, bySector, 0, 0) == 8)
    {
        return FALSE;
    }

    lResult1 = iap(PREPARE_SECTOR_FOR_WRITE_OPERATION, bySector, bySector, 0, 0);
    lResult2 = iap(COPY_RAM_TO_FLASH, ptrWriteAddr, (long)data, 256, SystemCoreClock/1000 );
    lResult3 = iap(COMPARE, ptrWriteAddr, (long)data, iBuffSize, 0);

    if ( lResult1 || lResult2 || lResult3 ) 
    {
        // iap_err("Writing sector");
	    return FALSE;
    }

    return TRUE;

}

static DWORD ptrEraseAddr;
//****************************************************************************
// Function name:       s_erase
//****************************************************************************
// Descriptions:       	Erase sector.
//
// parameters:	         none
//
// Returned value:      none
//
//****************************************************************************
BOOL s_erase(BYTE bySect)
{

    long lResult1;
    long lResult2;
    long lResult3;


    BOOL   bIsSectorOKForWrite;


    bIsSectorOKForWrite = SectorHasWritePermission( bySect, &ptrEraseAddr );
    if(!bIsSectorOKForWrite)
        return FALSE;


    //*************************************************************************
    // If blank, skip erase.
    //*************************************************************************
    if (!iap(BLANK_CHECK_SECTOR, bySect, bySect, 0, 0))
    {
        return TRUE;
    }


    lResult1 = iap(PREPARE_SECTOR_FOR_WRITE_OPERATION, bySect, bySect, 0, 0);
    lResult2 = iap(ERASE_SECTOR, bySect, bySect, SystemCoreClock/1000, 0);
    lResult3 = iap(BLANK_CHECK_SECTOR, bySect, bySect, 0, 0);


    if( lResult1 || lResult2 || lResult3 )
    {
      return FALSE;
    }

    return TRUE;
}


//******************************************************************************
// Device     ASCII/dec coding     Hex coding
//******************************************************************************
// LPC1769     638664503     0x26113F37
// LPC1768     637615927     0x26013F37
// LPC1767     637610039     0x26012837
// LPC1766     637615923     0x26013F33
// LPC1765     637613875     0x26013733
// LPC1764     637606178     0x26011922
// LPC1759     621885239     0x25113737
// LPC1758     620838711     0x25013F37
// LPC1756     620828451     0x25011723
// LPC1754     620828450     0x25011722
// LPC1752     620761377     0x25001121
// LPC1751     620761368     0x25001118
// LPC1751[1]  620761360     0x25001110

#define LPC1769_PART_ID     638664503
#define LPC1768_PART_ID     637615927
#define LPC1767_PART_ID     637610039
#define LPC1766_PART_ID     637615923
#define LPC1765_PART_ID     637613875
#define LPC1764_PART_ID     637606178
#define LPC1759_PART_ID     621885239
#define LPC1758_PART_ID     620838711
#define LPC1756_PART_ID     620828451
#define LPC1754_PART_ID     620828450
#define LPC1752_PART_ID     620761377
#define LPC1751_PART_ID     620761368
#define LPC1751_1_PART_ID   620761360



static unsigned long ulPartID;
static unsigned long ulBootLoaderVersion;

static char ptrTestWriteBuff[] = "abcdefg";
static volatile DWORD  dwRead;

//******************************************************************************
//
//******************************************************************************
BOOL doIAPFlashDiagnostic( void )
{


    BOOL   bPartIdPass;
    DWORD* ptrMem;


    bPartIdPass = FALSE;


    // get part ID and validate
    part_id();
    ulPartID = iap_res[1];

    // check ID against the LPC1765 - that is the usual device for build
    if(ulPartID == LPC1765_PART_ID )
    {
    	bPartIdPass = TRUE;
    }

    // get boot-loader version and store
    version();
    ulBootLoaderVersion = iap_res[1];

    // erase the sector
    s_erase(BLOCK_FOR_DIAGNOSTIC_TEST);
    WaitMsecs(100);

    ptrMem = (DWORD*) BLOCK_FOR_TEST_ADDR;
    dwRead = *ptrMem;

    s_write((unsigned char *)ptrTestWriteBuff, 7, BLOCK_FOR_DIAGNOSTIC_TEST);
    dwRead = *ptrMem;

    return bPartIdPass;

}



#define MAX_USER_FLASH_SECT_SIZE 4096

//******************************************************************************
//
//******************************************************************************
BOOL writeUserFlashSpace( BYTE* ptrTheBUffer, int iBufferSize, BYTE theSector )
{



    BOOL bDidErase;
    BOOL bDidWrite;

	if( iBufferSize > MAX_USER_FLASH_SECT_SIZE )
	{

        return FALSE;
	}


	//*****************************************************************************
	// Erase the sector
	//*****************************************************************************
    bDidErase =  s_erase(theSector);
    if( !bDidErase )
       return FALSE;

    WaitMsecs(1000);

    bDidWrite =  s_write(ptrTheBUffer, iBufferSize, theSector);

    if( bDidWrite )
    {
        return TRUE;
    }

    return FALSE;
}



