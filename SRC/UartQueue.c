//******************************************************************************
//
//  uartQueue.C
//
//      Copyright (c) 2006, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module implements a generic queue for bytes
//
//  Date            Author      Comment
//  ---------------------------------------------------------------------------
//  Mar. 3, 2007    JK          Version 1v1
//******************************************************************************

#include "typedefs.h"
#include "uartQueue.h"


//******************************************************************************/
//
//  Function:    initUartQ
//
//  Arguments:   UART_Q* theQ
//
//  Returns:     void
//
//  Description: this function initializes the members of the UartQ pointed to 
//               by the argument of the function queue; it must be called first
//               before any other operation on the UartQ is attempted
//
//******************************************************************************/

void initUartQ( UART_Q* theQ )
{
    theQ->boolQOverFlow         = FALSE;
    theQ->boolQOverFlowPending  = FALSE;

    theQ->wReadIndex            = 0;
    theQ->wWriteIndex           = 0;
}



//******************************************************************************/
//
//  Function:    Add2UartQ
//
//  Arguments:   BYTE newData, UART_Q* theQ
//
//  Returns:     True if the write succeeds; false othewise
//
//  Description: this function accepts a single byte and a pointer to a UartQ to
//               write the byte to.
//
//******************************************************************************/

BYTE Add2UartQ( BYTE newData, UART_Q* theQ )
{
    if( theQ->boolQOverFlowPending  == TRUE )
    {
        theQ->boolQOverFlow = TRUE; 
        return FALSE;
    }
    else
    {
        theQ->theData[(theQ->wWriteIndex)++] = newData;
     
        if( theQ->wWriteIndex >= UART_Q_SIZE )
        {
            theQ->wWriteIndex = 0;               // reset the index since we have wrapped around the circular Q
        }
         return TRUE;
    }
}




//******************************************************************************/
//
//  Function: blockWriteToUartQ
//
//  Arguments: UART_Q* theQ, BYTE* ptrByteBuffer, WORD wAmountToAdd
//
//  Returns: True if the block write succeeds; false otherwise
//
//  Description: This function writes a block of bytes to the UartQ. The block of 
//               bytes to be copied to the byte queue is pointed to by the argument
//               "ptrByteBuffer"; the argument'wAmountToAdd' indicates the size of 
//               the block
//
//******************************************************************************/

BYTE blockWriteToUartQ( UART_Q* theQ, BYTE* ptrByteBuffer, WORD wAmountToAdd )
{

    WORD wNewWritePointer;
    BYTE boolWrapAround;
    WORD wWriteIndex;
    BYTE* ptrWriteByte;
    BYTE* ptrSrcByte; 
    WORD wAmountToWrite;

    boolWrapAround = FALSE;

    if( wAmountToAdd > UART_Q_SIZE )
    {
        return FALSE;
    }

    wNewWritePointer = theQ->wWriteIndex;
    wNewWritePointer += wAmountToAdd;

    if( wNewWritePointer >= UART_Q_SIZE )
    {
        //*********************************************************************
        // this calculated the write pointer up-to the last position that  
        // will be written: the +1 is required to account for the [0] location
        // in the buffer 
        //*********************************************************************

        wNewWritePointer -= (UART_Q_SIZE );
        boolWrapAround = TRUE;
    }

    if(( theQ->wReadIndex > theQ->wWriteIndex )&& ( wNewWritePointer >=theQ->wReadIndex ) )
    {
        //*********************************************************************
        // in this case, we are trying to write too much data at once;
        // we would overflow the read pointer if we wrote - therefore don't
        //*********************************************************************
        return FALSE;
    }

    if(( theQ->wWriteIndex  > theQ->wReadIndex )&& (boolWrapAround==TRUE ) &&( wNewWritePointer >=theQ->wReadIndex )  )
    {
        //*********************************************************************
        // in this case, we are trying to write too much data at once;
        // we would overflow the read pointer if we wrote - therefore don't
        //*********************************************************************
        return FALSE;
    }

    //*************************************************************************
    // if we have made it this far, it means that there is space in the queue 
    // to write the data - so write the data:
    //*************************************************************************

    ptrWriteByte = (BYTE*) &(theQ->theData[(theQ->wWriteIndex)]);
    ptrSrcByte   = ptrByteBuffer;


    if( boolWrapAround == TRUE )
    {
        wAmountToWrite = UART_Q_SIZE - theQ->wWriteIndex;

        //*********************************************************************
        // write up to the end of the buffer
        //*********************************************************************
        for( wWriteIndex = 0; wWriteIndex < wAmountToWrite; wWriteIndex++ )
        {
            *ptrWriteByte = *ptrSrcByte;
             ptrWriteByte++;
             ptrSrcByte++;
        }
        //*********************************************************************
        // now write the remainder:
        //*********************************************************************
        ptrWriteByte = &(theQ->theData[0]);

        //*********************************************************************
        // write up to the end of the buffer
        //*********************************************************************
        for( wWriteIndex = 0; wWriteIndex < wNewWritePointer; wWriteIndex++ )
        {
            *(ptrWriteByte++) = *(ptrSrcByte++);
        }

    }
    else
    {
        //*********************************************************************
        // write the whole block at once:
        //*********************************************************************
        for( wWriteIndex = 0; wWriteIndex < wAmountToAdd; wWriteIndex++ )
        {
            *(ptrWriteByte++) = *(ptrSrcByte++);
        }
    }

    //*************************************************************************
    // in both cases we set the write pointer to that which we previously 
    // calculated:
    //*************************************************************************
    theQ->wWriteIndex =  wNewWritePointer;

    return TRUE;
}


//******************************************************************************/
//
//  Function: ReadUartQ
//
//  Arguments:  UART_Q* theQ
//
//  Returns: the next byte read from the queue if one is available; if the queue
//           is empty then it returns 'NULL'
//
//  Description:
//
//******************************************************************************/

BYTE ReadUartQ( UART_Q* theQ )
{
    BYTE byTheData;

    theQ->boolQOverFlowPending  = FALSE;

    if( theQ->wReadIndex == theQ->wWriteIndex )
    {
        return NULL;
    }
    else
    {
        byTheData = theQ->theData[(theQ->wReadIndex)++]; 
 
        if( theQ->wReadIndex >= UART_Q_SIZE )
        {
            theQ->wReadIndex  = 0;
        } 

        return byTheData;
    }
}

//******************************************************************************/
//
//  Function: PeekAtUartQ
//
//  Arguments: UART_Q* theQ
//
//  Returns: the next byte read from the queue if one is available; if the queue
//           is empty then it returns 'NULL'. However, the as the 'peek' function
//           it DOESN'T remove the data read from the queue
//
//  Description:
//
//******************************************************************************/
BYTE PeekAtUartQ( UART_Q* theQ )
{
    //*************************************************************************
    // the peek function will be used in order to 'preview' the next pending  byte
    // in the queue. This will allow a function to 'peek' at the data without removing 
    // it from the queue
    //*************************************************************************

    BYTE byTheData;

    byTheData = theQ->theData[(theQ->wReadIndex)]; 
 
    return byTheData;
}

//******************************************************************************/
//
//  Function: UartQHasData
//
//  Arguments: UART_Q* theQ
//
//  Returns: True if there is data to be read in the queue; false otherwise.
//
//  Description:
//
//******************************************************************************/
BYTE UartQHasData( UART_Q* theQ )
{
    if( theQ->wReadIndex != theQ->wWriteIndex)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

//******************************************************************************/
//
//  Function:  GetReadIndexUartQ
//
//  Arguments: UART_Q* theQ
//
//  Returns:   the current read index for the queue
//
//  Description:
//
//******************************************************************************/
BYTE GetReadIndexUartQ( UART_Q* theQ )
{
    return (theQ->wReadIndex);
}

void ResetReadIndexUartQ(BYTE byNewReadIndex,UART_Q* theQ )
{
    theQ->wReadIndex = byNewReadIndex;
}



//******************************************************************************/
//
//  Function:  getUartQOverFlowStatus
//
//  Arguments: UART_Q* theQ 
//
//  Returns:   The current overflow status of the queue
//
//  Description:
//
//******************************************************************************/
BYTE getUartQOverFlowStatus( UART_Q* theQ )
{
    return theQ->boolQOverFlow;
}








