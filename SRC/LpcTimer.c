//******************************************************************************
//
//  LpcTimer.c: LPC Timer Access and Control Module
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module provides access and control of the LPC 32 bit hardware timers.
//  Pclk for the timers is sysclk/4 by default.  DMA support is currently not included.
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2011-05-02      John     Initial Implementation 
//
//******************************************************************************


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "LpcTimer.h"
#include "target.h"


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------

#define TIMER_PCLK_FREQ         (SystemCoreClock/4)
#define TIMER_PRESCALE_VALUE    (TIMER_PCLK_FREQ/(LpcTimerInitData->TimerFrequency) - 1)

// match pin configuration macros (some have multiple pin options, change as required)
#define ENABLE_MAT0_0_PIN       LPC_PINCON->PINSEL3 |= (0x3<<24)
#define ENABLE_MAT0_1_PIN       LPC_PINCON->PINSEL3 |= (0x3<<26)
#define ENABLE_MAT1_0_PIN       LPC_PINCON->PINSEL3 |= (0x3<<12)
#define ENABLE_MAT1_1_PIN       LPC_PINCON->PINSEL3 |= (0x3<<18)
#define ENABLE_MAT2_0_PIN       LPC_PINCON->PINSEL0 |= (0x3<<12)
#define ENABLE_MAT2_1_PIN       LPC_PINCON->PINSEL0 |= (0x3<<14)
#define ENABLE_MAT2_2_PIN       LPC_PINCON->PINSEL0 |= (0x3<<16)
#define ENABLE_MAT2_3_PIN       LPC_PINCON->PINSEL0 |= (0x3<<18)
#define ENABLE_MAT3_0_PIN       LPC_PINCON->PINSEL0 |= (0x3<<20)
#define ENABLE_MAT3_1_PIN       LPC_PINCON->PINSEL0 |= (0x3<<22)

// capture pin configuration macros (some have multiple pin options, change as required)
#define ENABLE_CAP0_0_PIN       LPC_PINCON->PINSEL3 |= (0x3<<20)
#define ENABLE_CAP0_1_PIN       LPC_PINCON->PINSEL3 |= (0x3<<22)
#define ENABLE_CAP1_0_PIN       LPC_PINCON->PINSEL3 |= (0x3<<4)
#define ENABLE_CAP1_1_PIN       LPC_PINCON->PINSEL3 |= (0x3<<6)
#define ENABLE_CAP2_0_PIN       LPC_PINCON->PINSEL0 |= (0x3<<8)
#define ENABLE_CAP2_1_PIN       LPC_PINCON->PINSEL0 |= (0x3<<10)
#define ENABLE_CAP3_0_PIN       LPC_PINCON->PINSEL1 |= (0x3<<14)
#define ENABLE_CAP3_1_PIN       LPC_PINCON->PINSEL1 |= (0x3<<16)

#define TCR_ENABLE_BIT          (1<<0)
#define TCR_RESET_BIT           (1<<1)

#define MCR_INT_EN_BITS         0x0249
#define CCR_INT_EN_BITS         0x0024


//------------------------------------------------------------------------------
//  GLOBAL DECLARATIONS
//------------------------------------------------------------------------------

// array of pointers to the timer base addresses
static LPC_TIM_TypeDef *pLpcTimerRegs[NUM_OF_LPC_TIMERS] =
        {
                (LPC_TIM_TypeDef*) LPC_TIM0_BASE,
                (LPC_TIM_TypeDef*) LPC_TIM1_BASE,
                (LPC_TIM_TypeDef*) LPC_TIM2_BASE,
                (LPC_TIM_TypeDef*) LPC_TIM3_BASE
        };

static BOOL LpcTimerInitialized[NUM_OF_LPC_TIMERS] =
        {
                FALSE,
                FALSE,
                FALSE,
                FALSE
        };

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

void (*Timer0IsrCallBack)( WORD InterruptFlags ) = NULL;
void (*Timer1IsrCallBack)( WORD InterruptFlags ) = NULL;
void (*Timer2IsrCallBack)( WORD InterruptFlags ) = NULL;
void (*Timer3IsrCallBack)( WORD InterruptFlags ) = NULL;


//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS
//------------------------------------------------------------------------------


//******************************************************************************
//
//  Function: LpcTimerInit
//
//  Arguments: TimerNum - timer number (0-3) to initialize
//             LpcTimerInitData - pointer to structure containing initialization data
//
//  Returns: TRUE if successful, else FALSE
//
//  Description: This function will configure and enable the selected h/w timer.
//               Interrupt will only be registered with NVIC if one or more
//               interrupts are enabled in the capture or match control registers.
//
//******************************************************************************
BOOL LpcTimerInit( BYTE TimerNum, LPC_TIMER_INIT_DATA *LpcTimerInitData )
{
    // ensure selected timer module is enabled, PCLK is set
    switch( TimerNum )
    {
        case 0:
            // timer 0 enabled by PCONP bit 1
            LPC_SC->PCONP |= (1<<1);

            // set Timer0 pclk to sysclk/4
// DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
//          LPC_SC->PCLKSEL0 &= ~(3<<2);
            break;

        case 1:
            // timer 1 enabled by PCONP bit 2
            LPC_SC->PCONP |= (1<<2);

            // set Timer1 pclk to sysclk/4
// DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
//          LPC_SC->PCLKSEL0 &= ~(3<<4);
            break;

        case 2:
            // timer 2 enabled by PCONP bit 22
            LPC_SC->PCONP |= (1<<22);

            // set Timer2 pclk to sysclk/4
// DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
//          LPC_SC->PCLKSEL1 &= ~(3<<12);
            break;

        case 3:
            // timer 3 enabled by PCONP bit 23
            LPC_SC->PCONP |= (1<<23);

            // set Timer3 pclk to sysclk/4
// DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
//          LPC_SC->PCLKSEL1 &= ~(3<<14);
            break;

        default:
            // invalid timer number
            return( FALSE );
    }

    // set prescale register (calculate value if TimerFrequency is set)
    if( LpcTimerInitData->TimerFrequency == 0 )
    {
        // no need to calculate prescale value, use PrescaleRegValue
        pLpcTimerRegs[TimerNum]->PR = LpcTimerInitData->PrescaleRegValue;
    }
    else
    {
        // verify TimerFrequency is not greater than PCLK
        if( LpcTimerInitData->TimerFrequency > TIMER_PCLK_FREQ )
        {
            return( FALSE );
        }

        // calculate prescale value
        pLpcTimerRegs[TimerNum]->PR = TIMER_PRESCALE_VALUE;

        // record calculated value in init structure for reference
        LpcTimerInitData->PrescaleRegValue = TIMER_PRESCALE_VALUE;
    }

    // set count control register value
    pLpcTimerRegs[TimerNum]->CTCR = LpcTimerInitData->CountCtrlRegValue;

#ifdef TO_FINISH
we may need to set the pin-states here as well based on whether a capture input
is used to drive the main clocking
#endif



    // set match control register value
    pLpcTimerRegs[TimerNum]->MCR = LpcTimerInitData->MatchCtrlRegValue;

    // set match register values
    pLpcTimerRegs[TimerNum]->MR0 = LpcTimerInitData->MatchRegValue[0];
    pLpcTimerRegs[TimerNum]->MR1 = LpcTimerInitData->MatchRegValue[1];
    pLpcTimerRegs[TimerNum]->MR2 = LpcTimerInitData->MatchRegValue[2];
    pLpcTimerRegs[TimerNum]->MR3 = LpcTimerInitData->MatchRegValue[3];

    // set capture control register value
    pLpcTimerRegs[TimerNum]->CCR = LpcTimerInitData->CaptureCtrlRegValue;

    // set external match register value
    pLpcTimerRegs[TimerNum]->EMR = LpcTimerInitData->ExternalMatchRegValue;

    // configure external match and capture pins as required
    switch( TimerNum )
    {
        case 0:
            if( (LpcTimerInitData->ExternalMatchRegValue & 0x01) != 0 )
            {
                // need to enable MAT0.0
                ENABLE_MAT0_0_PIN;
            }
            if( (LpcTimerInitData->ExternalMatchRegValue & 0x02) != 0 )
            {
                // need to enable MAT0.1
                ENABLE_MAT0_1_PIN;
            }
            if( (LpcTimerInitData->CaptureCtrlRegValue & 0x07) != 0 )
            {
                // need to enable CAP0.0
                ENABLE_CAP0_0_PIN;
            }
            if( (LpcTimerInitData->CaptureCtrlRegValue & 0x38) != 0 )
            {
                // need to enable CAP0.1
                ENABLE_CAP0_1_PIN;
            }

            break;

        case 1:
            if( (LpcTimerInitData->ExternalMatchRegValue & 0x01) != 0 )
            {
                // need to enable MAT1.0
                ENABLE_MAT1_0_PIN;
            }
            if( (LpcTimerInitData->ExternalMatchRegValue & 0x02) != 0 )
            {
                // need to enable MAT1.1
                ENABLE_MAT1_1_PIN;
            }
            if( (LpcTimerInitData->CaptureCtrlRegValue & 0x07) != 0 )
            {
                // need to enable CAP1.0
                ENABLE_CAP1_0_PIN;
            }
            if( (LpcTimerInitData->CaptureCtrlRegValue & 0x38) != 0 )
            {
                // need to enable CAP1.1
                ENABLE_CAP1_1_PIN;
            }

            break;

        case 2:
            if( (LpcTimerInitData->ExternalMatchRegValue & 0x01) != 0 )
            {
                // need to enable MAT2.0
                ENABLE_MAT2_0_PIN;
            }
            if( (LpcTimerInitData->ExternalMatchRegValue & 0x02) != 0 )
            {
                // need to enable MAT2.1
                ENABLE_MAT2_1_PIN;
            }
            if( (LpcTimerInitData->ExternalMatchRegValue & 0x04) != 0 )
            {
                // need to enable MAT2.2
                ENABLE_MAT2_2_PIN;
            }
            if( (LpcTimerInitData->ExternalMatchRegValue & 0x08) != 0 )
            {
                // need to enable MAT2.3
                ENABLE_MAT2_3_PIN;
            }
            if( (LpcTimerInitData->CaptureCtrlRegValue & 0x07) != 0 )
            {
                // need to enable CAP2.0
                ENABLE_CAP2_0_PIN;
            }
            if( (LpcTimerInitData->CaptureCtrlRegValue & 0x38) != 0 )
            {
                // need to enable CAP2.1
                ENABLE_CAP2_1_PIN;
            }

            break;

        case 3:
            if( (LpcTimerInitData->ExternalMatchRegValue & 0x01) != 0 )
            {
                // need to enable MAT3.0
                ENABLE_MAT3_0_PIN;
            }
            if( (LpcTimerInitData->ExternalMatchRegValue & 0x02) != 0 )
            {
                // need to enable MAT3.1
                ENABLE_MAT3_1_PIN;
            }
            if( (LpcTimerInitData->CaptureCtrlRegValue & 0x07) != 0 )
            {
                // need to enable CAP3.0
                ENABLE_CAP3_0_PIN;
            }
            if( (LpcTimerInitData->CaptureCtrlRegValue & 0x38) != 0 )
            {
                // need to enable CAP3.1
                ENABLE_CAP3_1_PIN;
            }

            break;
    }

    // clear interrupt status flags
    pLpcTimerRegs[TimerNum]->IR = LPCTIMER_MR0_IF | LPCTIMER_MR1_IF | LPCTIMER_MR2_IF | LPCTIMER_MR3_IF
            | LPCTIMER_CR0_IF | LPCTIMER_CR1_IF;

    // reset and enable timer
    pLpcTimerRegs[TimerNum]->TCR = TCR_RESET_BIT;
    pLpcTimerRegs[TimerNum]->TCR = TCR_ENABLE_BIT;

    // register ISR callback function and register interrupt with NVIC if any interrupt sources are enabled
    if( ((pLpcTimerRegs[TimerNum]->MCR & MCR_INT_EN_BITS) != 0)
            || ((pLpcTimerRegs[TimerNum]->CCR & CCR_INT_EN_BITS) != 0) )
    {
        switch( TimerNum )
        {
            case 0:
                Timer0IsrCallBack = LpcTimerInitData->IsrCallBack;
                NVIC_EnableIRQ( TIMER0_IRQn );
                NVIC_SetPriority( TIMER0_IRQn, LpcTimerInitData->IntPriority );
                break;

            case 1:
                Timer1IsrCallBack = LpcTimerInitData->IsrCallBack;
                NVIC_EnableIRQ( TIMER1_IRQn );
                NVIC_SetPriority( TIMER1_IRQn, LpcTimerInitData->IntPriority );
                break;

            case 2:
                Timer2IsrCallBack = LpcTimerInitData->IsrCallBack;
                NVIC_EnableIRQ( TIMER2_IRQn );
                NVIC_SetPriority( TIMER2_IRQn, LpcTimerInitData->IntPriority );
                break;

            case 3:
                Timer3IsrCallBack = LpcTimerInitData->IsrCallBack;
                NVIC_EnableIRQ( TIMER3_IRQn );
                NVIC_SetPriority( TIMER3_IRQn, LpcTimerInitData->IntPriority );
                break;
        }
    }

    LpcTimerInitialized[TimerNum] = TRUE;

    return( TRUE );
}


//******************************************************************************
//
//  Function: LpcTimerReset
//
//  Arguments: TimerNum - timer number (0-3) to reset
//
//  Returns: void
//
//  Description: This function will reset the selected h/w timer count value so
//               it will restart from 0.
//
//******************************************************************************
void LpcTimerReset( BYTE TimerNum )
{
    if( LpcTimerInitialized[TimerNum] == TRUE )
    {
        pLpcTimerRegs[TimerNum]->TCR |= TCR_RESET_BIT;
        pLpcTimerRegs[TimerNum]->TCR &= ~TCR_RESET_BIT;
    }
}


//******************************************************************************
//
//  Function: LpcTimerStop
//
//  Arguments: TimerNum - timer number (0-3) to stop
//
//  Returns: void
//
//  Description: This function will stop (disable) the selected h/w timer counter.
//
//******************************************************************************
void LpcTimerStop( BYTE TimerNum )
{
    if( LpcTimerInitialized[TimerNum] == TRUE )
    {
        pLpcTimerRegs[TimerNum]->TCR &= ~TCR_ENABLE_BIT;
    }
}


//******************************************************************************
//
//  Function: LpcTimerStart
//
//  Arguments: TimerNum - timer number (0-3) to start
//
//  Returns: void
//
//  Description: This function will start (enable) the selected h/w timer counter.
//
//******************************************************************************
void LpcTimerStart( BYTE TimerNum )
{
    if( LpcTimerInitialized[TimerNum] == TRUE )
    {
        pLpcTimerRegs[TimerNum]->TCR |= TCR_ENABLE_BIT;
    }
}


//******************************************************************************
//
//  Function: LpcTimerReadCapReg
//
//  Arguments: TimerNum - timer number (0-3) to access
//             CapRegNum - Capture register number (0-1) to read
//
//  Returns: Selected Capture Register Value
//
//  Description: This function will read the selected Capture register value.
//
//
//******************************************************************************
DWORD LpcTimerReadCapReg( BYTE TimerNum, BYTE CapRegNum )
{
    switch( CapRegNum )
    {
        case 0:
            return( pLpcTimerRegs[TimerNum]->CR0 );
            break;

        case 1:
            return( pLpcTimerRegs[TimerNum]->CR1 );
            break;

        default:
            break;
    }

    return( 0 );
}


//******************************************************************************
//
//  Function: LpcTimerReadCountReg
//
//  Arguments: TimerNum - timer number (0-3) to access
//
//  Returns: Selected count Register Value
//
//  Description: This function will read the selected timer count register value.
//
//
//******************************************************************************
DWORD LpcTimerReadCountReg( BYTE TimerNum )
{
    return( pLpcTimerRegs[TimerNum]->TC );
}


//******************************************************************************
//
//  Function: LpcTimerWriteMatchReg
//
//  Arguments: TimerNum - timer number (0-3) to access
//             MatRegNum - match register number (0-4) to write
//             MatchRegValue - data to write to match register
//
//  Returns: void
//
//  Description: This function will write an updated value to the selected
//               match register.
//
//******************************************************************************
void LpcTimerWriteMatchReg( BYTE TimerNum, BYTE MatRegNum, DWORD MatchRegValue )
{
    switch( MatRegNum )
    {
        case 0:
            pLpcTimerRegs[TimerNum]->MR0 = MatchRegValue;
            break;

        case 1:
            pLpcTimerRegs[TimerNum]->MR1 = MatchRegValue;
            break;

        case 2:
            pLpcTimerRegs[TimerNum]->MR2 = MatchRegValue;
            break;

        case 3:
            pLpcTimerRegs[TimerNum]->MR3 = MatchRegValue;
            break;

        default:
            break;
    }
}



//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS
//------------------------------------------------------------------------------


//******************************************************************************
//
//  Function: TIMER0_IRQHandler
//
//  Arguments: none
//
//  Returns: void
//
//  Description: Timer 0 ISR.  Reads & clears interrupt flags and passes flags
//               to the callback function.
//
//******************************************************************************
extern void TIMER0_IRQHandler( void )
{
    DWORD InterruptRegValue;

    // read interrupt flags
    InterruptRegValue = LPC_TIM0->IR;

    // clear interrupt
    LPC_TIM0->IR = InterruptRegValue;

    // call callback function if not NULL
    if( Timer0IsrCallBack != NULL )
    {
        Timer0IsrCallBack( InterruptRegValue );
    }
}


//******************************************************************************
//
//  Function: TIMER1_IRQHandler
//
//  Arguments: none
//
//  Returns: void
//
//  Description: Timer 1 ISR.  Reads & clears interrupt flags and passes flags
//               to the callback function.
//
//******************************************************************************
extern void TIMER1_IRQHandler( void )
{
    DWORD InterruptRegValue;

    // read interrupt flags
    InterruptRegValue = LPC_TIM1->IR;

    // clear interrupt
    LPC_TIM1->IR = InterruptRegValue;

    // call callback function if not NULL
    if( Timer1IsrCallBack != NULL )
    {
        Timer1IsrCallBack( InterruptRegValue );
    }
}


//******************************************************************************
//
//  Function: TIMER2_IRQHandler
//
//  Arguments: none
//
//  Returns: void
//
//  Description: Timer 2 ISR.  Reads & clears interrupt flags and passes flags
//               to the callback function.
//
//******************************************************************************
extern void TIMER2_IRQHandler( void )
{
    DWORD InterruptRegValue;

    // read interrupt flags
    InterruptRegValue = LPC_TIM2->IR;

    // clear interrupt
    LPC_TIM2->IR = InterruptRegValue;

    // call callback function if not NULL
    if( Timer2IsrCallBack != NULL )
    {
        Timer2IsrCallBack( InterruptRegValue );
    }
}


//******************************************************************************
//
//  Function: TIMER3_IRQHandler
//
//  Arguments: none
//
//  Returns: void
//
//  Description: Timer 3 ISR.  Reads & clears interrupt flags and passes flags
//               to the callback function.
//
//******************************************************************************
extern void TIMER3_IRQHandler( void )
{
    DWORD InterruptRegValue;

    // read interrupt flags
    InterruptRegValue = LPC_TIM3->IR;

    // clear interrupt
    LPC_TIM3->IR = InterruptRegValue;

    // call callback function if not NULL
    if( Timer3IsrCallBack != NULL )
    {
        Timer3IsrCallBack( InterruptRegValue );
    }
}
