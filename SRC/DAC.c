//******************************************************************************
//
//  DAC.c: LPC DAC Module
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  Provides setup and control of the LPC UART serial ports.  Currently the
//  following features are not supported - DMA, Autobaud, IrDA and Baud Rate
//  divisor calculations using the fractional divider register.
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-07-19      Jason     Initial Implementation 
//
//******************************************************************************


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "HardwareAbstraction.h"

//
////------------------------------------------------------------------------------
////  CONSTANT & MACRO DEFINITIONS
////------------------------------------------------------------------------------
//
////Set port P0.26 To be the DAC output
//#define ENABLE_DAC_AOUT  LPC_PINCON->PINSEL1 |= (1<<21); LPC_PINCON->PINSEL1 &= ~(1<<20)
//
//
//
//
//
////------------------------------------------------------------------------------
////  GLOBAL DECLARATIONS
////------------------------------------------------------------------------------
//
//
//
//
////------------------------------------------------------------------------------
////  PRIVATE FUNCTION PROTOTYPES
////------------------------------------------------------------------------------
//
//
//
//
////------------------------------------------------------------------------------
////  PUBLIC FUNCTIONS
////------------------------------------------------------------------------------
//void initOnChipDAC( void )
//{
//    ENABLE_DAC_AOUT;
//}
//
//
////------------------------------------------------------------------------------
////
////------------------------------------------------------------------------------
//BOOL updateOnChipDACOutput( WORD wAnalogLevel )
//{
//
//    DWORD dwRegVal;
//
//    // check the data range is correct
//	if( wAnalogLevel > 1023 )
//        return FALSE;
//
//	dwRegVal = wAnalogLevel;
//	dwRegVal <<= 6;
//
//	LPC_DAC->DACR = dwRegVal;
//
//    return TRUE;
//
//}









