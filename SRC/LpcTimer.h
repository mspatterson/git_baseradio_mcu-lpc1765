//******************************************************************************
//
//  LpcTimer.h: LPC Timer Access and Control Module
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module provides access and control of the LPC 32 bit hardware timers.
//  Pclk for the timers is sysclk/4 by default.  DMA support is currently not included.
//
//******************************************************************************

#ifndef LPCTIMER_H_
#define LPCTIMER_H_


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "typedefs.h"



//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------

#define NUM_OF_LPC_TIMERS       4

// interrupt register flags
#define LPCTIMER_MR0_IF         (1<<0)
#define LPCTIMER_MR1_IF         (1<<1)
#define LPCTIMER_MR2_IF         (1<<2)
#define LPCTIMER_MR3_IF         (1<<3)
#define LPCTIMER_CR0_IF         (1<<4)
#define LPCTIMER_CR1_IF         (1<<5)


//------------------------------------------------------------------------------
//  TYPEDEF DECLARATIONS
//------------------------------------------------------------------------------

// use this structure to pass initialization data to LpcTimerInit()
typedef struct
{
    // desired operating frequency of timer in Hz (pclk is sysclk/4 by default)
    // Set to zero if prescale value is being written directly
    DWORD TimerFrequency;
    
    // prescale register value - this will be filled with the prescale value
    // calculated based on TimerFrequency value if TimerFrequency > 0.
    // If TimerFrequency = 0, this value will be used to set prescale register.
    DWORD PrescaleRegValue;
    
    // interrupt call back function, set to NULL if not required
    // the interrupt status register value is passed to this function
    // but the interrupt status will already have been cleared
    // InterruptFlags:  b0 - Match Reg 0 interrupt flag
    //                  b1 - Match Reg 1 interrupt flag
    //                  b2 - Match Reg 2 interrupt flag
    //                  b3 - Match Reg 3 interrupt flag
    //                  b4 - Capture Reg 0 interrupt flag
    //                  b5 - Capture Reg 1 interrupt flag
    void (*IsrCallBack)( WORD InterruptFlags );
    
    // timer interrupt priority, 0 = highest, 31 = lowest
    BYTE IntPriority;

    // value to be written to count control register
        // b1:0 - 00 = timer mode
        //        01 = counter mode, incr on capture rising edge
        //        10 = counter mode, incr on capture falling edge
        //        11 = counter mode, incr on capture both edges
        // b3:2 - 00 = use CAPn.0, 01 = use CAPn.1
    DWORD CountCtrlRegValue;
    
    // Match control register value
        // b0+3n - Match Register n interrupt enabled when set
        // b1+3n - Match Register n reset enable, timer reset on match if bit set
        // b2+3n - Match Register n stop enable, timer stopped on match if bit set
    DWORD MatchCtrlRegValue;
    
    // values for match registers 0-3
    DWORD MatchRegValue[4];
    
    // External Match Register value
        // b3:0 - enternal match pin enables for MATn.3 to MATn.0, set to enable
        // b5:4 - MATn.0 pin ctrl, 00= do nothing, 01= clr pin, 10= set pin, 11= toggle pin
        // b7:6 - MATn.1 pin ctrl, 00= do nothing, 01= clr pin, 10= set pin, 11= toggle pin
        // b9:8 - MATn.2 pin ctrl, 00= do nothing, 01= clr pin, 10= set pin, 11= toggle pin
        // b11:10 - MATn.3 pin ctrl, 00= do nothing, 01= clr pin, 10= set pin, 11= toggle pin
    DWORD ExternalMatchRegValue;
    
    // Capture Control Register value
        // b0 - capture reg 0 enable on rising edge
        // b1 - capture reg 0 enable on falling edge
        // b2 - capture reg 0 interrupt enable
        // b3 - capture reg 1 enable on rising edge
        // b4 - capture reg 1 enable on falling edge
        // b5 - capture reg 1 interrupt enable
    DWORD CaptureCtrlRegValue;
    
} LPC_TIMER_INIT_DATA;



//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS PROTOTYPES
//------------------------------------------------------------------------------


//******************************************************************************
//
//  Function: LpcTimerInit
//
//  Arguments: TimerNum - timer number (0-3) to initialize
//             LpcTimerInitData - pointer to structure containing initialization data
//
//  Returns: TRUE if successful, else FALSE
//
//  Description: This function will configure and enable the selected h/w timer.
//               Interrupt will only be registered with NVIC if one or more
//               interrupts are enabled in the capture or match control registers.
//
//******************************************************************************
BOOL LpcTimerInit( BYTE TimerNum, LPC_TIMER_INIT_DATA *LpcTimerInitData );


//******************************************************************************
//
//  Function: LpcTimerReset
//
//  Arguments: TimerNum - timer number (0-3) to reset
//
//  Returns: void
//
//  Description: This function will reset the selected h/w timer count value so
//               it will restart from 0.
//
//******************************************************************************
void LpcTimerReset( BYTE TimerNum );


//******************************************************************************
//
//  Function: LpcTimerStop
//
//  Arguments: TimerNum - timer number (0-3) to stop
//
//  Returns: void
//
//  Description: This function will stop the selected h/w timer counter.
//
//******************************************************************************
void LpcTimerStop( BYTE TimerNum );


//******************************************************************************
//
//  Function: LpcTimerStart
//
//  Arguments: TimerNum - timer number (0-3) to start
//
//  Returns: void
//
//  Description: This function will start the selected h/w timer counter.
//
//******************************************************************************
void LpcTimerStart( BYTE TimerNum );


//******************************************************************************
//
//  Function: LpcTimerReadCapReg
//
//  Arguments: TimerNum - timer number (0-3) to access
//             CapRegNum - Capture register number (0-1) to read
//
//  Returns: Selected Capture Register Value
//
//  Description: This function will read the selected Capture register value.
//
//
//******************************************************************************
DWORD LpcTimerReadCapReg( BYTE TimerNum, BYTE CapRegNum );


//******************************************************************************
//
//  Function: LpcTimerReadCountReg
//
//  Arguments: TimerNum - timer number (0-3) to access
//
//  Returns: Selected count Register Value
//
//  Description: This function will read the selected timer count register value.
//
//
//******************************************************************************
DWORD LpcTimerReadCountReg( BYTE TimerNum );


//******************************************************************************
//
//  Function: LpcTimerWriteMatchReg
//
//  Arguments: TimerNum - timer number (0-3) to access
//             MatRegNum - match register number (0-4) to write
//             MatchRegValue - data to write to match register
//
//  Returns: void
//
//  Description: This function will write an updated value to the selected 
//               match register.
//
//******************************************************************************
void LpcTimerWriteMatchReg( BYTE TimerNum, BYTE MatRegNum, DWORD MatchRegValue );


#endif /* LPCTIMER_H_ */
