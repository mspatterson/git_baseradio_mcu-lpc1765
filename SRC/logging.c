#include <string.h>

#include "logging.h"
#include "HardwareAbstraction.h"
#include "UartManager.h"


static UART_MGR_OBJECT myUartPortDebug;
static SERIAL_CFG      cfgInfoDebugSerPort;

// We maintain a small linear buffer in the case of back to back writes to the debug port
#define DEBUG_BUFF_LEN  256
static BYTE pbyDebugBuff[DEBUG_BUFF_LEN];


static void testPortTxISRDebugSerPort( void )
{
    // In the 'normal' build, let the default Tx ISR handle this event
    UartTxISR( &myUartPortDebug );

}

static void testPortRxISRDebugSerPort( BOOL bIntWasTimeOut )
{
    UartRxISR( &myUartPortDebug );

}

void InitLogging( void )
{
    BOOL bPortDidInit;

    myUartPortDebug.byPortNum = UART_DEBUG_PN;
    myUartPortDebug.UartRxDataCallBack = testPortRxISRDebugSerPort;
    myUartPortDebug.UartTxDataCallBack = testPortTxISRDebugSerPort;

    InitUartManager( &myUartPortDebug );

    cfgInfoDebugSerPort.dwSpeed   = 19200;
    cfgInfoDebugSerPort.wDataBits = 8;
    cfgInfoDebugSerPort.wParity   = PARITY_NONE;
    cfgInfoDebugSerPort.wStopBits = ONE_STOP_BIT;
    cfgInfoDebugSerPort.wFlowCntl = NO_FLOW_CNTL;

    bPortDidInit = OpenUartSerialPort( &myUartPortDebug, &cfgInfoDebugSerPort );
}


BOOL WriteToDebugPort( const BYTE* pBuff, WORD buffLen )
{
    // If we are currently sending, we need to append what we can to the buffer.
    // If we aren't sending, just copy to the buffer and start



    // With this build option the debug port behaves as expected

    // Disable interrupts while accessing the queue
    DisableInts();

    if( myUartPortDebug.bTxQSending )
    {
        WORD bytesToCopy;

        if( myUartPortDebug.wTxQSize + buffLen <= DEBUG_BUFF_LEN )
            bytesToCopy = buffLen;
        else
            bytesToCopy = DEBUG_BUFF_LEN - myUartPortDebug.wTxQSize;

        memcpy( &(pbyDebugBuff[myUartPortDebug.wTxQSize]), pBuff, bytesToCopy );

        myUartPortDebug.wTxQSize += bytesToCopy;

        // Tack on "\r\n" if we have room
        if( myUartPortDebug.wTxQSize < DEBUG_BUFF_LEN )
        {
            pbyDebugBuff[myUartPortDebug.wTxQSize]   = '\r';
            pbyDebugBuff[myUartPortDebug.wTxQSize+1] = '\n';

            myUartPortDebug.wTxQSize += 2;
        }
    }
    else
    {
        // Copy the buffer across. We tack on "\r\n" if there is room
        WORD bytesToCopy;

        if( buffLen < DEBUG_BUFF_LEN )
            bytesToCopy = buffLen;
        else
            bytesToCopy = DEBUG_BUFF_LEN;

        memcpy( pbyDebugBuff, pBuff, bytesToCopy );

        if( bytesToCopy + 2 <= DEBUG_BUFF_LEN )
        {
            pbyDebugBuff[bytesToCopy]   = '\r';
            pbyDebugBuff[bytesToCopy+1] = '\n';

            bytesToCopy += 2;
        }

        UartPortSendBuffer( &myUartPortDebug, pbyDebugBuff, bytesToCopy );
    }

    EnableInts();

    return TRUE;

}


BOOL DebugPortSending( void )
{
    // With this build option, return the actual state of the transmit queue
    return UartTxQueueSending( &myUartPortDebug );
}
