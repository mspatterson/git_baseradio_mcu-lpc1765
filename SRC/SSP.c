//******************************************************************************
//
//  SSP.c:  LPC17xx/LPC177x/178x SSP Peripheral Module
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module provides configuration and control of the internal SSP peripheral.
//  Currently only supports master mode.  This does not cover the SSP peripheral
//  so there is no FIFO for receive or transmit.
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-03-01      JD/JK     Initial Implementation
//
//******************************************************************************


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "SSP.h"
#include "target.h"


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------
#define TRANSMIT_FIFO_EMPTY      (1<<0)
#define TRANSMIT_FIFO_NOT_FULL   (1<<1)
#define RECEIVE_FIFO_NOT_EMPTY   (1<<2)




#define SCR_DEFAULT 0
#define SSP_INT_ENABLE_MASK 0x0000000F

#define SSP_ENABLE 0x00000002


// #if (!(TARGET == LPC177x_78x ) )
    // PIN enable macros

/*
    #define SCK_PIN_ENABLE      LPC_PINCON->PINSEL0 |= (0x3<<30)
    #define MISO_PIN_ENABLE     LPC_PINCON->PINSEL1 |= (0x3<<2)
    #define MOSI_PIN_ENABLE     LPC_PINCON->PINSEL1 |= (0x3<<4)
    #define SSEL_PIN_ENABLE     LPC_PINCON->PINSEL1 |= (0x3<<0)

    // SSP1


    #define SSEL1_PIN_ENABLE       LPC_PINCON->
    #define SCK1_PIN_ENABLE        LPC_PINCON->
    #define MISO1_PIN_ENABLE       LPC_PINCON->
    #define MOSI1_PIN_ENABLE       LPC_PINCON->
*/


    // SSP1
    #define SSEL1_FUNC_MASK   0x00003000
    #define SSEL1_FUNC_SELECT 0x00002000

    #define SCK1_FUNC_MASK    0x0000C000
    #define SCK1_FUNC_SELECT  0x00008000

    #define MISO1_FUNC_MASK   0x00030000
    #define MISO1_FUNC_SELECT 0x00020000

    #define MOSI1_FUNC_MASK   0x000C0000
    #define MOSI1_FUNC_SELECT 0x00080000



    #define SCK1_PIN_ENABLE        LPC_PINCON->PINSEL0&=(~SCK1_FUNC_MASK);LPC_PINCON->PINSEL0|=SCK1_FUNC_SELECT;
    #define SSEL1_PIN_ENABLE       LPC_PINCON->PINSEL0&=(~SSEL1_FUNC_MASK );LPC_PINCON->PINSEL0|=SSEL1_FUNC_SELECT;
    #define MISO1_PIN_ENABLE       LPC_PINCON->PINSEL0&=(~MISO1_FUNC_MASK );LPC_PINCON->PINSEL0|=MISO1_FUNC_SELECT;
    #define MOSI1_PIN_ENABLE       LPC_PINCON->PINSEL0&=(~MOSI1_FUNC_MASK );LPC_PINCON->PINSEL0|=MOSI1_FUNC_SELECT;




    // SSP0
    //*************************************************************************


    // SCK0   - P1[20]
    #define SCK0_FUNC_SELECT  0x00000300
    #define SCK0_FUNC_MASK    0x00000300
    #define SCK0_PIN_ENABLE   LPC_PINCON->PINSEL3&=(~SCK0_FUNC_MASK);LPC_PINCON->PINSEL3|=SCK0_FUNC_SELECT;

    // SSEL0  - P1[21]
    #define SSEL0_FUNC_SELECT 0x00000C00
    #define SSEL0_FUNC_MASK   0x00000C00
    #define SSEL0_PIN_ENABLE  LPC_PINCON->PINSEL3&=(~SSEL0_FUNC_MASK );LPC_PINCON->PINSEL3|=SSEL0_FUNC_SELECT;

    // MISO0  - P1[23]
    #define MISO0_FUNC_SELECT 0x0000C000
    #define MISO0_FUNC_MASK   0x0000C000
    #define MISO0_PIN_ENABLE  LPC_PINCON->PINSEL3&=(~MISO0_FUNC_MASK );LPC_PINCON->PINSEL3|=MISO0_FUNC_SELECT;

    // MOSI0  - P1[24]
    #define MOSI0_FUNC_SELECT 0x00030000
    #define MOSI0_FUNC_MASK   0x00030000
    #define MOSI0_PIN_ENABLE  LPC_PINCON->PINSEL3&=(~MOSI0_FUNC_MASK );LPC_PINCON->PINSEL3|=MOSI0_FUNC_SELECT;


/*
    // SCK0   - P0[15]
    #define SCK0_FUNC_SELECT  0x80000000
    #define SCK0_FUNC_MASK    0xC0000000
    #define SCK0_PIN_ENABLE   LPC_PINCON->PINSEL0&=(~SCK0_FUNC_MASK);LPC_PINCON->PINSEL0|=SCK0_FUNC_SELECT;

    // SSEL0  - P0[16]
    #define SSEL0_FUNC_SELECT 0x00000002
    #define SSEL0_FUNC_MASK   0x00000003
    #define SSEL0_PIN_ENABLE  LPC_PINCON->PINSEL1&=(~SSEL0_FUNC_MASK );LPC_PINCON->PINSEL1|=SSEL0_FUNC_SELECT;



    // MISO0  - P0[17]
    #define MISO0_FUNC_SELECT 0x00000008
    #define MISO0_FUNC_MASK   0x0000000C
    #define MISO0_PIN_ENABLE  LPC_PINCON->PINSEL1&=(~MISO0_FUNC_MASK );LPC_PINCON->PINSEL1|=MISO0_FUNC_SELECT;

    // MOSI0  - P0[18]
    #define MOSI0_FUNC_SELECT 0x00000020
    #define MOSI0_FUNC_MASK   0x00000030
    #define MOSI0_PIN_ENABLE  LPC_PINCON->PINSEL1&=(~MOSI0_FUNC_MASK );LPC_PINCON->PINSEL1|=MOSI0_FUNC_SELECT;
*/




/*
#else

    #define IOCON_FUNC_MASK   0xFFFFFFF8

    // SSP1
    #define SSEL1_FUNC_SELECT 0x00000002
    #define SCK1_FUNC_SELECT  0x00000002
    #define MISO1_FUNC_SELECT 0x00000002
    #define MOSI1_FUNC_SELECT 0x00000002

    #define SSEL1_PIN_ENABLE       LPC_IOCON->P0_6&=IOCON_FUNC_MASK;LPC_IOCON->P0_6|=SSEL1_FUNC_SELECT
    #define SCK1_PIN_ENABLE        LPC_IOCON->P0_7&=IOCON_FUNC_MASK;LPC_IOCON->P0_7|=SCK1_FUNC_SELECT
    #define MISO1_PIN_ENABLE       LPC_IOCON->P0_8&=IOCON_FUNC_MASK;LPC_IOCON->P0_8|=MISO1_FUNC_SELECT
    #define MOSI1_PIN_ENABLE       LPC_IOCON->P0_9&=IOCON_FUNC_MASK;LPC_IOCON->P0_9|=MOSI1_FUNC_SELECT


    // SSP0
    #define SCK0_FUNC_SELECT  0x00000005
    #define SSEL0_FUNC_SELECT 0x00000003
    #define MISO0_FUNC_SELECT 0x00000005
    #define MOSI0_FUNC_SELECT 0x00000005


    #define SCK0_PIN_ENABLE        LPC_IOCON->P1_20&=IOCON_FUNC_MASK;LPC_IOCON->P1_20 |=SCK0_FUNC_SELECT
    #define SSEL0_PIN_ENABLE       LPC_IOCON->P1_21&=IOCON_FUNC_MASK;LPC_IOCON->P1_21 |=SSEL0_FUNC_SELECT
    #define MISO0_PIN_ENABLE       LPC_IOCON->P1_23&=IOCON_FUNC_MASK;LPC_IOCON->P1_23 |=MISO0_FUNC_SELECT
    #define MOSI0_PIN_ENABLE       LPC_IOCON->P1_24&=IOCON_FUNC_MASK;LPC_IOCON->P1_24 |=MOSI0_FUNC_SELECT


#endif
*/

//------------------------------------------------------------------------------
//  GLOBAL DECLARATIONS
//------------------------------------------------------------------------------

void (*SSP0IsrCallback)( WORD SSPData );
void (*SSP1IsrCallback)( WORD SSPData );
void (*SSP2IsrCallback)( WORD SSPData );

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
LPC_SSP_TypeDef* mapSSPPortNumToControlStruct( BYTE byPortNum );


//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS
//------------------------------------------------------------------------------
#define CPHA_BIT_POS 7
#define SSP_CPHA_BIT (1<<CPHA_BIT_POS)

#define CPOL_BIT_POS 6
#define SSP_CPOL_BIT (1<<CPOL_BIT_POS)


//******************************************************************************
//
//******************************************************************************
BOOL SSPSetClockPhase(  BYTE bySSPPort, BYTE byClockOutPhase )
{

	DWORD dwConReg0;
    LPC_SSP_TypeDef* LPC_SSP;

    // get a pointer to the control struct for this port - and check it
    LPC_SSP = mapSSPPortNumToControlStruct( bySSPPort );
    if( LPC_SSP == NULL )
        return FALSE;

    // check the polarity setting is OK
    if( byClockOutPhase > SSP_CPHA_2ND_EDGE )
        return FALSE;


    dwConReg0 =  LPC_SSP->CR0;

    //clear the clock phase bit & set it according to the new setting
    dwConReg0 &=(~SSP_CPHA_BIT);

    // set it according to the new parameter
    dwConReg0 |= (byClockOutPhase<<CPHA_BIT_POS);

    // write the control reg values
    LPC_SSP->CR0 = dwConReg0;

    return TRUE;

}



//******************************************************************************
//
//******************************************************************************
BOOL SSPSetClockPolarity(  BYTE bySSPPort, BYTE byClockOutPolarity )
{

	DWORD dwConReg0;
    LPC_SSP_TypeDef* LPC_SSP;

    // get a pointer to the control struct for this port - and check it
    LPC_SSP = mapSSPPortNumToControlStruct( bySSPPort );
    if( LPC_SSP == NULL )
        return FALSE;

    // check the polarity setting is OK
    if( byClockOutPolarity > SSP_CPOL_INTERFRAME_HIGH )
        return FALSE;

    // #define SSP_CPOL_INTERFRAME_LOW   0x00


    dwConReg0 =  LPC_SSP->CR0;

    //clear the clock phase bit & set it according to the new setting
    dwConReg0 &=(~SSP_CPOL_BIT);

    // set it according to the new parameter
    dwConReg0 |= (byClockOutPolarity<<CPOL_BIT_POS);

    // write the control reg values
    LPC_SSP->CR0 = dwConReg0;

    return TRUE;

}



//******************************************************************************
//
//  Function: SSPInit
//
//  Arguments: CtrlRegValue - value to be written to SPI ctrl reg
//                          b2 set to select word lengths other than 8 bits
//                          b3 clock phase ctrl
//                          b4 clock polarity ctrl
//                          b5 set to one to select master mode
//                          b6 0=MSb first, 1=LSb first
//                          b7 set to enable interrupt generation
//                          b11:8 word length select, 8-16 bits (0b1000=8, 0b1111=15, 0b0000=16)
//             SckFreq - desired SCK frequency in Hz
//                       (max is pclk/8, min is pclk/255, by default pclk = sysclk/4)
//             PinEnables - enables for each SPI pin (normally SCLK will always
//                          be enabled, but depending on usage others may not)
//                          b0 set to enable SCLK pin (SPI_SCK_EN_BIT)
//                          b1 set to enable MISO pin (SPI_MISO_EN_BIT)
//                          b2 set to enable MOSI pin (SPI_MOSI_EN_BIT)
//                          b3 set to enable SSEL pin (SPI_SSEL_EN_BIT)
//             IntPriority - SPI interrupt priority, 0 = highest, 31 = lowest
//             IsrCallBack - function pointer to SPI ISR callback function (use NULL for none)
//                           (newly received data is passed to this function)
//
//  Returns: TRUE if initialization successful, else FALSE
//
//  Description: This function will initialize the SPI port according to the
//               passed parameters.  Note that if SSEL can be left disabled if
//               not used, or if there are multiple slaves on bus.  In that case
//               the calling function will need to manually control the GPIO
//               driving each SSEL.
//
//******************************************************************************

BOOL SSPInit( BYTE bySSPPort,
              BYTE byDataSize,
              BYTE byFrameFormat,
              BYTE byClockOutPolarity,
              BYTE byClockOutPhase,
              BOOL bLoopBackEnable,
              BOOL bSSPIsMaster,
              BOOL bSlaveOutDisable,
              DWORD dwInterruptEnable,
              DWORD SckFreq,
              BYTE PinEnables,
              BYTE IntPriority,
              void (*IsrCallBack)( WORD SpiData ) )
{

    DWORD dwTemp;
    DWORD dwConReg0;
    DWORD dwConReg1;
    DWORD DividerVal;
    DWORD dwPCLKForDivCalc;


    LPC_SSP_TypeDef* LPC_SSP;



    // set SPI pclk to sysclk div by 'X'
    // DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
    // Thus, check bits '11' and '10' of the 'PCLKSEL1_Val' in system_LPC17xx.c


    switch( bySSPPort )
    {

        case 0:

            LPC_SSP = LPC_SSP0;
            LPC_SC->PCONP |= (1<<21);

            // configure pins for SPI as specified by PinEnables
            if( (PinEnables & SSP_SCK_EN_BIT) != 0)
            {
                SCK0_PIN_ENABLE;
            }
            if( (PinEnables & SSP_MISO_EN_BIT) != 0)
            {
                MISO0_PIN_ENABLE;
            }
            if( (PinEnables & SSP_MOSI_EN_BIT) != 0)
            {
                MOSI0_PIN_ENABLE;
            }
            if( (PinEnables & SSP_SSEL_EN_BIT) != 0)
            {
                SSEL0_PIN_ENABLE;
            }

            SSP0IsrCallback  = IsrCallBack;

            break;

        case 1:

            // enable power to SSSP
            LPC_SC->PCONP |= (1<<10);
            LPC_SSP = LPC_SSP1;


            // configure pins for SPI as specified by PinEnables
            if( (PinEnables & SSP_SCK_EN_BIT) != 0)
            {
                SCK1_PIN_ENABLE;
            }
            if( (PinEnables & SSP_MISO_EN_BIT) != 0)
            {
                MISO1_PIN_ENABLE;
            }
            if( (PinEnables & SSP_MOSI_EN_BIT) != 0)
            {
                MOSI1_PIN_ENABLE;
            }
            if( (PinEnables & SSP_SSEL_EN_BIT) != 0)
            {
                SSEL1_PIN_ENABLE;
            }

            SSP1IsrCallback = IsrCallBack;

            break;

        case 2:
/*
            // LPC_SSP = LPC_SSP2;
            LPC_SC->PCONP |= (1<<20);

            // note: pin select must go here!!!

            // SSP2IsrCallback = IsrCallBack;
            return FALSE;
*/
            break;
        default:
            return FALSE;
            break;
    }



    dwConReg0 = 0;

    // Data Transfer Size
    if( (byDataSize < 4) || (byDataSize > 16) )
        return NULL;
    dwConReg0 = byDataSize - 1;

    // Frame Format
    if( byFrameFormat > SSP_FORMAT_MICRO_WIRE )
        return NULL;
    dwTemp     = byFrameFormat << 4;
    dwConReg0 |= dwTemp;

    // Clock Out Polarity
    if( byClockOutPolarity > SSP_CPOL_INTERFRAME_HIGH )
        return NULL;
    dwTemp     = byClockOutPolarity << 6;
    dwConReg0 |= dwTemp;

    // Clock Out Phase
    if( byClockOutPhase > SSP_CPHA_2ND_EDGE )
        return NULL;
    dwTemp     = byClockOutPhase << 7;
    dwConReg0 |= dwTemp;


    // SPS Clock Frequency Handling:
    // Bit Frequency = PCLK / (CPSDVSR x [SCR+1])
    // *) SCR = Number of prescaler-output clocks per bit (minus 1);
    //    i.e., SCR = 3 means 4 prescaler-output clocks
    // For this implementation we hard-code the SCR value and calculate the
    // the CPSDVR

    // calculate SCK divider value
    // DividerVal = (SystemCoreClock/PCLK_DIVIDER)/( (SCR_DEFAULT+1)*(SckFreq) );
    
    switch( bySSPPort )
    {
        case 0:
            dwPCLKForDivCalc = PeripheralClockForSSP0;
            break;

        default:
            dwPCLKForDivCalc = PeripheralClock;
        break;
    }

    DividerVal = dwPCLKForDivCalc / ( (SCR_DEFAULT+1)*(SckFreq) );


    if( (DividerVal < 2) || (DividerVal > 254) )
    {
        // illegal divider value
        return( FALSE );
    }

    LPC_SSP->CPSR = DividerVal;


    dwConReg1 = 0;
    if( bLoopBackEnable )
        dwConReg1 |= SSP_LOOPBACK_ENABLE;

    // Slave Enable and Slave output Disable
    if( !bSSPIsMaster )
    {
        dwConReg1 |= SSP_IS_SLAVE;
        if( bSlaveOutDisable )
            dwConReg1 |= SSP_SLAVE_OUT_DISABLE;
    }



    // write the control reg values
    LPC_SSP->CR0 = dwConReg0;
    LPC_SSP->CR1 = dwConReg1;

#ifdef TO_BE_COMPLETED
    // if interrupt is enabled, register with NVIC
    if( ( dwInterruptEnable & SSP_INT_ENABLE_MASK ) != 0 )
    {
            NVIC_EnableIRQ( SSP1_IRQn );
            NVIC_SetPriority( SSP1_IRQn, IntPriority );
            LPC_SSP->IMSC = ( dwInterruptEnable & SSP_INT_ENABLE_MASK );
    }
#endif

    return( TRUE );
}


//******************************************************************************
//
//  Function: SSPEnable
//
//  Arguments: BYTE byPortNum - the SSP Port being selected
//
//  Returns: void
//
//  Description: This function enables the port for tx/rx capability; this function
//               should be called after 'SSPInit(...);' above, and before
//               attempting to send or receive data on the port.
//
//******************************************************************************
void SSPEnable( BYTE byPortNum )
{

    LPC_SSP_TypeDef* LPC_SSP;

    LPC_SSP = mapSSPPortNumToControlStruct(byPortNum);

    if( LPC_SSP != NULL )
        LPC_SSP->CR1 |= SSP_ENABLE;

}


//******************************************************************************
//
//  Function: SSPDisable
//
//  Arguments: BYTE byPortNum - the SSP Port being selected
//
//  Returns: void
//
//  Description: This function disables the port for tx/rx capability; .
//
//******************************************************************************
void SSPDisable( BYTE byPortNum )
{

    LPC_SSP_TypeDef* LPC_SSP;

    LPC_SSP = mapSSPPortNumToControlStruct(byPortNum);

    if( LPC_SSP != NULL )
        LPC_SSP->CR1 &= (~SSP_ENABLE);

}



//******************************************************************************
//
//  Function: SSPWriteData
//
//  Arguments: SSPData - data to be written to SSP data register
//
//  Returns: void
//
//  Description: This function will write to the SSP data register, resulting in
//               the data being transmitted on the SSP bus.  This function must
//               not be called during an active SSP transfer, or the current
//               transfer will be corrupted.
//
//******************************************************************************
void SSPWriteData( BYTE bySSPPort, WORD SSPData )
{

    LPC_SSP_TypeDef* LPC_SSP;

    LPC_SSP = mapSSPPortNumToControlStruct(bySSPPort);


    if( LPC_SSP != NULL )
        LPC_SSP->DR = SSPData;


}


//******************************************************************************
//
//  Function: SSPTransferDone
//
//  Arguments: none
//
//  Returns: TRUE if current SSP transfer is complete, else FALSE
//
//  Description: This function will check the transfer complete status of the
//               SSP bus.  SSPReadData or SSPWriteData must be called in order
//               to clear the transfer done status.
//
//******************************************************************************
BOOL SSPTransferDone( BYTE bySSPPort )
{
    LPC_SSP_TypeDef* LPC_SSP = mapSSPPortNumToControlStruct(bySSPPort);

    BOOL bIsDone = FALSE;

    if( LPC_SSP != NULL )
    {
        if( (LPC_SSP->SR & TRANSMIT_FIFO_EMPTY) != 0 )
            bIsDone = TRUE;
    }

    return( bIsDone );
}


//******************************************************************************
//
//  Function: SSPReadData
//
//  Arguments: none
//
//  Returns: Data read from SSP data register
//
//  Description: This function will read the current data in the SSP data register.
//               This will be the SSP receive data if a SSP transfer is complete.
//               This function should not be called during an active SSP transfer.
//
//******************************************************************************
WORD SSPReadData( BYTE bySSPPort )
{
    LPC_SSP_TypeDef* LPC_SSP = mapSSPPortNumToControlStruct(bySSPPort);

    if( LPC_SSP != NULL )
        return( LPC_SSP->DR );

    // Fall through means undefined port
    return 0xFFFF;
}




//*****************************************************************************
// This function is a generic handler to transmit a buffer and receive a buffer
// simultaneously
//
//*****************************************************************************
void SSPSendAndReceive( BYTE bySSPPort, BYTE* ptrSendData, BYTE* ptrRxBuffer, WORD wTransSize )
{

    WORD wTxIndex;
    WORD wRxIndex;
    LPC_SSP_TypeDef* LPC_SSP;


    LPC_SSP = mapSSPPortNumToControlStruct(bySSPPort);

    if( LPC_SSP == NULL )
        return;

    wTxIndex = 0;
    wRxIndex = 0;


    while( ( wTxIndex < wTransSize  ) || ( wRxIndex < wTransSize ) )
    {

        // now: if there is data to send and the Tx Fifo has space, we send it
        if( ( LPC_SSP->SR & TRANSMIT_FIFO_NOT_FULL )&& ( wTxIndex < wTransSize ) )
        {
            LPC_SSP->DR = ptrSendData[wTxIndex++];
        }

        // if the rx fifo has data we receive it
        if( ( LPC_SSP->SR & RECEIVE_FIFO_NOT_EMPTY ) && ( wRxIndex < wTransSize ) )
        {
            ptrRxBuffer[wRxIndex++] = LPC_SSP->DR;
        }

    }


}




//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS
//------------------------------------------------------------------------------

//******************************************************************************
//
//  Function: SSP_IRQHandler
//
//  Arguments:
//
//  Returns:
//
//  Description: SSP ISR - calls provided callback function if available
//
//******************************************************************************
extern void SSP_IRQHandler( void )
{


#ifdef TO_BE_REVISED


    volatile int DataReg;

    // write one to interrupt status register to clear IRQ
    LPC_SSP->SPINT = 1;

    // status register must be read to clear transfer complete flag
    DataReg = LPC_SSP->SR;

    // read data register to clear transfer complete flag
    DataReg = LPC_SSP->DR;
    

SSP0IsrCallback
SSP1IsrCallback
SSP2IsrCallback

    if( SSPIsrCallback != NULL )
    {
        // pass new data to callback function
        SSPIsrCallback( DataReg );
    }
#endif
}



//******************************************************************************
//
//  Function: mapSSPPortNumToControlStruct
//
//  Arguments: port number: 0,1,2 are valid; all others invalid
//
//  Returns: LPC_SSP_TypeDef* - pointer to the control struct for the SSP port
//
//  Description:
//
//******************************************************************************

LPC_SSP_TypeDef* mapSSPPortNumToControlStruct( BYTE byPortNum )
{

    LPC_SSP_TypeDef* LPC_SSP;

    switch( byPortNum )
    {
        case 0:
            LPC_SSP = LPC_SSP0;
            break;
        case 1:
            LPC_SSP = LPC_SSP1;

            break;
        case 2:
            LPC_SSP = NULL;
            // LPC_SSP2->DR = SSPData;
            break;
        default:
            LPC_SSP = NULL;
            break;
    }

    return LPC_SSP;

}

