//******************************************************************************
//
//  CommsInterface.c:
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
// Contains the wrapper for generating an arbitrary waveform (which will usually
// be a ramp function)
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  Version 1.0     JK      July 9, 2012
//
//******************************************************************************




#include <string.h>


#include "HardwareAbstraction.h"
#include "UartManager.h"
#include "CRC-CCITT16.h"
// #include "DataManager.h"



static UART_MGR_OBJECT myCmdCommsPort;
static SERIAL_CFG      cfgInfoCommandSerPort;

// We maintain a small linear buffer in the case of back to back writes to the debug port
#define COMMS_BUFF_LEN  256
static BYTE pbyCmdCommsBuff[COMMS_BUFF_LEN];




//******************************************************************************
// Private Forward Declarations
//******************************************************************************
void processCommandHeader(PKT_HEADER* ptrPkt);
BOOL processPktWithPayload( PKT_HEADER* ptrPktHdr, BYTE* ptrPayload, WORD wPayldSize );
void sendErrorResponse( PKT_HEADER* ptrRxPkt, BYTE byTheError );



//******************************************************************************
//
//******************************************************************************
static void commsPortTxISR( void )
{
    // In the 'normal' build, let the default Tx ISR handle this event
    UartTxISR( &myCmdCommsPort );

}

//******************************************************************************
//
//******************************************************************************
static void commsPortRxISR( BOOL bIntWasTimeOut )
{
    UartRxISR( &myCmdCommsPort );

}


//******************************************************************************
//
//******************************************************************************
#define CMD_CMS_RX_HDR_1_WAITING 0x01
#define CMD_CMS_RX_HDR_2_WAITING 0x02
#define CMD_CMS_RX_HDR_RXING     0x04
#define CMD_CMS_RX_PAYLD_RXING   0x08

static BYTE byCmdCommsRxState = CMD_CMS_RX_HDR_1_WAITING;




//******************************************************************************
//
//******************************************************************************
void InitCommandCommsInterface( void )
{
    BOOL bPortDidInit;

    myCmdCommsPort.byPortNum = CMD_COMMS_PN;
    myCmdCommsPort.UartRxDataCallBack = commsPortRxISR;
    myCmdCommsPort.UartTxDataCallBack = commsPortTxISR;

    InitUartManager( &myCmdCommsPort );

    cfgInfoCommandSerPort.dwSpeed   = CMD_COMMS_BAUD_RATE;
    cfgInfoCommandSerPort.wDataBits = 8;
    cfgInfoCommandSerPort.wParity   = PARITY_NONE;
    cfgInfoCommandSerPort.wStopBits = ONE_STOP_BIT;
    cfgInfoCommandSerPort.wFlowCntl = NO_FLOW_CNTL;

    bPortDidInit = OpenUartSerialPort( &myCmdCommsPort, &cfgInfoCommandSerPort );

    byCmdCommsRxState = CMD_CMS_RX_HDR_1_WAITING;

}


//******************************************************************************
//
//******************************************************************************
BOOL WriteToCmdCommsPort( const BYTE* pBuff, WORD buffLen )
{
    // If we are currently sending, we need to append what we can to the buffer.
    // If we aren't sending, just copy to the buffer and start



    // With this build option the debug port behaves as expected

    // Disable interrupts while accessing the queue
    DisableInts();

    if( myCmdCommsPort.bTxQSending )
    {
        WORD bytesToCopy;

        if( myCmdCommsPort.wTxQSize + buffLen <= COMMS_BUFF_LEN )
            bytesToCopy = buffLen;
        else
            bytesToCopy = COMMS_BUFF_LEN - myCmdCommsPort.wTxQSize;

        memcpy( &(pbyCmdCommsBuff[myCmdCommsPort.wTxQSize]), pBuff, bytesToCopy );

        myCmdCommsPort.wTxQSize += bytesToCopy;

        // Tack on "\r\n" if we have room
        if( myCmdCommsPort.wTxQSize < COMMS_BUFF_LEN )
        {
            pbyCmdCommsBuff[myCmdCommsPort.wTxQSize]   = '\r';
            pbyCmdCommsBuff[myCmdCommsPort.wTxQSize+1] = '\n';

            myCmdCommsPort.wTxQSize += 2;
        }
    }
    else
    {
        // Copy the buffer across. We tack on "\r\n" if there is room
        WORD bytesToCopy;

        if( buffLen < COMMS_BUFF_LEN )
            bytesToCopy = buffLen;
        else
            bytesToCopy = COMMS_BUFF_LEN;

        memcpy( pbyCmdCommsBuff, pBuff, bytesToCopy );

        if( bytesToCopy + 2 <= COMMS_BUFF_LEN )
        {
            pbyCmdCommsBuff[bytesToCopy]   = '\r';
            pbyCmdCommsBuff[bytesToCopy+1] = '\n';

            bytesToCopy += 2;
        }

        UartPortSendBuffer( &myCmdCommsPort, pbyCmdCommsBuff, bytesToCopy );
    }

    EnableInts();

    return TRUE;

}


//******************************************************************************
//
//******************************************************************************
BOOL CmdCommsPortSending( void )
{
    // With this build option, return the actual state of the transmit queue
    return UartTxQueueSending( &myCmdCommsPort );
}







static PKT_HEADER pktRxHdr;
static BYTE byHeaderIndex;

#define BYTE_SIZEOF_CRC sizeof(WORD)
#define MAX_PAYLOAD_SIZE 1026
static BYTE ptrPayloadBuff[MAX_PAYLOAD_SIZE];

#define HEADER_SYNC_SIZE 2


static WORD wRxingPaySize;
static WORD wPayloadIndex;


//******************************************************************************
//
//******************************************************************************
void updateCmdCommsRxStateMachine( BYTE byNewData )
{



    BYTE* ptrRxPktAsBytes;
    WORD wCalcdCRC;
    WORD wCRCBuffLen;
    WORD wPayldCRC;

    ptrRxPktAsBytes = (BYTE*)&pktRxHdr;

        switch( byCmdCommsRxState )
        {
            case CMD_CMS_RX_HDR_1_WAITING:
                if( byNewData == HEADER_SYNC_1)
                {
                    byHeaderIndex = 0;
                	ptrRxPktAsBytes[byHeaderIndex++] = HEADER_SYNC_1;
                	byCmdCommsRxState = CMD_CMS_RX_HDR_2_WAITING;
                }
                break;

            case CMD_CMS_RX_HDR_2_WAITING:
                if( byNewData == HEADER_SYNC_2)
                {
                	ptrRxPktAsBytes[byHeaderIndex++] = HEADER_SYNC_2;
                	byCmdCommsRxState = CMD_CMS_RX_HDR_RXING;
                }
                else
                {
                	byCmdCommsRxState = CMD_CMS_RX_HDR_1_WAITING;
                }
                break;

            case CMD_CMS_RX_HDR_RXING:
            	ptrRxPktAsBytes[byHeaderIndex++] = byNewData;
            	if( byHeaderIndex >= ( sizeof( PKT_HEADER) ) )
            	{
            	    // the header is fully received
            		// now, we know the header bytes are correct since that is
                    // validated in the state machine itself; so, we'll Validate
                    // the CRC


            		// calculate the CRC on the whole header - excepting the CRC, of course
            		wCRCBuffLen = sizeof(PKT_HEADER) - 2;
            		wCalcdCRC =  calcCCITTCRC( ptrRxPktAsBytes, wCRCBuffLen );
                    if( wCalcdCRC == pktRxHdr.wHeaderCRC )
                    {

                        switch(pktRxHdr.wCmdRspID)
                        {
                            case WRITE_GRP_CM:
                                //validate that the payload size is OK
                            	if( pktRxHdr.wPayloadSize > MAX_PAYLOAD_SIZE )
                            	{
                                	sendErrorResponse( &pktRxHdr, COMMS_ERR_PAYLOAD_SIZE );

                            		byCmdCommsRxState = CMD_CMS_RX_HDR_1_WAITING;

                            	}
                            	else
                            	{
                            		wPayloadIndex     = 0;
                                    wRxingPaySize     = pktRxHdr.wPayloadSize;
                                    byCmdCommsRxState = CMD_CMS_RX_PAYLD_RXING;
                            	}
                                break;
                            default:
                                // there is no payload in all other packets -
                                // so just proces directly
                                processCommandHeader(&pktRxHdr);

                                // go back to default state waiting for new packet
                                // the last one has finished
                        		byCmdCommsRxState = CMD_CMS_RX_HDR_1_WAITING;
                                break;
                        }

                    }
                    else
                    {
                        // if the packet is failed: send an error response to the host
                    	sendErrorResponse( &pktRxHdr, COMMS_ERR_HEADER_CRC_FAIL );

                	    byCmdCommsRxState = CMD_CMS_RX_HDR_1_WAITING;
                    }
                }
                break;


            case CMD_CMS_RX_PAYLD_RXING:
            	ptrPayloadBuff[wPayloadIndex++] = byNewData;
                if( wPayloadIndex >= wRxingPaySize )
                {
                    // calculate CRC on the whole payload - except for its CRC
                	wCalcdCRC = calcCCITTCRC( ptrPayloadBuff, (wRxingPaySize - 2) );
                	wPayldCRC = ptrPayloadBuff[wRxingPaySize-1];
                	wPayldCRC <<= 8;
                	wPayldCRC |= ptrPayloadBuff[wRxingPaySize-2];

                	if( wPayldCRC == wCalcdCRC)
                    {
                		processPktWithPayload( &pktRxHdr, ptrPayloadBuff, wRxingPaySize );
                    }
                	else
                	{
                    	sendErrorResponse( &pktRxHdr, COMMS_ERR_HEADER_CRC_FAIL );
                	}

            	    byCmdCommsRxState = CMD_CMS_RX_HDR_1_WAITING;
                }
                break;

            default:
                // error - we shouldn't be in this state
            	byCmdCommsRxState = CMD_CMS_RX_HDR_1_WAITING;
                break;


        }

}


static BYTE byNewRxData;
//******************************************************************************
//
//******************************************************************************
void updateCmdComms( void  )
{
    BOOL bGotChar;



    bGotChar =  GetUartPortChar(  &myCmdCommsPort, &byNewRxData );
    if( bGotChar )
    {
        updateCmdCommsRxStateMachine( byNewRxData );
    }
}




static PKT_HEADER pktTxHdr;
const BYTE* ptrBuff = (BYTE*)&pktTxHdr;




//******************************************************************************
//
//******************************************************************************
void sendErrorResponse( PKT_HEADER* ptrRxPkt, BYTE byTheError )
{


    WORD wCRCBuffLen;
    WORD wCalcdCRC;


    pktTxHdr.byHdr1     = HEADER_SYNC_1;
    pktTxHdr.byHdr2     = HEADER_SYNC_2;
    pktTxHdr.wCmdRspID  = CMD_ERROR_RESPONSE;

    pktTxHdr.wPktParam1   = ptrRxPkt->wHeaderCRC;
    pktTxHdr.wPktParam2   = (WORD) byTheError;



    pktTxHdr.wPayloadSize = 0;


	wCRCBuffLen = sizeof(PKT_HEADER) - 2;
	wCalcdCRC   = calcCCITTCRC( (BYTE*)(&pktTxHdr), wCRCBuffLen );

    pktTxHdr.wHeaderCRC = wCalcdCRC;

    // post the packet to the port for transmission
    WriteToCmdCommsPort( ptrBuff,  sizeof(PKT_HEADER) );


}

//******************************************************************************
//
//******************************************************************************
void sendCmdResponse( PKT_HEADER* ptrRxPkt, WORD wPktParam2 )
{


    WORD wCRCBuffLen;
    WORD wCalcdCRC;


    pktTxHdr.byHdr1 = HEADER_SYNC_1;
    pktTxHdr.byHdr2 = HEADER_SYNC_2;
    pktTxHdr.wCmdRspID    = (ptrRxPkt->wCmdRspID) | RSP_PKT_INDICATOR_BIT ; 
    pktTxHdr.wPktParam1   = ptrRxPkt->wPktParam1;

    if( ptrRxPkt->wCmdRspID == READ_REG_CMD )
    {
        pktTxHdr.wPktParam2 = wPktParam2;
    }
    else
    {
        // not necessary that we set this field to zero = but we do it just to
        // be consistent
        pktTxHdr.wPktParam2 = 0;
    }


    pktTxHdr.wPayloadSize = 0;


	wCRCBuffLen = sizeof(PKT_HEADER) - 2;
	wCalcdCRC   = calcCCITTCRC( (BYTE*)(&pktTxHdr), wCRCBuffLen );

    pktTxHdr.wHeaderCRC = wCalcdCRC;

    // post the packet to the port for transmission
    WriteToCmdCommsPort( ptrBuff,  sizeof(PKT_HEADER) );


}

#define MAX_PKT_RSP_SIZE (sizeof(PKT_HEADER) + MAX_ARB_WAVE_SAMPLES ) 
static BYTE ptrBigRespPkt[MAX_PKT_RSP_SIZE];



//******************************************************************************
//
//******************************************************************************
void sendParamaterGroupReadResponse( PKT_HEADER* ptrRxPkt,  BYTE* ptrParamBuff, WORD wParamBuffSize )
{

    BYTE* ptrHeader;
    WORD wCRCBuffLen;
    WORD wCalcdCRC;
    WORD wIndex;

    pktTxHdr.byHdr1      = HEADER_SYNC_1;
    pktTxHdr.byHdr2      = HEADER_SYNC_2;
    pktTxHdr.wCmdRspID   = COMMIT_PARAMS_TO_FLASH_RSP;
	
    pktTxHdr.wPktParam1  = ptrRxPkt->wHeaderCRC;
    pktTxHdr.wPktParam2  = 0x0000;

    pktTxHdr.wPayloadSize = wParamBuffSize;

	
	wCRCBuffLen = sizeof(PKT_HEADER) - 2;
	wCalcdCRC   = calcCCITTCRC( (BYTE*)(&pktTxHdr), wCRCBuffLen );
    pktTxHdr.wHeaderCRC = wCalcdCRC;

    // copy the command header into the packet
	ptrHeader = (BYTE*)(&pktTxHdr);
	for( wIndex = 0; wIndex < sizeof( PKT_HEADER); wIndex ++ )
	{
	    ptrBigRespPkt[wIndex] = ptrHeader[wIndex];
    }	
	
    // copy the paramaters (the payload) into the packet
	for( wIndex = sizeof( PKT_HEADER); wIndex < wParamBuffSize; wIndex ++ )
	{
	    ptrBigRespPkt[wIndex] = ptrParamBuff[wIndex-sizeof(PKT_HEADER)];
    }	

	// calculate the CRC for the parameters (i.e., the payload)
	wCalcdCRC   = calcCCITTCRC( ptrParamBuff, wParamBuffSize );
	
	// add the CRC to the packet
    ptrBigRespPkt[wIndex++] = (BYTE) (wCalcdCRC >> 8);
    ptrBigRespPkt[wIndex++] = (BYTE) (wCalcdCRC);	
	
	
	
    // post the packet to the port for transmission
    WriteToCmdCommsPort( ptrBigRespPkt,  wIndex );


}




//******************************************************************************
//
//******************************************************************************
BOOL processPktWithPayload( PKT_HEADER* ptrPktHdr, BYTE* ptrPayload, WORD wPayldSize )
{


    BOOL bSuccess;

	
    bSuccess = FALSE;
    switch( ptrPktHdr->wCmdRspID )
    {
        case WRITE_GRP_CM:

        	// validate the target register
        	if( ptrPktHdr->wPktParam1 != LD_PARAM_GROUP_RD_ADDR )
                return FALSE;

            // validate the size of the command
            if( ptrPktHdr->wPayloadSize != ( sizeof( LD_OP_PARAMS) + 2 ) )
                return FALSE;

        	doGroupParamRegWrite( (LD_OP_PARAMS*) ptrPayload );
            bSuccess = TRUE;
            break;


        default:
            break;
    }

    return bSuccess;

}


static BYTE* ptrGroupParams;
//******************************************************************************
//
//******************************************************************************
void processCommandHeader( PKT_HEADER* ptrPkt )
{


    int iErrorCode;
	WORD wReadData;
    BOOL bSuccess;

    switch( ptrPkt->wCmdRspID )
    {
        case READ_REG_CMD:
        	iErrorCode = wDoOpParamRegRead( ptrPkt->wPktParam1, &wReadData  );
            if( iErrorCode == 0 )
            {
                sendCmdResponse(ptrPkt, wReadData );
            }
            else
            {
            	sendErrorResponse( ptrPkt, iErrorCode );
            }
            break;

        case WRITE_REG_CMD:
        	iErrorCode = wDoOpParamRegWrite( ptrPkt->wPktParam1, ptrPkt->wPktParam2 );
        	if( iErrorCode == 0 )
        	{
                sendCmdResponse(ptrPkt, 0 );
        	}
        	else
        	{
            	sendErrorResponse( ptrPkt, iErrorCode );
        	}
        	break;

        case COMMIT_PARAMS_TO_FLASH:
        	// bSuccess = commitVolatileParamsToFlash(  );
            // sendCmdResponse(ptrPkt, bSuccess, wReadData );
        	break;

        case READ_GRP_CM:
        	ptrGroupParams = (BYTE*) getVolatileLdOpParamsPtrFromDataMGR();
        	if( ptrGroupParams == NULL)
        	{
            	sendErrorResponse( ptrPkt, COMMS_ERR_INVALID_ADDRESS );
        	}
        	else
        	{
                sendParamaterGroupReadResponse( ptrPkt,ptrGroupParams, sizeof(LD_OP_PARAMS) );
        	}
            break;


        default:
            // invalid command - not in the command list above
        	bSuccess = FALSE;
            break;
    }



}



