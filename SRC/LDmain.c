/*
===============================================================================
 Name        : main.c
 Author      :
 Version     :
 Copyright   : Copyright (C)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>
#include "LDMain.h"
#include "typedefs.h"
#include "target.h"
#include "gpio_cmsis_17xx.h"
#include "hardwareAbstraction.h"
#include "timer.h"
#include "SSP.h"
#include "Uart.h"
#include "UartQueue.h"
#include "UartManager.h"
#include "logging.h"
#include <stdio.h>
#include "CommUsbInterface.h"
#include "CommRF1Interface.h"
#include "CommRF2Interface.h"
#include "BaseStnDefs.h"

// Variable to store CRP value in. Will be placed automatically
// by the linker when "Enable Code Read Protect" selected.
// See crp.h header for more information
__CRP const unsigned int CRP_WORD = CRP_NO_CRP ;

const char welcomeMessage[] ="MAIN MCU PORT \r\n";
const char testRfMessage[] = {0x46,0x80, 0x00, 0x00, 0x00, 0x00, 0xE6 };

//*****************************************************************************
// Main System Loop Timer
//*****************************************************************************
static TIMERHANDLE thMainLoopTimer;
static TIMERHANDLE thServiceTimer;
static TIMERHANDLE thGenTimer;

#define MAIN_LOOP_INTERVAL 10
#define SERVICE_INTERVAL	1000
#define REPORT_SERVICE_TIMEOUT	13000

#define LED_INTERVAL 20


//*****************************************************************************
//System Clock Notes
//*****************************************************************************
// 5MHz MCLK in
// PLL -> SYSCLK (Frequency??)
// Peripheral Clock to sub-block -> SYSCLK/4



#define RAMP_TEST_SAMPLE_COUNT 4000



BYTE bData[128];
WORD wCount;

void UpdateStatusTask(void);
void HostCommTask(void);
void UpdateTestTask(void);
void TestFCCTest(void);

//*****************************************************************************
// Static Data for the System Init process
//*****************************************************************************

//******************************************************************************
//
//  Function: doSystemInit
//
//  Arguments:	none
//
//  Returns: void.
//
//  Description: This function initialise the clock and the timer
//
//******************************************************************************
void doSystemInit( void )
{
    int iSysClk;

    SystemCoreClockUpdate();
    iSysClk = SystemCoreClock;


    InitTimers( NULL /*pass a function pointer to call on timer update*/ );
    thMainLoopTimer = RegisterTimer();
    thServiceTimer = RegisterTimer();
    thGenTimer = RegisterTimer();
    InitContactClosuresTimers();

}


//*****************************************************************************
//
//*****************************************************************************
int main(void)
{

	int i;
	BYTE bTmp;
	WORD wCount;
	bTmp = '0';
	wCount = 20;

	GetResetReason();

	setGPIODir();
	resetExtWD();

	for(i=0;i<sizeof(bData);i++)
	{
		bData[i]= bTmp++;
		if(bTmp>126)
			bTmp ='0';
	}
	//*************************************************************************
    //
    //*************************************************************************
    doSystemInit();
//	InitWDT();
//    KickWatchDog();

//    while(1);
    InitCommUsb();
    InitCommRF1();
    InitCommRF2();
    ResetTimer(thMainLoopTimer, MAIN_LOOP_INTERVAL );
    StartTimer(thMainLoopTimer);
    ResetTimer(thServiceTimer, SERVICE_INTERVAL );
    StartTimer(thServiceTimer);
    StartTimer(thGenTimer);

//    TestFCCTest();
//    PIN_SET(USB_PROG_DISABLE);
    while(1)
    {

    	resetExtWD();
//    	KickWatchDog();

    	if(PIN_STATUS(RF_LINK_1))
    		PIN_SET( RF1LED);
    	else
    		PIN_CLR( RF1LED);

    	if(PIN_STATUS(RF_LINK_2))
    		PIN_SET( RF2LED);
       	else
       		PIN_CLR( RF2LED);

    	UpdateUsbComRxBuf();
    	UpdateRF1ComRxBuf();
    	UpdateRF2ComRxBuf();


    	HostCommTask();
    	if(TimerExpired( thGenTimer))
    	{
    		UpdateStatusTask();
    	}
//    	UpdateTestTask();

    	ContactTimeoutTask();
    	if(ComPortClosed()==TRUE)
    	{
    		SetContactClosure((BYTE)0);	// Deactivate all contacts
    	}


    }
    return 0 ;
}




//******************************************************************************
//
//  Function: HostCommTask
//
//  Arguments:	none
//
//  Returns: void.
//
//  Description: This function reads and distributes the data coming from the USB port
//
//******************************************************************************
void HostCommTask(void)
{
	if(IsUsbRxPacketReady())
	{
		wCount = ReadUsbRxPacket(bData,sizeof(bData));
		if(HaveCommPacket(bData, wCount))
		{// command packet received, execute
			ExecuteCommand();
		}
		else
		{	// OAP packet received send to RF1 - outside radio with the N connector
			WriteToRF1Port(bData,wCount);

			// disable Update Status Task for a few seconds
			ResetTimer(thGenTimer, REPORT_SERVICE_TIMEOUT);
		}
	}
}

//******************************************************************************
//
//  Function: UpdateStatusTask
//
//  Arguments:	none
//
//  Returns: void.
//
//  Description: This function reads and sends the status of the radios to the USB port every 10 ms
//
//******************************************************************************
void UpdateStatusTask(void)
{
	BYTE radioNbr;

	if(TimerExpired( thMainLoopTimer))
	{

		radioNbr = 0;
		SendGetRadioStatCommand(radioNbr);
		ResetTimer(thMainLoopTimer, RADIO_RESP_TIME);
		while(TimerExpired( thMainLoopTimer)==FALSE)
		{
			UpdateRF1ComRxBuf();
		}
		if(IsRF1RxPacketReady())
		{
			wCount = ReadRF1RxPacket(bData,sizeof(bData));
			ParseStatusData(radioNbr, bData,wCount);
		}

		radioNbr = 1;
		SendGetRadioStatCommand(radioNbr);
		ResetTimer(thMainLoopTimer, RADIO_RESP_TIME);
		while(TimerExpired( thMainLoopTimer)==FALSE)
		{
			UpdateRF2ComRxBuf();
		}
		if(IsRF2RxPacketReady())
		{
			wCount = ReadRF2RxPacket(bData,sizeof(bData));
			ParseStatusData(radioNbr, bData,wCount);
		}
		SendBaseStnStatus();
		ResetTimer(thMainLoopTimer, MAIN_LOOP_INTERVAL );
	}
}



//******************************************************************************
//
//  Function: ResetStatusTimer
//
//  Arguments:	none
//
//  Returns: void.
//
//  Description: reset the update status timer to 0. It will cause the Status Event to be sent.
//
//
//******************************************************************************
void ResetStatusTimer(void)
{
	ResetTimer(thMainLoopTimer, (WORD)0 );
}


//******************************************************************************
//
//  Function: UpdateTestTask
//
//  Arguments:	none
//
//  Returns: void.
//
//  Description: test function.
//
//******************************************************************************
void UpdateTestTask(void)
{
	BYTE radioNbr;


	if(TimerExpired( thMainLoopTimer))
	{
		if(GetTestStatus())
		{
			radioNbr = 1;
			WriteToRF1Port( (const BYTE*)testRfMessage,sizeof(testRfMessage));
			ResetTimer(thMainLoopTimer, RADIO_RESP_TIME);
			while(TimerExpired( thMainLoopTimer)==FALSE);
			WriteToRF2Port( (const BYTE*) testRfMessage,sizeof(testRfMessage));

		}
		else
		{
			if(TimerExpired( thServiceTimer))
			{
				ResetTimer(thServiceTimer, SERVICE_INTERVAL );
				WriteToUsbPort((const BYTE*)welcomeMessage,sizeof(welcomeMessage));
			}
		}
		ResetTimer(thMainLoopTimer, MAIN_LOOP_INTERVAL );

	}
}

/****************************************************
 * TEST FUNCTIONS
 ****************************************************/
void TestFCCTest(void)
{
	BYTE radioNbr;
	BYTE radioChannel;
	BYTE radioPower;
	radioNbr = 1;
	radioChannel = 25;
	radioPower = 0xD5;
	SetRadioChannel(radioNbr,radioChannel);
	ResetTimer(thMainLoopTimer, RADIO_RESP_TIME);
	while(TimerExpired( thMainLoopTimer)==FALSE);
	radioNbr = 2;
	SetRadioChannel(radioNbr,radioChannel);
	ResetTimer(thMainLoopTimer, RADIO_RESP_TIME);
	while(TimerExpired( thMainLoopTimer)==FALSE);

	radioNbr = 1;
	radioChannel = 25;
	SetRadioPower(radioNbr,radioPower);
	ResetTimer(thMainLoopTimer, RADIO_RESP_TIME);
	while(TimerExpired( thMainLoopTimer)==FALSE);
	radioNbr = 2;
	SetRadioPower(radioNbr,radioPower);
	ResetTimer(thMainLoopTimer, RADIO_RESP_TIME);
	while(TimerExpired( thMainLoopTimer)==FALSE);

	ResetTimer(thMainLoopTimer, MAIN_LOOP_INTERVAL);

	while(1)
	{
		resetExtWD();

		if(TimerExpired( thMainLoopTimer))
		{

			WriteToRF1Port((BYTE*)testRfMessage,(WORD)sizeof(testRfMessage));
			ResetTimer(thMainLoopTimer, RADIO_RESP_TIME);
			while(TimerExpired( thMainLoopTimer)==FALSE);
			WriteToRF2Port((BYTE*)testRfMessage,(WORD)sizeof(testRfMessage));

			ResetTimer(thMainLoopTimer, MAIN_LOOP_INTERVAL);
		}
	}

}







