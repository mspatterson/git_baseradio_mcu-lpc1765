//******************************************************************************
//
//  UartManager.c:
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  Provides setup and control of the LPC UART serial ports.  Currently the
//  following features are not supported - DMA, Autobaud, IrDA and Baud Rate
//  divisor calculations using the fractional divider register.
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-02-01      JK     Initial Implementation
//
//******************************************************************************


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include <string.h>

#include "hardwareAbstraction.h"
#include "uart.h"
#include "uartQueue.h"
#include "timer.h"
#include "clocksAndDivisors.h"
#include "UartManager.h"


//------------------------------------------------------------------------------
// Extern Variable From 'Uart.c'
//------------------------------------------------------------------------------
extern LPC_UART1_TypeDef *pLpcUartRegs[];



//------------------------------------------------------------------------------
//  GLOBAL DECLARATIONS
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS
//------------------------------------------------------------------------------

//******************************************************************************
//
//  Function: uartTxQueueSending
//
//  Arguments: Uart Manager Object
//
//  Returns: Returns 'True' if the Tx Queue is being transmitted; returns false
//           otherwise
//
//  Description:
//
//******************************************************************************
BOOL UartTxQueueSending( UART_MGR_OBJECT* ptrUartManObj )
{
    return ptrUartManObj->bTxQSending;
}


//******************************************************************************
//
//  Function: InitUartManager
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: This function must be the first to be called from 
//               this module to initialize static variables for all the modem
//               ports under. Here is what the caller must do:
//
//               1) declare a 'UART_MGR_OBJECT' object in memory - and pass the
//                  pointer in as a paramter to this function call
//               2) setup two 'wrapper' calls to the UartRxISR and UartTxISR
//                  handlers - and pass these in as parameters.
//
//               The reason for '2' above is as follows: we must register a
//               function call that takes no parameters with the interrupt
//               controller; but, the low level handlers in this routine
//               (UartRxISR and UartTxISR) are the interrupt handler for all the
//               uart ports - and they need to know what buffer-queue that they
//               can post data to. Therefore, the most straightforward option
//               is to force the caller to wrap these low level handlers (which
//               take the uart object as a pointer) in a 'void' function which
//               passes the uart object pointer to the ISR handler - then register
//               this 'wrapper' call with the interrupt controller.
//
//******************************************************************************
BOOL InitUartManager( UART_MGR_OBJECT* ptrUartManObj )
{
    if( ptrUartManObj == NULL)
        return FALSE;

    // validate the port number selected
    if( ptrUartManObj-> byPortNum  >= NUM_OF_LPC_UARTS )
        return FALSE;

    // initialize the Rx queue
    ptrUartManObj->pUartRxQueue  = &(ptrUartManObj->UartRxQueue);
    initUartQ( ptrUartManObj->pUartRxQueue );

    // initialize the Tx queue tracking vars
    ptrUartManObj->ptrTxQ      = NULL;
    ptrUartManObj->wTxQIndex   = 0;
    ptrUartManObj->wTxQSize    = 0;
    ptrUartManObj->bTxQSending = FALSE;

    return TRUE;
}


//******************************************************************************
//
//  Function: OpenUartSerialPort
//
//  Arguments:
//    cfgInfo   - A SERIAL_CFG data structure containing specific
//                information requiring the configuration
//                of the Uart port.
//
//  Returns: TRUE on success.
//           FALSE otherwise.
//
//  Description: This function configures the wireless Uart port based on
//               the passed SERIAL_CFG parameter.
//
//******************************************************************************
BOOL OpenUartSerialPort( UART_MGR_OBJECT* ptrUartManObj, SERIAL_CFG* cfgInfo )
{
    // Opens the serial port identified by aPort, and sets its
    // parameters to those in the passed config structure. Returns
    // one of the SERIAL_RTN_TYPES enums.
    BYTE bySymbolLen;
    BYTE byParity;
    BOOL bInitResult;

    LPC_UART_INIT_DATA UartInitStrct;
    memset( &UartInitStrct, 0, sizeof( LPC_UART_INIT_DATA ) );

    // Desired/actual Baud Rate - if this is set to a non-zero value it will be
    // used to calculate the divisor latch value based on the system clk frequency.
    // If this is set to zero when passed to initialization function, the divider
    // latch value will be written directly from the init data structure.  After
    // initialization is completed, this will contain the actual integer Baud Rate.

#ifdef TO_COMPLETE
after fixing the uart timing issue, re-instate this code:
    switch( cfgInfo->dwSpeed )
    {
        case 115200:
        case  57600:
        case  38400:
        case  19200:
        case   9600:
        case   4800:
        case   2400:
        case   1200:
        case    300:
            UartInitStrct.BaudRate = cfgInfo->dwSpeed;
            break;

        default:
            return FALSE;
            break;
    }
#else
    UartInitStrct.BaudRate = cfgInfo->dwSpeed;
#endif
    // Divisor Latch Value - if BaudRate is 0, this value will be written directly
    // to the Divisor Latch registers.  If the divisor value is calculated based on
    // BaudRate, this will contain the calculated divisor latch value on return from
    // initialization function.
    //      Baud Rate = pclk/(divisor latch value * 16),  default pclk = sysclk/4
    //          assumes fractional divider is not used
    UartInitStrct.DivisorLatchValue = 0;

    // Fractional Divider Register value - can be used to reduce baud rate error.
    // The fractional divder is currently not supported for calculated divider
    // latch values, so if the fractional dividor is to be used the divider latch
    // value must also be written manually.  See LPC user manual for info.
    UartInitStrct.FractionalDividerRegValue = 0;

    // Interrupt Enable Register value
        // b0 - Rx data available/timeout interrupt enable
        // b1 - Tx holding register empty interrupt enable
        // b2 - Line status interrupt enable
        // b3 - Modem status interrupt enable (UART1 only)
        // b7 - CTS interrupt enable (UART1 only)
        // b8 - end of autobaud interrupt enable
        // b9 - autobaud timeout interrupt enable
    UartInitStrct.IrqEnableRegValue = ENABLE_RX_INT | ENABLE_TX_INT;

    // UART interrupt priority, 0 = highest, 31 = lowest
    UartInitStrct.IntPriority = 0;

    // Line status interrupt callback function, set to NULL if not required
    // Interrupt will already be cleared, Line status register value is passed
    // to the callback function.
    UartInitStrct.LineStatusCallBack = NULL;

    // Rx Data interrupt callback function, set to NULL if not required
    // Rx data must be read in callback function to clear the interrupt.  FifoTimeout
    // will be TRUE if interrupt was generated due to a Rx timeout (data is in Rx FIFO
    // but interrupt threshold has not been reached).
    UartInitStrct.RxDataCallBack = ptrUartManObj->UartRxDataCallBack;

    // Tx Data interrupt callback function, set to NULL if not required
    // Interrupt generated when Tx holding register is empty (no data left in FIFO)
    // Interrupt should already be cleared.  Callback can write new data if required
    // or do nothing if transmission is complete.
    UartInitStrct.TxDataCallBack = ptrUartManObj->UartTxDataCallBack;

    // Modem Status interrupt callback function (UART1 only), set to NULL if not required
    // See LPC Users Manual for more info
    UartInitStrct.ModemStatusCallBack = NULL;

    // FIFO Control Register value
        // b0 - FIFO enable, set to enable FIFOs for Rx and Tx
        // b1 - Rx FIFO reset, FIFO will always be reset on initialization
        // b2 - Tx FIFO reset, FIFO will always be reset on initialization
        // b3 - DMA mode select
        // b7:6 - Rx FIFO trigger level, 00= 1 byte, 01= 4 bytes, 10= 8 bytes, 11= 14 bytes
    UartInitStrct.FifoCtrlRegValue = 0;

    // Line Control Register value
        // b1:0 - word length select, 00= 5 bits, 01= 6 bits, 10= 7 bits, 11= 8 bits
        // b2 - stop bit select, 0 = 1 stop bit, 1= 2 stop bits
        // b3 - parity enable
        // b5:4 - parity select, 00= odd, 01= even, 10= force 1, 11= force 0
        // b6 - break control
        // b7 - divisor latch access control (will be ignore, handled internally)

    if( cfgInfo->wDataBits == 8 )
    {
        bySymbolLen = UART_SYM_LEN_8_BITS;
    }
    else
    {
        // Invalid port word size
        return( FALSE );
    }

    switch( cfgInfo->wParity )
    {
        case PARITY_NONE:
            byParity = UART_PARITY_NONE;
            break;

        case PARITY_EVEN:
            byParity = UART_PARITY_EVEN;
            break;

        case PARITY_ODD:
            byParity = UART_PARITY_ODD;
            break;

        default:
        {
            // Invalid port parity
            return( FALSE );
            break;
        }
    }

    UartInitStrct.LineCtrlRegValue = bySymbolLen | byParity;

    // Modem Control Register value (UART1 only, see user manual)
    UartInitStrct.ModemCtrlRegValue = 0;

    // RS485 Control Register value (UART1 only)
    UartInitStrct.RS485CtrlRegValue = 0;

    // RS485 Address Match Register value (UART1 only)
    UartInitStrct.RS485AddrMatchRegValue = 0;

    // RS485 Delay Register value (UART1 only)
    UartInitStrct.RS485DelayRegValue = 0;

#ifdef CODE_TO_COMPLETE
    port flow control stuff below:
    // Verify flow control
    if( cfgInfo->wFlowCntl == NO_FLOW_CNTL )
    {
        // do nothing.
    }
    else if( cfgInfo->wFlowCntl == XON_XOFF_FLOW_CNTL )
    {
        // This is not supported
        return FALSE;
    }
    else if( cfgInfo->wFlowCntl == RTS_CTS_FLOW_CNTL )
    {
        // Enable signals
    }
    else
    {
        // Invalid flow control
        return FALSE;
    }
#endif

    // open the port
    bInitResult = UartInit( ptrUartManObj->byPortNum, &UartInitStrct );

    initUartQ( ptrUartManObj->pUartRxQueue );

    // Be sure interrupts are disable at init time
    UartTransmitDisable( ptrUartManObj->byPortNum );

    return( bInitResult);
}


//******************************************************************************
//
//  Function: GetUartSerialPortSettings
//
//  Arguments:
//    OUT cfgInfo - Pointer to a SERIAL_CFG data structure containing specific
//                  information requiring the configuration
//                  of the Uart port.
//
//  Returns: void.
//
//  Description: This function gets the configuration the Uart port and
//               populates the passed cfgInfo structure.
//
//******************************************************************************
void GetUartSerialPortSettings( SERIAL_CFG* cfgInfo )
{
#ifdef CODE_TO_COMPLETE

    WORD wSettings = SCCR1 & 0xFFD3;   // mask out interrupts and comm enable bits
    WORD wRegisterSpeed = SCCR0;
    DWORD dwBitRate = SYS_CLOCK / ( (DWORD)wRegisterSpeed * 32L );

    if( ( dwBitRate >= BIT_RATE_1200 ) && ( dwBitRate < BIT_RATE_2400 ) )
    {
        // Set port speed 
        cfgInfo->dwSpeed = BIT_RATE_1200;
    }

    else if( ( dwBitRate >= BIT_RATE_2400 ) && ( dwBitRate < BIT_RATE_4800 ) )
    {
        // Set port speed 
        cfgInfo->dwSpeed = BIT_RATE_2400;
    }

    else if( ( dwBitRate >= BIT_RATE_4800 ) && ( dwBitRate < BIT_RATE_9600 ) )
    {
        // Set port speed 
        cfgInfo->dwSpeed = BIT_RATE_4800;
    }

    else if( ( dwBitRate >= BIT_RATE_9600 ) && ( dwBitRate < BIT_RATE_19200 ) )
    {
        // Set port speed 
        cfgInfo->dwSpeed = BIT_RATE_9600;
    }

    else if( ( dwBitRate >= BIT_RATE_19200 ) && ( dwBitRate < BIT_RATE_38400 ) )
    {
        // Set port speed 
        cfgInfo->dwSpeed = BIT_RATE_19200;
    }

    else if( ( dwBitRate >= BIT_RATE_38400 ) && ( dwBitRate < BIT_RATE_57600 ) )
    {
        // Set port speed 
        cfgInfo->dwSpeed = BIT_RATE_38400;
    }

    else if( ( dwBitRate >= BIT_RATE_57600 ) && ( dwBitRate < BIT_RATE_115200 ) )
    {
        // Set port speed 
        cfgInfo->dwSpeed = BIT_RATE_57600;
    }

    else if( dwBitRate >= BIT_RATE_115200 )
    {
        // Set port speed 
        cfgInfo->dwSpeed = BIT_RATE_115200;
    }

    // Set port word size
    if( ( wSettings & 0x0200 ) == 0x0200 )
    {
        // Indicate one more stop bit in frame
        cfgInfo->wDataBits = 8;
        cfgInfo->wStopBits = 2;
    }
    else
    {
        // we have a regular 10-bit frame
        cfgInfo->wDataBits = 8;
        cfgInfo->wStopBits = 1;
    }
    
    // Set port parity
    if( ( wSettings & 0x0400 ) == 0x0400 )
    {
        // Parity is enabled, see what type
        if( ( wSettings & 0x0C00 ) == 0x0C00 )
        {
            cfgInfo->wParity = PARITY_ODD;
        }
        else
        {
            cfgInfo->wParity = PARITY_EVEN;
        }
    }
    else
    {
        cfgInfo->wParity = PARITY_NONE;
    }

#endif
}


//******************************************************************************
//
//  Function: FlushUartSerialTxQueue
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: Call this function to reset the transmit queue sending process;
//               it can be called to terminate a queue send that is in progress,
//               or to reset a queue process that has terminated
//
//******************************************************************************
void FlushUartSerialTxQueue( UART_MGR_OBJECT* ptrUartManObj )
{
    // Disable interrupts while accessing the queue
    DisableInts();

    // reset the Tx queue tracking vars
    ptrUartManObj->ptrTxQ      = NULL;
    ptrUartManObj->wTxQIndex   = 0;
    ptrUartManObj->wTxQSize    = 0;
    ptrUartManObj->bTxQSending = FALSE;

    UartTransmitDisable(ptrUartManObj->byPortNum);

    // Enable them again.
    EnableInts();
}


//******************************************************************************
//
//  Function: FlushUartSerialRxQueue
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: Call this function to flush the receive data queue.
//
//******************************************************************************
void FlushUartSerialRxQueue( UART_MGR_OBJECT* ptrUartManObj )
{
    // Disable interrupts while accessing the queue
    DisableInts();

    // Clears data from the port queues.
    initUartQ( ptrUartManObj->pUartRxQueue );

    // Enable them again.
    EnableInts();
}


//******************************************************************************
//
//  Function: GetUartPortChar
//
//  Arguments:
//    byData    - data received from the Uart's serial interface.
//
//  Returns: TRUE if data was pending - byte returned in byData
//           FALSE otherwise.
//
//  Description: Call this function to get a byte from the hardware's 
//               receiving FIFO and add it to a queue.  
//
//******************************************************************************
BOOL GetUartPortChar( UART_MGR_OBJECT* ptrUartManObj, BYTE* byData )
{
    BYTE byReadData;


    if( !UartQHasData(ptrUartManObj->pUartRxQueue) )
    {
        // Fall through means we have no data.
        return FALSE;
    }

    DisableInts();

    byReadData = ReadUartQ( ptrUartManObj->pUartRxQueue );

    EnableInts();

    *byData = byReadData;

    return TRUE;
}


//******************************************************************************
//
//  Function: GetUartPortLSR
//
//  Arguments:
//    byPort - port number to get LSR for
//
//  Returns: DWORD value representing current LSR value
//
//  Description: This function gets a port's LSR
//
//******************************************************************************
DWORD GetUartPortLSR( BYTE portNbr )
{
    if( portNbr < NUM_OF_LPC_UARTS )
        return pLpcUartRegs[portNbr]->LSR;
    else
        return 0;
}


static volatile DWORD dwRegStatus;
//******************************************************************************
//
//  Function: UartRxIsr
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: This is the Uart Recieve ISR function. It gets called when an
//               RX interrupt is pending.
//
//******************************************************************************
void UartRxISR( UART_MGR_OBJECT* ptrUartManObj )
{
    char RxData;

    // read all available data from the Uart; the LSR register inidates data as:
    // LSR bit 0 is set when the UnRBR holds an unread character and is cleared
    //  when the UARTn RBR FIFO is empty.
    dwRegStatus = pLpcUartRegs[ptrUartManObj->byPortNum]->LSR;

    while( dwRegStatus & UART_RX_DATA_RDY )
    {
        RxData = UartReadByte( ptrUartManObj->byPortNum );
        Add2UartQ( RxData, ptrUartManObj->pUartRxQueue );
        dwRegStatus = pLpcUartRegs[ptrUartManObj->byPortNum]->LSR;
    }
}


//******************************************************************************
//
//  Function: UartTxISR
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: This is the Uart Transmit ISR function. It gets called when an
//               TX interrupt is pending.
//
//******************************************************************************
void UartTxISR( UART_MGR_OBJECT* ptrUartManObj )
{
    if( ptrUartManObj->wTxQIndex < ptrUartManObj->wTxQSize )
    {
        BYTE byToSend = ptrUartManObj->ptrTxQ[(ptrUartManObj->wTxQIndex)++];
        UartWriteByte( ptrUartManObj->byPortNum, byToSend );
    }
    else
    {
        // Disable TX interrupt
        UartTransmitDisable( ptrUartManObj->byPortNum );
        ptrUartManObj->bTxQSending = FALSE;
    }
}


//******************************************************************************
//
//  Function: UartPortSendByte
//
//  Arguments:
//    portNbr   - UART port number
//    byData    - Byte to send
//
//  Returns: TRUE on success, false if the transmitter was busy or the port
//           was invalid
//
//  Description: Call this function to write a single byte directly
//               to the UART's transmit register
//
//******************************************************************************
BOOL UartPortSendByte( BYTE portNbr, BYTE byData )
{
    UartWriteByte( portNbr, byData );
    UartTransmitEnable( portNbr );

    return TRUE;
}


//******************************************************************************
//
//  Function: UartPortSendBuffer
//
//  Arguments:
//    pBuffer   - An array of BYTE data to be shifted
//                out to the transmitting port.
//    wLength   - Specifies the length of the pBuffer array.
//
//  Returns: void.
//
//  Description: Call this function to write the passed byte onto the
//               hardware's transmitting FIFO. The function only writes to
//               the FIFO when hardware's transmitting port is ready for
//               shifting data out.
//
//******************************************************************************
BOOL UartPortSendBuffer( UART_MGR_OBJECT* ptrUartManObj, const BYTE* pBuffer, WORD wLength )
{
    // ensure that a tx queue isn't already in progress
    if( ptrUartManObj->bTxQSending == TRUE )
        return FALSE;

    DisableInts();

    // set the queue tracking vars to track this new Tx Queue being posted here
    ptrUartManObj->ptrTxQ      = pBuffer;
    ptrUartManObj->wTxQIndex   = 0;
    ptrUartManObj->wTxQSize    = wLength;
    ptrUartManObj->bTxQSending = TRUE;

    // enable the interrupt
    // Need the Uart Port Number Here
    if( !UartIsTransmitEnabled(ptrUartManObj->byPortNum) )
    {
        // we need to 'prime' the Uart transmit ISR: because it triggers
        // when the transmit buffer is empty, we need to first send a
        // a character in order to get the first trigger to occur. So we
        // call the Uart ISR explicitly - which sends the first char,
        // thereby triggering successive calls to UartTxISR which are
        // made via an interrupt call
        ptrUartManObj->UartTxDataCallBack( );
        UartTransmitEnable(ptrUartManObj->byPortNum);
    }

    EnableInts();

    return TRUE;
}
