//******************************************************************************
//
//  CRC-CCITT.C
//
//      Copyright (c) 2006, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module implements a calculator for the CCITT 16-bit CRC
//
//  Date            Author      Comment
//  ---------------------------------------------------------------------------
//  Aug. 30, 2007    JK          Version 1v0
//******************************************************************************


#include "typedefs.h"


#define CRC_START       0xFFFF
#define CCITT_POLY      0x1021
#define MSB_OF_CRC_SET  0x8000
#define MSB_OF_DATA_SET 0x80

static WORD wCRC;

#define BIT_SIZEOF_BYTE 8

//******************************************************************************/
//
//  Function:    calcCCITTCRC
//
//  Arguments:    BYTE* - pointer to buffer for CRC calc;
//                WORD  - Length of the buffer
//
//  Returns:      Calculated CRC
//
//  Description:  Calculates the CCITTCRC
//
//******************************************************************************/
WORD calcCCITTCRC( BYTE* ptrBuffer, WORD wBuffLen )
{

    WORD wBuffIndex;
    BYTE byByteIndex;
    BOOL bXorFlag;
    BYTE byCurrentData;

    wCRC = CRC_START;

    //*************************************************************************
    // Run through the buffer byte-by-byte
    //*************************************************************************
    for(  wBuffIndex = 0; wBuffIndex < wBuffLen; wBuffIndex++ )
    {
        byCurrentData = ptrBuffer[wBuffIndex];

        //*********************************************************************
        // Process each bit in the data byte
        //*********************************************************************
        for( byByteIndex=0; byByteIndex < BIT_SIZEOF_BYTE; byByteIndex++ )
        {
            if( wCRC & MSB_OF_CRC_SET )
            {
                bXorFlag=TRUE;
            }
            else
            {
                bXorFlag=FALSE;
            }

            wCRC <<= 1;

            if( byCurrentData & MSB_OF_DATA_SET )
            {
                wCRC = wCRC + 1;
            }

            if( bXorFlag )
            {
                wCRC = wCRC ^ CCITT_POLY;
            }

            // Now, Shift Data byte
            byCurrentData <<= 1;
        }  

    }


    //*************************************************************************
    // Now: we have to finish the buffer by running the final 16-bits
    //*************************************************************************
    for( byByteIndex=0; byByteIndex < 2*BIT_SIZEOF_BYTE; byByteIndex++ )
    {
        if( wCRC & MSB_OF_CRC_SET )
        {
            bXorFlag=TRUE;
        }
        else
        {
            bXorFlag=FALSE;
        }

        wCRC <<= 1;

        if( bXorFlag )
        {
            wCRC = wCRC ^ CCITT_POLY;
        }

        // Now, Shift Data byte
        byCurrentData <<= 1;
       
    }

    return wCRC;

}
