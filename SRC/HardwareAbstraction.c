//******************************************************************************
//
//  hardwareAbstraction.c: 
//
//      Copyright (c ) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  Contains abstractions for the hardware layer
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  Version 1.0     JK      July 6, 2012
//
//******************************************************************************


#include "HardwareAbstraction.h"
#include "timer.h"

#define CONTACT_TIMEOUT_MS	10000

// using internal RC osc as source gives 1us resolution on watchdog
#define WDTC_REG_VALUE      1000000     // use nominal 1 sec timeout

static BYTE ContactOutputs;
static BYTE lastReset;

static TIMERHANDLE thContact1;
static TIMERHANDLE thContact2;
static TIMERHANDLE thContact3;
static TIMERHANDLE thContact4;
//******************************************************************************
//
//******************************************************************************
void setGPIODir( void )
{
    //******************************************************************************
    //
    //******************************************************************************


	PIN_CLR(USB_PROG_DISABLE);
	PIN_DIR_OUT( USB_PROG_DISABLE );
	PIN_SET(USB_PROG_EN);
	PIN_DIR_OUT( USB_PROG_EN );

	PIN_SET(PROG_WDOG);
	PIN_DIR_OUT( PROG_WDOG );
	PIN_SET( ZC_UART_CTS );



	//Contact_Closure Outputs are inactive on power up or reset
	PIN_CLR(Contact_Closure1);
	PIN_CLR(Contact_Closure2);
	PIN_CLR(Contact_Closure3);
	PIN_CLR(Contact_Closure4);
	PIN_CLR(LED1);
	PIN_CLR(LED2);
	PIN_CLR(LED3);
	PIN_CLR(LED4);
	PIN_CLR(RF1LED);
	PIN_CLR(RF2LED);



	PIN_DIR_OUT( ZC_UART_CTS );
	PIN_DIR_OUT( Contact_Closure1 );
	PIN_DIR_OUT( Contact_Closure2 );
	PIN_DIR_OUT( Contact_Closure3 );
	PIN_DIR_OUT( Contact_Closure4 );
	PIN_DIR_OUT( LED1 );
	PIN_DIR_OUT( LED2 );
	PIN_DIR_OUT( LED3 );
	PIN_DIR_OUT( LED4 );
	PIN_DIR_OUT( RF1LED );
	PIN_DIR_OUT( RF2LED );




	PIN_DIR_IN(VERSION_IN0);
	PIN_DIR_IN(VERSION_IN1);
	PIN_DIR_IN(VERSION_IN2);
	PIN_DIR_IN(VERSION_IN3);
	PIN_DIR_IN(PROG_DD_1);
	PIN_DIR_IN(PROG_DC_1);
	PIN_DIR_IN(PROG_DD_2);
	PIN_DIR_IN(PROG_DC_2);

	PIN_DIR_IN(RF_LINK_1);
    PIN_DIR_IN(RF_LINK_2);
    PIN_DIR_IN(SUSPEND_PC);
    PIN_DIR_IN(USB_CONN);
    PIN_DIR_IN(ZC_UART_RI);		// input for now
    PIN_DIR_IN(RESET_N1);		// input for now
    PIN_DIR_IN(RESET_N2);		// input for now
    PIN_DIR_IN(ZC_UART_RTS);



};


void resetExtWD(void)
{
	static BOOL toggle = FALSE;

	toggle^=1;

	if(toggle)
		PIN_SET(PROG_WDOG);
	else
		PIN_CLR(PROG_WDOG);
}

/********************************************************************
 * CONTACT CLOSURE OPERATION - INTERLOCK
> 1. On power up, the default state for all outputs is 'off' (eg,
> contacts open).
> 2. On receipt of a 'close output' command for a switch, the Base Radio
> will energize that switch immediately and start a 10 second timer.
>     - If the timer expires, the Base Radio shall automatically open
> 		that output
>     - If another 'close output' command is received for that contact
> 		before the 10 second timer expires,
>       the contact shall remain closed and the timer shall be reset to 10 seconds
> 3. On receipt of an 'open output' command, the output shall
> immediately be opened. The output shall remain off indefinitely (eg, there is no timer operation in
> this state)
> 4. If the Base Radio detects through the USB interface that the host
> 	no longer has its control serial port open, the Base Radio shall
>	immediately open all contacts.
 *
 ***********************************************************************/

//******************************************************************************
//	activate/ deactivate contacts
// if the corresponding bit field is 1 - activate(close) the contact and reset th timer
// if 0 - deactivate (open) the contact
//******************************************************************************
void SetContactClosure(BYTE bOutputs)
{
	if(bOutputs & (1<<0))
	{
		PIN_SET(Contact_Closure1);
		PIN_SET(LED1);
		ResetTimer(thContact1, CONTACT_TIMEOUT_MS );
	}
	else
	{
		PIN_CLR(Contact_Closure1);
		PIN_CLR(LED1);
	}

	if(bOutputs & (1<<1))
	{
		PIN_SET(Contact_Closure2);
		PIN_SET(LED2);
		ResetTimer(thContact2, CONTACT_TIMEOUT_MS );
	}
	else
	{
		PIN_CLR(Contact_Closure2);
		PIN_CLR(LED2);
	}

	if(bOutputs & (1<<2))
	{
		PIN_SET(Contact_Closure3);
		PIN_SET(LED3);
		ResetTimer(thContact3, CONTACT_TIMEOUT_MS );
	}
	else
	{
		PIN_CLR(Contact_Closure3);
		PIN_CLR(LED3);
	}

	if(bOutputs & (1<<3))
	{
		PIN_SET(Contact_Closure4);
		PIN_SET(LED4);
		ResetTimer(thContact4, CONTACT_TIMEOUT_MS );
	}
	else
	{
		PIN_CLR(Contact_Closure4);
		PIN_CLR(LED4);
	}

}

//BYTE GetContactStatus(void)
//{
//	return ContactOutputs;
//}

BYTE GetContactStatus(void)
{
	ContactOutputs = 0;

	if(PIN_STATUS(Contact_Closure1))
	{
		ContactOutputs |= 1<<0;
	}
	else
	{
		ContactOutputs &= ~(1<<0);
	}

	if(PIN_STATUS(Contact_Closure2))
	{
		ContactOutputs |= 1<<1;
	}
	else
	{
		ContactOutputs &= ~(1<<1);
	}

	if(PIN_STATUS(Contact_Closure3))
	{
		ContactOutputs |= 1<<2;
	}
	else
	{
		ContactOutputs &= ~(1<<2);
	}

	if(PIN_STATUS(Contact_Closure4))
	{
		ContactOutputs |= 1<<3;
	}
	else
	{
		ContactOutputs &= ~(1<<3);
	}

	return ContactOutputs;
}


void InitContactClosuresTimers(void)
{
	 thContact1 = RegisterTimer();
	 thContact2 = RegisterTimer();
	 thContact3 = RegisterTimer();
	 thContact4 = RegisterTimer();
	 StartTimer(thContact1);
	 StartTimer(thContact2);
	 StartTimer(thContact3);
	 StartTimer(thContact4);
}

//******************************************************************************
//
//  Function: ContactTimeoutTask
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: Check for expired timer on each contact and deactivate
//				the contact if expired
//
//
//******************************************************************************
void ContactTimeoutTask(void)
{
	if(TimerExpired( thContact1))
	{
		PIN_CLR(Contact_Closure1);
		PIN_CLR(LED1);
	}
	if(TimerExpired( thContact2))
	{
		PIN_CLR(Contact_Closure2);
		PIN_CLR(LED2);
	}
	if(TimerExpired( thContact3))
	{
		PIN_CLR(Contact_Closure3);
		PIN_CLR(LED3);
	}
	if(TimerExpired( thContact4))
	{
		PIN_CLR(Contact_Closure4);
		PIN_CLR(LED4);
	}
}

//******************************************************************************
//
//  Function: ComPortClosed
//
//  Arguments: void.
//
//  Returns: BOOL.
//
//  Description: We can use RTS line as an indication of the USBport state.
// 				RTS is low when the port is open, and it's high when is closed.
//              return TRUE when the port is CLOSED
//				return FALSE when the port is OPEN
//******************************************************************************
BOOL ComPortClosed(void)
{
	if (PIN_STATUS(ZC_UART_RTS))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}



//******************************************************************************
//
//  Function: KickWatchDog
//
//  Arguments: none
//
//  Returns: void
//
//  Description: Once the driver library has been initialized either UpdateDriverLibrary
//               or this function must be called every 800 milliseconds to prevent
//               reset due to the watch dog timer expiring.
//
//******************************************************************************
void KickWatchDog( void )
{
    LPC_WDT->WDFEED = 0xaa;
    LPC_WDT->WDFEED = 0x55;
}

void InitWDT(void)
{
    // enable WDT
    // set WDT reload value
    LPC_WDT->WDTC = WDTC_REG_VALUE;

    // enable the WDT and allow it to reset the core on timeout
    LPC_WDT->WDMOD = 3;
}




//******************************************************************************
//
//  Function: GetResetReason
//
//  Arguments: none
//
//  Returns: void
//
//  Description: saves the reason for the last reset in variable:
//                  1 - reset resulted from power up of board
//                  2 - reset resulted from WDT timeout
//                  3 - reset resulted from power brown out
//                  RT_EXCEPTION - reset resulted from other source, likely fault handler
//
//******************************************************************************
void GetResetReason( void )
{
    if( LPC_SC->RSID & (1<<0) )
    {
        // power on reset
    	lastReset=1;
    }
    else if( LPC_SC->RSID & (1<<2) )
    {
        // watchdog reset
    	lastReset = 2;
    }
    else if( LPC_SC->RSID & (1<<3) )
    {
        // brown out reset
    	lastReset = 3;
    }
    else
    {
    	// assume some other fault caused reset
    	lastReset = (BYTE)LPC_SC->RSID;
    }

}



BYTE GetLastReset(void)
{
	return lastReset;
}




//******************************************************************************
//
//******************************************************************************
void inlineDelay(DWORD dwNOPCount )
{

    DWORD dwCount;

    dwNOPCount = dwNOPCount*35;

    for( dwCount = 0; dwCount < dwNOPCount; dwCount++ )
    {
        asm ("NOP");

    }



}



//******************************************************************************
//
//  Function: EnableInts
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: This function enables the system interrupts using inline
//               Assembly ocde.
//
//******************************************************************************
void EnableInts( void )
{
    __asm volatile ("cpsie i" );
}


//******************************************************************************
//
//  Function: DisableInts
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: This function disables the system interrupts using
//               inline Assembly code.
//
//******************************************************************************
void DisableInts( void )
{
    __asm volatile ("cpsid i" );
}






//******************************************************************************
//
//******************************************************************************
WORD divByUsingAddition( DWORD dwNumerator, WORD wDenominator )
{

	DWORD dwAccumulator;
	DWORD dwIndex;
	DWORD dwDenominator;



	if( wDenominator == 0 )
		return 0;

	if( dwNumerator < wDenominator )
		 return 0;


	dwDenominator = wDenominator;
    dwAccumulator = 0;
    dwIndex       = 0;
    while(dwAccumulator < dwNumerator )
    {
    	if( (dwNumerator - dwAccumulator) < dwDenominator  )
    	{
    		dwAccumulator = dwNumerator;
    	}
    	else
    	{
    	    dwAccumulator += dwDenominator;
            dwIndex++;
    	}
    }

    return (WORD) dwIndex;


}


// the 16-bit DAC reference is 2500 mV
// Full scae is 65535
// therefore, LSB/V = 65535/2.5 = 26214
// OR, milliLSB/milliV = 26214

#define MILLI_LSB_PER_MILLI_VOLT 26214
#define MAX_MILLI_VOLTS_OUT 2500
//******************************************************************************
//
//******************************************************************************
WORD convertVoltsTo16BitDacOn2500milliVoltReference( WORD wMilliVoltsOut )
{


    DWORD dwMilliLSBs;
    DWORD dwLSBs;
    DWORD dwMilliVoltsOut;

    if( wMilliVoltsOut > MAX_MILLI_VOLTS_OUT)
        return 0;

    // first, calculate the number of 'milliLSB'
    dwMilliVoltsOut = (DWORD)wMilliVoltsOut;
    dwMilliLSBs = dwMilliVoltsOut*MILLI_LSB_PER_MILLI_VOLT;

    // now, we can only set the DAC in actual LSB units, so
    // just divide the milliLSB by 1000 to determine how many dac LSB units:

    dwLSBs = divByUsingAddition( dwMilliLSBs, 1000 );

    return ((WORD)dwLSBs);

}




