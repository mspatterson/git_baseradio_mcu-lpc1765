//******************************************************************************
//
//  WTTTSProtocolUtils.c: Communication protocol routines
//
//  Copyright (c) 2013, Microlynx Systems Ltd.
//  ALL RIGHTS RESERVED
//
//  This module provides functions to read and write to the information memory
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2013-03-26     DD          Initial Version
//
//******************************************************************************

#include "BaseStnDefs.h"
#include "HardwareAbstraction.h"
#include "LDMain.h"

//
// Private Declarations
//

// The WTTTS packet parser is table driven. To add a new command
// or response, do the following:
//
//   1. In the .h file, add a new WTTTS_CMD_ or WTTTS_RESP_ define
//      (if required)
//
//   2. Change the NBR_CMD_TYPES or NBR_RESP_TYPES define (if
//      a new cmd or response was defined).
//
//   3. If a new command or response has been defined, and that
//      item has a data payload, define a structure for the
//      payload in the .h file, and add a member of that structure
//      type to the WTTTS_DATA_UNION union definition.
//
//   4. Finally, add a row to (for new cmds or responses) or
//      modify an existing entry in teh m_pktInfo[] table.

#define NBR_CMD_TYPES  10   // Total number of WTTTS_CMD_... defines
#define NBR_RESP_TYPES 4   // Total number of WTTTS_RESP_... defines
#define RFC_RATE_DEFAULT	1000		//ms
#define RFC_TIMEOUT_DEFAULT	10			//ms
#define STREAM_RATE_DEFAULT	10			//ms
#define STREAM_TIMEOUT_DEFAULT	300		//seconds
#define PAIR_TIMEOUT_DEFAULT	900		//seconds
#define RF_PACKET_NUMBER		128

#define RFC_RATE_MIN		10		//ms
#define RFC_TIMEOUT_MIN		3			//ms
#define STREAM_RATE_MIN		9			//ms
#define STREAM_TIMEOUT_MIN	5		//seconds
#define PAIR_TIMEOUT_MIN	10		//seconds

typedef struct {
    BYTE pktHdr;        // Expected header byte
    BYTE cmdRespByte;   // One of the WTTTS_CMD_... or WTTTS_RESP_ defines
    BYTE expDataLen;    // Required value for header pktLen param
} WTTTS_PKT_INFO;

static const WTTTS_PKT_INFO m_pktInfo[ ] = {
    { BASE_STN_HDR_RX, BASE_STN_CMD_GET_STATUS,			0 },
    { BASE_STN_HDR_RX, BASE_STN_CMD_SET_CHAN_NBR,		sizeof(BASE_STN_SET_RADIO_CHAN) },
    { BASE_STN_HDR_RX, BASE_STN_CMD_SET_RF_PWR,			sizeof(BASE_STN_SET_RADIO_PWR) },
    { BASE_STN_HDR_RX, BASE_STN_CMD_SET_FCC_MODE,    sizeof( BASE_STN_SET_FCC_TEST_MODE ) },
    { BASE_STN_HDR_RX, BASE_STN_CMD_SET_CHAN_PARAM, 	0 },	// TBD
    { BASE_STN_HDR_RX, BASE_STN_CMD_SET_UPDATE_RATE,	sizeof( BASE_STN_SET_RATE_PKT ) },
    { BASE_STN_HDR_RX, BASE_STN_CMD_SET_PWR_STATE,      0 },	// TBD
    { BASE_STN_HDR_RX, BASE_STN_CMD_SET_OUTPUTS, 		sizeof(BASE_STN_SET_OUTPUTS_PKT) },
    { BASE_STN_HDR_RX, BASE_STN_CMD_SET_PROGRAM_MODE,    sizeof( BASE_STN_SET_PROGRAM_MODE ) },
    { BASE_STN_HDR_RX, BASE_STN_CMD_RESET_FIRMWARE,   	0 }

};

typedef struct {
    BYTE radioChannel;
    BYTE radioRssi;
    BYTE radioPower;
    BYTE radioInputs;
    WORD radioBitRate;
} RADIO_STAT_INFO;

static RADIO_STAT_INFO	radioStatInfo[NBR_BASE_RADIO_CHANS];

static BYTE radioPacket[RF_PACKET_NUMBER];
static BYTE pData[20];

static BYTE *wtttsPacket;
static BYTE sequenceNumber;
static BYTE currMode =1;
static BOOL  havePkt = FALSE;
//static BOOL  radioBusy = FALSE;
WTTTS_PKT_HDR* pktHdr;
BYTE* pktData;

static BYTE lastReset;
static WORD statUpdateRate;
static BYTE testStatus;


BYTE usbCtrlSignals;

void word2ByteArray(void *pVoid, WORD wValue);

//******************************************************************************
//
//  Function: SendBaseStnStatus
//
//  Arguments:	none
//
//  Returns: void.
//
//  Description: This function sends the Status event packet
//
//******************************************************************************
void SendBaseStnStatus(void)
{
	WTTTS_PKT_HDR*	pBaseStatusHeader;
	BASE_STATUS_STATUS_PKT*	pBaseStatusPacket;
	BYTE*	pBaseStatusCRC;
	BYTE *pByte;

	// Initialise the pointers
	pBaseStatusHeader = (WTTTS_PKT_HDR*)radioPacket;
	pBaseStatusPacket = (BASE_STATUS_STATUS_PKT*)&radioPacket[sizeof(WTTTS_PKT_HDR)];
	pBaseStatusCRC	= (BYTE*)&radioPacket[sizeof(WTTTS_PKT_HDR)+sizeof(BASE_STATUS_STATUS_PKT)];

	pBaseStatusHeader->pktHdr = BASE_STN_HDR_TX;
	pBaseStatusHeader->pktType = BASE_STN_RESP_STATUS;
	pBaseStatusHeader->seqNbr = sequenceNumber;		// Base Radio shall echo back the sequence number of the last received command
	pBaseStatusHeader->dataLen = sizeof( BASE_STATUS_STATUS_PKT );
	pBaseStatusHeader->timeStamp = 0;		// not used in this protocol

	pBaseStatusPacket->radioInfo[0].chanNbr = radioStatInfo[0].radioChannel;
	pBaseStatusPacket->radioInfo[0].rssi = radioStatInfo[0].radioRssi;
	pBaseStatusPacket->radioInfo[0].bitRate = radioStatInfo[0].radioBitRate;
	pBaseStatusPacket->radioInfo[1].chanNbr = radioStatInfo[1].radioChannel;
	pBaseStatusPacket->radioInfo[1].rssi = radioStatInfo[1].radioRssi;
	pBaseStatusPacket->radioInfo[1].bitRate = radioStatInfo[1].radioBitRate;
	pBaseStatusPacket->radioInfo[2].chanNbr = radioStatInfo[2].radioChannel;
	pBaseStatusPacket->radioInfo[2].rssi = radioStatInfo[2].radioRssi;
	pBaseStatusPacket->radioInfo[2].bitRate = radioStatInfo[2].radioBitRate;

	pBaseStatusPacket->lastResetType = GetLastReset();	// to do
	pBaseStatusPacket->outputStatus = GetContactStatus();
	pBaseStatusPacket->statusUpdateRate = GetStatUpdateRate();	// to do

	pBaseStatusPacket->fwVer[0]= MAJOR_VER;
	pBaseStatusPacket->fwVer[1]= MINOR_VER;
	pBaseStatusPacket->fwVer[2]= SUB_MINOR_VER;
	pBaseStatusPacket->fwVer[3]= BUILD_VER;

	usbCtrlSignals = 0;
	if(radioStatInfo[0].radioInputs !=0)
	{
		usbCtrlSignals |=0x01;
	}
	if(radioStatInfo[1].radioInputs !=0)
	{
		usbCtrlSignals |=0x02;
	}
	if(radioStatInfo[2].radioInputs !=0)
	{
		usbCtrlSignals |=0x04;
	}
	if (PIN_STATUS(ZC_UART_RTS))
	{
		usbCtrlSignals |=0x08;
	}

	pBaseStatusPacket->usbCtrlSignals = usbCtrlSignals;

	pBaseStatusPacket->byRFU[0]= 0;		// not used in this protocol
	pBaseStatusPacket->byRFU[1]= 0;		// not used in this protocol

	*pBaseStatusCRC = CalculateCRC(radioPacket, sizeof(WTTTS_PKT_HDR)+sizeof(BASE_STATUS_STATUS_PKT));

	WriteToUsbPort(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(BASE_STATUS_STATUS_PKT)+1);

}

//******************************************************************************
//
//  Function: HaveCommPacket
//
//  Arguments:	none
//
//  Returns: void.
//
//  Description: This function sends the Status event packet
//
//******************************************************************************
BOOL HaveCommPacket(BYTE *pBuff, WORD buffLen)
{
	   // Scans the passed buffer for a Base Radio command or response. Returns true if
	    // a packet is found, in which case pktHdr and pktData pointers are asigned.
	    // Returns false if no packet is found.
    WORD bytesSkipped = 0;

    BOOL foundInfoItem = FALSE;
    int  dataLen       = 0;
    int  expPktLen     = 0;
    unsigned int iInfoItem = 0;
    BYTE expCRC;

    havePkt = FALSE;

    if( buffLen < (WORD)MIN_BASE_STN_PKT_LEN )
        return FALSE;

    while( bytesSkipped + (WORD)MIN_BASE_STN_PKT_LEN <= buffLen )
       {

           // Have enough bytes for a packet. Header byte must be the first
           // byte we see
           if( pBuff[bytesSkipped] != BASE_STN_HDR_RX )
           {
               bytesSkipped++;
               continue;
           }

           // Looks like the start of a header. See if the rest of the packet
           // makes sense.
           //WTTTS_PKT_HDR* pHdr = (WTTTS_PKT_HDR*)&( pBuff[bytesSkipped] );
           pktHdr = (WTTTS_PKT_HDR*)&( pBuff[bytesSkipped] );

           for(iInfoItem = 0; iInfoItem < NBR_CMD_TYPES ; iInfoItem++ )
           {

               if( m_pktInfo[iInfoItem].cmdRespByte != pktHdr->pktType )
                   continue;

               // Found a hdr / type pair match. Calc the number of bytes required
               // for this packet. That would be a header, the data portion, and
               // a checksum byte.
               dataLen   = m_pktInfo[iInfoItem].expDataLen;
               expPktLen = MIN_BASE_STN_PKT_LEN + dataLen;

               foundInfoItem = TRUE;
               break;
           }

           if( !foundInfoItem )
           {
               bytesSkipped++;
               continue;
           }

           // If we have enough bytes, check the CRC. Otherwise, we have to
           // wait for more bytes to come in
           if( bytesSkipped + expPktLen > buffLen )
               break;

           // Have enough bytes - validate the CRC
           expCRC = CalculateCRC( &( pBuff[bytesSkipped] ), expPktLen - 1 );

           if( pBuff[ bytesSkipped + expPktLen - 1 ] == expCRC )
           {
               // Success!
              // memcpy( &pktHdr, &( pBuff[bytesSkipped] ), sizeof( WTTTS_PKT_HDR ) );
        	   pktHdr = (WTTTS_PKT_HDR*)&pBuff[bytesSkipped];

               if( dataLen > 0 )
               {
                  // memcpy( &pktData, &( pBuff[bytesSkipped + sizeof( WTTTS_PKT_HDR )] ), dataLen );
            	   pktData = &pBuff[bytesSkipped + sizeof( WTTTS_PKT_HDR )] ;
               }
               // Remove this packet from the buffer
               bytesSkipped += expPktLen;

               havePkt = TRUE;
               break;
           }

           // CRC error - continue scanning
           bytesSkipped++;
       }

    return havePkt;
}

//******************************************************************************
//
//  Function: ExecuteCommand
//
//  Arguments:	none
//
//  Returns: void.
//
//  Description: This function executes the command send over the USB
//
//******************************************************************************
void ExecuteCommand(void)
{

	sequenceNumber = pktHdr->seqNbr;
	switch(pktHdr->pktType)
	{
		case BASE_STN_CMD_GET_STATUS:
			ResetStatusTimer();
			break;
		case BASE_STN_CMD_SET_CHAN_NBR:
			SetRadioChannel(pktData[0],pktData[1]);
			break;
		case BASE_STN_CMD_SET_RF_PWR:
			SetRadioPower(pktData[0],pktData[1]);
			break;
		case BASE_STN_CMD_SET_FCC_MODE:
			SetTestStatus(pktData[0]);
			break;
		case BASE_STN_CMD_SET_CHAN_PARAM:

			break;
		case BASE_STN_CMD_SET_UPDATE_RATE:

			break;
		case BASE_STN_CMD_SET_PWR_STATE:

			break;
		case BASE_STN_CMD_SET_OUTPUTS:
			SetContactClosure(pktData[0]);
			// test - enable programming only if contacts 3 and 4 are set at the same time
			if(pktData[0] == 0x0C)
			{
				PIN_SET(USB_PROG_DISABLE);
				PIN_DIR_OUT( USB_PROG_DISABLE );
				PIN_CLR(USB_PROG_EN);
				PIN_DIR_OUT( USB_PROG_EN );
			}
			else	// disable programming mode
			{
				PIN_CLR(USB_PROG_DISABLE);
				PIN_DIR_OUT( USB_PROG_DISABLE );
				PIN_SET(USB_PROG_EN);
				PIN_DIR_OUT( USB_PROG_EN );
			}
			break;
		case BASE_STN_CMD_RESET_FIRMWARE:

			break;
		case BASE_STN_CMD_SET_PROGRAM_MODE:	// enable programming mode
			PIN_SET(USB_PROG_DISABLE);
			PIN_DIR_OUT( USB_PROG_DISABLE );
			PIN_CLR(USB_PROG_EN);
			PIN_DIR_OUT( USB_PROG_EN );
			break;
		case BASE_STN_CMD_DISABLE_PROGRAM_MODE:	// disable programming mode
			PIN_CLR(USB_PROG_DISABLE);
			PIN_DIR_OUT( USB_PROG_DISABLE );
			PIN_SET(USB_PROG_EN);
			PIN_DIR_OUT( USB_PROG_EN );
			break;
//		case BASE_STN_CMD_TEST_STATUS:
//			SetTestStatus(pktData[0]);
//			break;
		default:
			break;
	}
}


// TO DO
BYTE GetCurrMode(void)
{
	return currMode;
}

//******************************************************************************
//
//  Function: ConvertLongTo4Bytes
//
//  Arguments:	Value to be converted, array to store the result
//
//  Returns: void.
//
//  Description: Parse a 32 bit long value to array of four bytes with least significant byte first
//
//******************************************************************************
void ConvertLongTo4Bytes(signed long slValue, BYTE *pData)
{

	pData[0] = 	(BYTE) slValue;
	pData[1] = 	(BYTE) (slValue>>8);
	pData[2] = 	(BYTE) (slValue>>16);
	pData[3] = 	(BYTE) (slValue>>24);

}

//******************************************************************************
//
//  Function: word2ByteArray
//
//  Arguments:	Value to be converted, array to store the result
//
//  Returns: void.
//
//  Description: Parse a 16 bit WORD value to array of two bytes with least significant byte first
//
//******************************************************************************
void word2ByteArray(void *pVoid, WORD wValue)
{
	BYTE *pByte;
	WORD wTemp;
	wTemp = wValue;	// function to do
	pByte = (BYTE*)	pVoid;
	*pByte++ = (BYTE)wTemp;
	*pByte = (BYTE)(wTemp>>8);
}


//******************************************************************************
//
//  Function: SendGetRadioStatCommand
//
//  Arguments:	radio number
//
//  Returns: void.
//
//  Description: send Get Radio Status command
//
//******************************************************************************
void SendGetRadioStatCommand(BYTE radioNbr)
{
	pData[0] = COMMAND;
	pData[1] = GET_RADIO_STATUS;

	switch(radioNbr)
	{
	case 0:
		WriteToRF1Port(pData,2);
		break;
	case 1:
		WriteToRF2Port(pData,2);
		break;
	default:
		break;
	}
}

//******************************************************************************
//
//  Function: SetRadioChannel
//
//  Arguments:	radio number, channel
//
//  Returns: void.
//
//  Description: send Set Radio Channel command
//
//******************************************************************************
void SetRadioChannel(BYTE radioNbr, BYTE channel)
{
	pData[0] = COMMAND;
	pData[1] = RADIO_CHANGE_CHANNEL;
	pData[2] = channel;

	switch(radioNbr)
	{
	case 0:
		WriteToRF1Port(pData,3);
		break;
	case 1:
		WriteToRF2Port(pData,3);
		break;
	default:
		break;
	}
}

//******************************************************************************
//
//  Function: SetRadioPower
//
//  Arguments:	radio number, power
//
//  Returns: void.
//
//  Description: send Set Radio Tx Power command
//
//******************************************************************************
void SetRadioPower(BYTE radioNbr, BYTE power)
{
	pData[0] = COMMAND;
	pData[1] = RADIO_CHANGE_POWER;
	pData[2] = power;

	switch(radioNbr)
	{
	case 0:
		WriteToRF1Port(pData,3);
		break;
	case 1:
		WriteToRF2Port(pData,3);
		break;
	default:
		break;
	}
}

//******************************************************************************
//
//  Function: SetRadioInSleep
//
//  Arguments:	radio number
//
//  Returns: void.
//
//  Description: send Set Radio Sleep command
//
//******************************************************************************
void SetRadioInSleep(BYTE radioNbr)
{
	pData[0] = COMMAND;
	pData[1] = ENTER_PM_3;

	switch(radioNbr)
	{
	case 0:
		WriteToRF1Port(pData,2);
		break;
	case 1:
		WriteToRF2Port(pData,2);
		break;
	default:
		break;
	}
}

//******************************************************************************
//
//  Function: ParseStatusData
//
//  Arguments:	radio number, data array to parse, length of data
//
//  Returns: void.
//
//  Description: Parse the received status data from the radio to the corresponding data structure.
//
//******************************************************************************
void ParseStatusData(BYTE radioNbr,BYTE *pData,WORD wLength)
{
	BYTE index;
	index = radioNbr;
	if(radioNbr>NBR_BASE_RADIO_CHANS)
		return;
	radioStatInfo[index].radioChannel = pData[1];
	radioStatInfo[index].radioRssi = pData[2];
	radioStatInfo[index].radioInputs = pData[3];
	radioStatInfo[index].radioPower = pData[4];

}



WORD GetStatUpdateRate(void)
{
	return	statUpdateRate;
}

//******************************************************************************
//
//  Function: CalculateCRC
//
//  Arguments:	data array , length of data
//
//  Returns: one byte CRC
//
//  Description: Calculate one byte CRC on the provided data
//
//******************************************************************************
BYTE CalculateCRC(BYTE *bPointer, WORD wLength)
{
	const WORD c1 = 52845;
    const WORD c2 = 22719;
    WORD r;
    WORD i;
    DWORD sum;
	BYTE cipher;

	r = 55665;
	sum = 0;

	for( i=0; i<wLength;i++)
	{
		cipher = ( *bPointer ^ ( r >> 8 ) );
		r = ( cipher + r ) * c1 + c2;
		sum += cipher;
		bPointer++;
	}

	return (BYTE)sum;
}


void SetTestStatus(BYTE testSt)
{
	testStatus = testSt;
}

BOOL GetTestStatus(void)
{
	if(testStatus)
		return TRUE;
	else
		return FALSE;
}



