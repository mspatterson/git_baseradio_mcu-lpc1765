//******************************************************************************
//
//  ADC.c: LPC17xx ADC module
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module provides access and control of the LPC17xx on board A/D converter.
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2011-04-26      John     Initial Implementation
//
//******************************************************************************

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "ADC.h"
#include "target.h"



//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------
#define ADCR_PDN_BIT        21
#define ADCR_START_BIT      24

// ADC pin enable and mode select macros
// P0.23, A0.0, function 01
#define AD0_0_PIN_ENABLE    LPC_PINCON->PINSEL1 &= ~(1<<15); LPC_PINCON->PINSEL1 |= (1<<14)
#define AD0_0_MODE_SEL      LPC_PINCON->PINMODE1 |= (1<<15); LPC_PINCON->PINMODE1 &= ~(1<<14)
// P0.24, A0.1, function 01
#define AD0_1_PIN_ENABLE    LPC_PINCON->PINSEL1 &= ~(1<<17); LPC_PINCON->PINSEL1 |= (1<<16)
#define AD0_1_MODE_SEL      LPC_PINCON->PINMODE1 |= (1<<17); LPC_PINCON->PINMODE1 &= ~(1<<16)
// P0.25, A0.2, function 01
#define AD0_2_PIN_ENABLE    LPC_PINCON->PINSEL1 &= ~(1<<19); LPC_PINCON->PINSEL1 |= (1<<18)
#define AD0_2_MODE_SEL      LPC_PINCON->PINMODE1 |= (1<<19); LPC_PINCON->PINMODE1 &= ~(1<<18)
// P0.26, A0.3, function 01
#define AD0_3_PIN_ENABLE    LPC_PINCON->PINSEL1 &= ~(1<<21); LPC_PINCON->PINSEL1 |= (1<<20)
#define AD0_3_MODE_SEL      LPC_PINCON->PINMODE1 |= (1<<21); LPC_PINCON->PINMODE1 &= ~(1<<20)
// P1.30, A0.4, function 11
#define AD0_4_PIN_ENABLE    LPC_PINCON->PINSEL3 |= (0x3<<28);
#define AD0_4_MODE_SEL      LPC_PINCON->PINMODE3 |= (1<<29); LPC_PINCON->PINMODE3 &= ~(1<<28)
// P1.31, A0.5, function 11
#define AD0_5_PIN_ENABLE    LPC_PINCON->PINSEL3 |= (0x3<<30);
#define AD0_5_MODE_SEL      LPC_PINCON->PINMODE3 |= (1<<31); LPC_PINCON->PINMODE3 &= ~(1<<30)
// P0.3, A0.6, function 10
#define AD0_6_PIN_ENABLE    LPC_PINCON->PINSEL0 |= (1<<7); LPC_PINCON->PINSEL0 &= ~(1<<6)
#define AD0_6_MODE_SEL      LPC_PINCON->PINMODE0 |= (1<<7); LPC_PINCON->PINMODE0 &= ~(1<<6)
// P0.2, A0.7, function 10
#define AD0_7_PIN_ENABLE    LPC_PINCON->PINSEL0 |= (1<<5); LPC_PINCON->PINSEL0 &= ~(1<<4)
#define AD0_7_MODE_SEL      LPC_PINCON->PINMODE0 |= (1<<5); LPC_PINCON->PINMODE0 &= ~(1<<4)


//------------------------------------------------------------------------------
//  GLOBAL DECLARATIONS
//------------------------------------------------------------------------------
static BYTE EnabledChnls = 0;
static BYTE ClkDivider = 0;


//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
void (*AdcIsrCallback)( void );


//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS
//------------------------------------------------------------------------------


//******************************************************************************
//
//  Function: AdcInit
//
//  Arguments: CtrlRegVal - value to be programmed to ADCR
//                          b7:0 channel select bits
//                          b15:8 pclk divider (this value + 1)
//                                a single conversion will take 65 of the divided clks
//                          b16 set to enable burst mode (continuous conversions)
//                          b21 must be set to enable ADC
//                          b26:24 start mode select (if not burst mode)
//                          b27 start edge select
//             IntEnRegVal - Interrupt enable register value
//                           b7:0 channel convert done interrupt enables
//                           b8 global convert done interrupt enable
//             IntPriority - ADC interrupt priority, 0 = highest, 31 = lowest
//             AdcCallBack - function pointer for ADC ISR callback function
//
//  Returns: TRUE if initialization successful, else FALSE
//
//  Description: Call this function to initialise the ADC module.  Any channels
//               selected in channel select bits will have input pins configured.
//
//******************************************************************************
BOOL AdcInit( DWORD CtrlRegVal, DWORD IntEnRegVal, BYTE IntPriority, void (*AdcCallBack)( void ) )
{
    // enable power to ADC
    LPC_SC->PCONP |= (1<<12);

    // set ADC pclk to sysclk/4
// DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
//    LPC_SC->PCLKSEL0 &= ~(3<<24);

    // set up pins for all enabled channels
    if( CtrlRegVal & 0x0001 )
    {
        // channel 0 enabled, configure input
        AD0_0_PIN_ENABLE;
        AD0_0_MODE_SEL;
    }
    if( CtrlRegVal & 0x0002 )
    {
        // channel 1 enabled, configure input
        AD0_1_PIN_ENABLE;
        AD0_1_MODE_SEL;
    }
    if( CtrlRegVal & 0x0004 )
    {
        // channel 2 enabled, configure input
        AD0_2_PIN_ENABLE;
        AD0_2_MODE_SEL;
    }
    if( CtrlRegVal & 0x0008 )
    {
        // channel 3 enabled, configure input
        AD0_3_PIN_ENABLE;
        AD0_3_MODE_SEL;
    }
    if( CtrlRegVal & 0x0010 )
    {
        // channel 4 enabled, configure input
        AD0_4_PIN_ENABLE;
        AD0_4_MODE_SEL;
    }
    if( CtrlRegVal & 0x0020 )
    {
        // channel 5 enabled, configure input
        AD0_5_PIN_ENABLE;
        AD0_5_MODE_SEL;
    }
    if( CtrlRegVal & 0x0040 )
    {
        // channel 6 enabled, configure input
        AD0_6_PIN_ENABLE;
        AD0_6_MODE_SEL;
    }
    if( CtrlRegVal & 0x0080 )
    {
        // channel 7 enabled, configure input
        AD0_7_PIN_ENABLE;
        AD0_7_MODE_SEL;
    }

    // write control register value
    LPC_ADC->ADCR = CtrlRegVal;

    // set the ADC interrupt enable register value
    LPC_ADC->ADINTEN = IntEnRegVal;

    // setup ISR callback function
    AdcIsrCallback = AdcCallBack;
	
	// remember clk divider and which channels are enabled
	EnabledChnls = (BYTE) CtrlRegVal;
    ClkDivider = (BYTE) (CtrlRegVal >> 8);

    // enable the ADC interrupt in the NVIC if required
    if( IntEnRegVal != 0 )
    {
        NVIC_EnableIRQ( ADC_IRQn );
        NVIC_SetPriority( ADC_IRQn, IntPriority );
    }
	
	return( TRUE );
}


//******************************************************************************
//
//  Function: AdcStartConversion
//
//  Arguments: ChnlSelect - channel to perform conversion (0 to 7)
//
//  Returns: TRUE if successful, else FALSE
//
//  Description: This function will perform a manual start conversion on the
//               selected ADC channel.  Note the selected channel must have been
//               enabled in AdcInit and the ADC must not be in burst mode.
//
//******************************************************************************
BOOL AdcStartConversion( BYTE ChnlSelect )
{
    // check if selected channel is valid
    if( (EnabledChnls & (1 << ChnlSelect)) == 0 )
    {
        return( FALSE );
    }
    
    // write to control register with selected channel, clkdiv value,
    // ADC enable bit set and start bit set
    LPC_ADC->ADCR = (1 << ChnlSelect) | (ClkDivider << 8) | (1 << ADCR_PDN_BIT) | (0x1 << ADCR_START_BIT);
    
    return( TRUE );
}


//******************************************************************************
//
//  Function: AdcReadChannel
//
//  Arguments: ChnlSelect - channel to read (0 to 7, or ADC_GLOBAL_DATA)
//
//  Returns: ADC conversion value in format:
//                  b15:4   - 12 bit ADC conversion value
//                  b26:24  - channel number (global data only)
//                  b30     - Set if overrun occurred (a conversion value was overwitten)
//                  b31     - Conversion done, set when new conversion value is available
//
//  Description: This function will read the last ADC conversion value for the
//               selected ADC channel or if ADC_GLOBAL_DATA is select, the ADC
//               conversion value from the most recent channel conversion.  If
//               the selected channel is not valid, 0xffffffff will be returned.
//
//******************************************************************************
DWORD AdcReadChannel( BYTE ChnlSelect )
{
    // check if selected channel is valid
    if( ((EnabledChnls & (1 << ChnlSelect)) == 0) && (ChnlSelect != ADC_GLOBAL_DATA) )
    {
        return( 0xffffffff );
    }
    
    switch( ChnlSelect )
    {
        case ADC_GLOBAL_DATA:
            return( LPC_ADC->ADGDR );
        
        case 0:
            return( LPC_ADC->ADDR0 );
            
        case 1:
            return( LPC_ADC->ADDR1 );
            
        case 2:
            return( LPC_ADC->ADDR2 );
            
        case 3:
            return( LPC_ADC->ADDR3 );
            
        case 4:
            return( LPC_ADC->ADDR4 );
            
        case 5:
            return( LPC_ADC->ADDR5 );
            
        case 6:
            return( LPC_ADC->ADDR6 );
            
        case 7:
            return( LPC_ADC->ADDR7 );
    }
    
    // should not get here
    return( 0xffffffff );
}



//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS
//------------------------------------------------------------------------------

//******************************************************************************
//
//  Function: ADC_IRQHandler
//
//  Arguments:
//
//  Returns:
//
//  Description: ADC ISR - calls provided callback function if available
//
//******************************************************************************
extern void ADC_IRQHandler( void )
{
    volatile int Status;

    if( AdcIsrCallback != NULL )
    {
        AdcIsrCallback();
    }
    else
    {
        // read status register to clear interrupt
        Status = LPC_ADC->ADSTAT;
    }
}
