////******************************************************************************
////
////  MCPWM.c: LPC Motor Control PWM Module
////
////      Copyright (c) 2012, Microlynx Systems Ltd
////      ALL RIGHTS RESERVED
////
////  This module provides control of the LPC internal PWM hardware.  Currently
////  only supports single edge controlled PWMs with no interrupt support.
////
////  Date            Author      Comment
////  ----------------------------------------------------------------
////  2012-07-13      JK      Initial Implementation
////
////******************************************************************************
//
//
////------------------------------------------------------------------------------
////  INCLUDES
////------------------------------------------------------------------------------
//// #include "PWM.h"
//#include "target.h"
//#include "typedefs.h"
//
//
////------------------------------------------------------------------------------
////  CONSTANT & MACRO DEFINITIONS
////------------------------------------------------------------------------------
//#define PCMCPWM (1<<17)
//
//#define TMR_CHAN_0_START (1<<0)
//#define TMR_CHAN_1_START (1<<8)
//#define TMR_CHAN_2_START (1<<16)
//
//
//
//
//// pin configuration macros (uncomment appropriate pin selection)
//// pin 1.19 set to function 01 for MCOA0
//#define MCOA0_PIN_ENABLE   LPC_PINCON->PINSEL3 &= ~(1<<7); LPC_PINCON->PINSEL3 |= (1<<6)
//
//
//// pin configuration macros (uncomment appropriate pin selection)
//// pin 1.22 set to function 01 for MCOB0
//// #define MCOB0_PIN_ENABLE   LPC_PINCON->PINSEL3 &= ~(1<<13); LPC_PINCON->PINSEL3 |= (1<<12)
//
//
//// pin 1.25 set to function xx for MCOA1
//#define MCOA1_PIN_ENABLE   LPC_PINCON->PINSEL3 &= ~(1<<19); LPC_PINCON->PINSEL3 |= (1<<18)
//
//
//// pin 1.28 set to function 01 for MCOA2
//#define MCOA2_PIN_ENABLE   LPC_PINCON->PINSEL3 &= ~(1<<25); LPC_PINCON->PINSEL3 |= (1<<24)
//
//
//
//
//#define MCPWM_MAT2_INT (1<<9)
////******************************************************************************
////
////  Function: MCPwmInit
////
////  Arguments: PwmChnlEnables - b0:5 set corresponding bits to enable PWM chnl 1-6
////             PwmCounterFrequency - desired counter frequency for PWM in Hz
////                                   the minimum pulse length is 1/PwmCounterFrequency
////             CounterRstValue - Value at which PWM counter is reset, this determines
////                               the PWM period as: CounterRstValue/PwmCounterFrequency
////                               (counter starts from 1)
////
////  Returns: TRUE if successful, else FALSE
////
////  Description: This function will configure and enable the selected PWM channels.
////
////
////******************************************************************************
//BOOL MCPwmInit( WORD wRampSampleCount, WORD wSingleSamplePeriod, WORD wMarkPulseWidth )
//
//{
//
//	DWORD dwWaveformPeriod;
//
//    // Clock Calculations
//    // the sample count and single sample period should be sent to this function in
//    // units of 'RefClk' - i.e., they are a count of the number of refclks for each
//    // of those parameters. Now, the system cpu is a PLL multiplied version of this
//    // clock; therefore, we need to multiply each of those counts to account for
//    // this fact: i.e., that we are counting a clock that is a multiple of the refclk
//    // here; and therefore, we need to multiply the total counts
//
//
//	dwWaveformPeriod = wRampSampleCount*wSingleSamplePeriod*15;
//	wSingleSamplePeriod = wSingleSamplePeriod*15;
//
//    // enable power to MCPWM
//    LPC_SC->PCONP |= (PCMCPWM);
//
//
//    // set PWM pclk to sysclk/4
//    // DUE TO ERRATA THIS MUST BE DONE BEFORE PLL IS ENABLED (in system_LPC17xx.c)
//    // LPC_SC->PCLKSEL0 &= ~(3<<12);
//
//    // disable all PWM channels to start
//    LPC_PWM1->PCR = 0;
//    LPC_MCPWM->MCCON_CLR |= (TMR_CHAN_0_START | TMR_CHAN_1_START | TMR_CHAN_2_START );
//
//
//    // set the output pins accordingly
//	MCOA0_PIN_ENABLE;
//	MCOA2_PIN_ENABLE;
//	MCOA1_PIN_ENABLE;
//
//
//    // set the Channel0 MCPWM into timer mode
//    LPC_MCPWM->MCCNTCON_CLR |=(1<<29);
//
//    // set the channel2 MCPWM into timer mode
//    LPC_MCPWM->MCCNTCON_CLR |=(1<<31);
//
//    // set the timer0 'limit' which determines the period of the PWM
//    LPC_MCPWM->MCPER0 = dwWaveformPeriod    - 1;
//    LPC_MCPWM->MCPER1 = dwWaveformPeriod    - 1;
//    LPC_MCPWM->MCPER2 = wSingleSamplePeriod - 1;
//
//    // set the Match0  which determines the Pulse Width (relative to period) of the PWM
//    LPC_MCPWM->MCPW0 = dwWaveformPeriod    - wMarkPulseWidth;
//    LPC_MCPWM->MCPW0 = dwWaveformPeriod    - wMarkPulseWidth;
//    LPC_MCPWM->MCPW2 = wSingleSamplePeriod - wMarkPulseWidth;
//
//    LPC_MCPWM->MCINTEN_SET = MCPWM_MAT2_INT;
//
//    // enable the MCPWM interrupt in the NVIC
//#if ENABLE_RAMP_SAMPLE_INTERRUPT == 1
//    NVIC_EnableIRQ( MCPWM_IRQn );
//    NVIC_SetPriority( MCPWM_IRQn, MCPWM_IRQ_PRIORITY );
//#endif
//
//    // enable PWM channel
//    LPC_MCPWM->MCCON_SET |= ( TMR_CHAN_0_START | TMR_CHAN_1_START |  TMR_CHAN_2_START );
//
//
//    /*
//    // validate PwmCounterFrequency
//    if( PwmCounterFrequency > (SystemCoreClock/4) )
//    {
//        // PwmCounterFrequency is too high
//        return( FALSE );
//    }
//
//
//*/
//
//
//    return( TRUE );
//}
//
//
////******************************************************************************
////
////******************************************************************************
//void MCPwmDisable( void )
//{
//    // disable the interrupt
//    NVIC_DisableIRQ( MCPWM_IRQn );
//
//    // disable all PWM channels
//    LPC_PWM1->PCR = 0;
//    LPC_MCPWM->MCCON_CLR |= (TMR_CHAN_0_START | TMR_CHAN_1_START | TMR_CHAN_2_START );
//
//
//}
//
//
//
//
////******************************************************************************
////
////  Function: MCPWM_IRQHandler
////
////  Arguments:
////
////  Returns:
////
////  Description: PWM ISR - writes to latch enable bits so that updated match
////               register values will be used.  This was required to ensure
////               new match register values reliably got latched into PWM.
////
////******************************************************************************
//extern void MCPWM_IRQHandler(void)
//{
//
//    // clear interrupt
//    LPC_MCPWM->MCINTFLAG_CLR = MCPWM_MAT2_INT;
//
//
//    // processArbWaveRampSampleInt(wDummy);
//
//}
//
//
//
//
//

