//******************************************************************************
//
//  BoardStatus.h:
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
// Contains the wrapper for generating a sine-wave modulator signal via a
// dedicated DDS chip
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  Version 1.0     JK      
//
//******************************************************************************


#include "target.h"
#include "typedefs.h"
#include "ADC.h"
#include "hardwareAbstraction.h"
#include "timer.h"

////******************************************************************************
////
////******************************************************************************
//static TIMERHANDLE thADCWaitTimer;
//#define ADC_WAIT_INTERVAL_MS 10
//
////******************************************************************************
////
////******************************************************************************
//#define ADC_CHAN7_SEL 7
//#define ADC_CHAN6_SEL 6
//#define ADC_CHAN5_SEL 5
//#define ADC_CHAN0_SEL 0
//
//
//#define ADC_CLK_DIV    8
//
//#define ADC_BURST_BIT 16
//#define ADC_POWER     21
//#define ADC_START     24
//
//
//
//#define AM_OUTER_THERMISTOR_S1 0
//#define AM_LOCAL_THERMISTOR_S2 1
//#define AM_INNER_THERMISTOR_S3 2
//
//// S4 - TP
//// S5 - TP
//// S6 - TP
//// S7 - TP
//// S8 - TP
//
//#define AM_DDS_REFOUT_S9    8
//#define AM_LM35DM_TEMP_S10  9
//#define AM_LTEC_VTEC_S11   10
//#define AM_LTEC_ITEC_S12   11
//#define AM_IN_VTEC_S13     12
//#define AM_IN_ITEC_S14     13
//#define AM_OUT_VTEC_S15    14
//#define AM_OUT_ITEC_S16    15
//
//
//
////*****************************************************************************
////
////*****************************************************************************
//void setAnalogMuxInForSignal( BYTE byTheSignal )
//{
//
//
//
//}
//
//
//
////*****************************************************************************
////
////*****************************************************************************
//// 0	Laser Driver Board Hardware Type
//// 		This field indicates whether the hardware for the Outer TEC control is
//// 		implemented as 'Dither Control' or 'Loop Control'
////			0 = Loop
////			1 = Dither
////*****************************************************************************
////	1	Laser Current Drive: Ramp Feedback Loop Status	'1' Loop is locked
////		'0' Loop out of lock
////*****************************************************************************
//// 2	Laser Current Drive: Modulator Feedback Loop Status
////		'1' Loop is locked
////		'0' Loop out of lock
////*****************************************************************************
//// 3	Local TEC Temperature Good Indication	Indicates when the control loop that
////		regulates TEC heating and cooling is within 100mV of target voltage that
////		represents temperature setpoint.
////*****************************************************************************
//// 4	Local TEC Heating
////		'1' TEC is heating
////		'0' TEC not heating
////*****************************************************************************
//// 5	Local TEC Cooling
//// 		'1' TEC is cooling
////		'0' TEC is not cooling
////*****************************************************************************
//// 6	Inner TEC Temperature Good Indication	Indicates when the control loop
//// 		that regulates TEC heating and cooling is within 100mV of target voltage
//// 		that represents temperature setpoint
////*****************************************************************************
//// 7	Inner TEC Heating
////		'1' TEC is heating
////		'0' TEC not heating
////*****************************************************************************
////8	Inner TEC Cooling
////		'1' TEC is cooling
////		'0' TEC is not cooling
////*****************************************************************************
////9	Inner TEC Temperature Good Indication	Indicates when the control loop that
//// regulates TEC heating and cooling is within 100mV of target voltage that
//// represents temperature setpoint
////*****************************************************************************
//// 10	Outer TEC Heating
//// 		'1' TEC is heating
////		'0' TEC not heating
////*****************************************************************************
//// 11	Outer TEC Cooling
////		'1' TEC is cooling
////		'0' TEC is not cooling
//
//
//
//#define LD_HW_TYPE_POS    0
//#define RAMP_CURNT_POS    1
//#define MOD_CURNT_POS     2
//
//#define LTEC_TMP_GD_POS   3
//#define LTEC_TMP_HEAT_POS 4
//#define LTEC_TMP_COOL_POS 5
//
//#define ITEC_TMP_GD_POS   6
//#define ITEC_TMP_HEAT_POS 7
//#define ITEC_TMP_COOL_POS 8
//
//#define OTEC_TMP_GD_POS   9
//#define OTEC_TMP_HEAT_POS 10
//#define OTEC_TMP_COOL_POS 11
//
//
//WORD wBrdStatusReg;
//
////*****************************************************************************
////
////*****************************************************************************
//void doBoardStatusUpdate( void )
//{
//
//
//
//
//}
//
////*****************************************************************************
////
////*****************************************************************************
//WORD getBoardStatusReg( void )
//{
//
//    doBoardStatusUpdate( );
//
//    return wBrdStatusReg;
//
//}
//
////*****************************************************************************
//// 3:0 - Reserved, user software should not write ones to reserved bits.
////       The value read from a reserved bit is not defined.
////
//// 15:4 RESULT When DONE is 1, this field contains a binary fraction representing
////             the voltage on the AD0[n] pin selected by the SEL field, as it falls
////             within the range of VREFP to VREFN. Zero in the field indicates that
////             the voltage on the input pin was less than, equal to, or close to
////             that on VREFN, while 0xFFF indicates that the voltage on the input
////             was close to, equal to, or greater than that on VREFP.
//
//// 23:16 -     Reserved, user software should not write ones to reserved bits. The
////             value read from a reserved bit is not defined.
////
//// 26:24 CHN   These bits contain the channel from which the RESULT bits were
////             converted (e.g. 000 identifies channel 0, 001 channel 1...).
////
//// 29:27 -     Reserved, user software should not write ones to reserved bits.
////             The value read from a reserved bit is not defined.
////
//// 30 OVERRUN  This bit is 1 in burst mode if the results of one or more conversions
////             was (were) lost and overwritten before the conversion that produced
////             the result in the RESULT bits. This bit is cleared by reading this
////             register.
////
//// 31 DONE     This bit is set to 1 when an A/D conversion completes. It is cleared
////             when this register is read and when the ADCR is written. If the ADCR
////             is written while a conversion is still in progress, this bit is set
////             and a new conversion is started.
////*****************************************************************************
//
//
////*****************************************************************************
////
////*****************************************************************************
//WORD wGetADCReading( BYTE byTheMuxSelect )
//{
//    BOOL  bTimerIsExpired;
//    DWORD dwResultReg;
//    WORD  wReading;
//    DWORD dwDone;
//
//    setAnalogMuxInForSignal(  byTheMuxSelect );
//	AdcStartConversion(ADC_CHAN0_SEL);
//
//	ResetTimer( thADCWaitTimer, ADC_WAIT_INTERVAL_MS );
//    StartTimer (thADCWaitTimer);
//
//    //*************************************************************************
//	// poll to determine if the conversion is done
//    //*************************************************************************
//	do {
//        dwResultReg     = AdcReadChannel( ADC_GLOBAL_DATA );
//        dwDone          = dwResultReg & ADC_IS_DONE;
//        bTimerIsExpired = TimerExpired(thADCWaitTimer);
//
//	}while( (!dwDone) && (!bTimerIsExpired) );
//
//	if( bTimerIsExpired )
//        return 0xFFFF;
//
//	wReading = (WORD)(dwResultReg >> ADC_RESULT_LSB );
//    return wReading;
//
//}
//
////*****************************************************************************
////
////*****************************************************************************
//WORD getITEC_ThermistorReading( void )
//{
//
//    WORD wReading;
//    wReading = wGetADCReading(AM_INNER_THERMISTOR_S3);
//
//    return wReading;
//
//}
//
//
////*****************************************************************************
////
////*****************************************************************************
//WORD getLTEC_ThermistorReading( void )
//{
//
//
//    WORD wReading;
//    wReading = wGetADCReading(AM_LOCAL_THERMISTOR_S2);
//
//    return wReading;
//
//
//}
//
////*****************************************************************************
////
////*****************************************************************************
//WORD getOTEC_ThermistorReading( void )
//{
//    WORD wReading;
//    wReading = wGetADCReading(AM_OUTER_THERMISTOR_S1);
//
//    return wReading;
//}
//
//
//
//
//#define ADC_PCLK_DIVISOR 255
//
////*****************************************************************************
////
////*****************************************************************************
//void initBoardStatusMonitor( void )
//{
//
//
//
//
//}
//
//
//
//
//
//

