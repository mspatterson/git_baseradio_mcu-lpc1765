//
// Base station radio specific defines
//
#ifndef _BASE_STN_DEFS_H
#define _BASE_STN_DEFS_H

// Base station payload definitions
#include "typedefs.h"
#include "buildOpts.h"
#include "CommUsbInterface.h"
#include "CommRF1Interface.h"
#include "CommRF2Interface.h"

// Packet header
typedef struct {
    BYTE pktHdr;       // WTTTS_HDR_... define
    BYTE pktType;      // WTTTS_CMD_... or WTTTS_RESP_... define
    BYTE seqNbr;       // Incremented on each tx packet. Rolls over to zero at 255
    BYTE dataLen;      // Number of bytes of packet data to follow; can be zero
    WORD timeStamp;    // Packet time, in 10's of msec, since device power-up
} WTTTS_PKT_HDR;


// Base station commands
typedef struct {
    BYTE chanNbr;
    BYTE newChanNbr;
} BASE_STN_SET_RADIO_CHAN;

typedef struct {
    BYTE radioNbr;
    BYTE powerLevel;     // Binary value, legit values as per radio chip spec
} BASE_STN_SET_RADIO_PWR;


typedef struct {
    WORD newUpdateRate;
} BASE_STN_SET_RATE_PKT;

typedef struct {
    BYTE outputFlags;
} BASE_STN_SET_OUTPUTS_PKT;

typedef struct {
    BYTE fccModeState;       // 0 = disabled, 1 = enabled
} BASE_STN_SET_FCC_TEST_MODE;

typedef struct {
    BYTE fccModeState;       // 0 = disabled, 1 = enabled
} BASE_STN_SET_PROGRAM_MODE;


// Base station responses
typedef struct {
    BYTE  chanNbr;
    BYTE  rssi;
    BYTE  rfu[2];
    DWORD bitRate;
} RADIO_CHAN_INFO;

#define NBR_BASE_RADIO_CHANS   3

// Status packet format
typedef struct {
    RADIO_CHAN_INFO radioInfo[NBR_BASE_RADIO_CHANS];
    BYTE  lastResetType;
    BYTE  outputStatus;
    WORD  statusUpdateRate;
    BYTE  fwVer[4];
    BYTE  usbCtrlSignals;
    BYTE  byRFU[2];
} BASE_STATUS_STATUS_PKT;


// All packet payloads are defined in the following union
typedef union {
  BASE_STATUS_STATUS_PKT statusPkt;
} BASE_STN_DATA_UNION;

#define SIZEOF_WTTTS_CHECKSUM	1

#define MIN_BASE_STN_PKT_LEN      ( sizeof( WTTTS_PKT_HDR ) + SIZEOF_WTTTS_CHECKSUM )
#define MAX_BASE_STN_PKT_LEN      ( sizeof( WTTTS_PKT_HDR ) + sizeof( BASE_STN_DATA_UNION ) + SIZEOF_WTTTS_CHECKSUM )

// Packet header defines
#define BASE_STN_HDR_TX   0x99       // outbound to host
#define BASE_STN_HDR_RX   0x04       // inbound from host

// Packet type definitions: outbound to WTTTS.
#define BASE_STN_CMD_GET_STATUS      0x10
#define BASE_STN_CMD_SET_CHAN_NBR    0x12
#define BASE_STN_CMD_SET_RF_PWR      0x13
#define BASE_STN_CMD_SET_CHAN_PARAM  0x14
#define BASE_STN_CMD_SET_FCC_MODE    0x15
#define BASE_STN_CMD_SET_UPDATE_RATE 0x16
#define BASE_STN_CMD_SET_PWR_STATE   0x1A
#define BASE_STN_CMD_SET_OUTPUTS     0x1B
#define BASE_STN_CMD_RESET_FIRMWARE  0x1C
#define BASE_STN_CMD_SET_PROGRAM_MODE 0x1D
#define BASE_STN_CMD_DISABLE_PROGRAM_MODE 0x1E


//#define BASE_STN_CMD_TEST_STATUS	 0x2A	// TBD

// Packet type definitions: inbound from WTTTS
#define BASE_STN_RESP_STATUS         0xA0


#define ENTER_PM_3	0XA7

#define COMMAND		0X53
#define TX_DATA		0X29
#define RADIO_CHANGE_CHANNEL	0XA9
#define RADIO_CHANGE_POWER		0XA6
#define GET_RADIO_STATUS		0xB3
#define RADIO_RESP_TIME			5

BOOL HaveCommPacket(BYTE *pBuff, WORD buffLen);
void ExecuteCommand(void);
void SendBaseStnStatus(void);

void SetRadioInSleep(BYTE radioNbr);
void SetRadioPower(BYTE radioNbr, BYTE power);
void SetRadioChannel(BYTE radioNbr, BYTE channel);
void SendGetRadioStatCommand(BYTE radioNbr);

WORD GetStatUpdateRate(void);
BYTE CalculateCRC(BYTE *bPointer, WORD wLength);
void ParseStatusData(BYTE radioNbr,BYTE *pData,WORD wLength);

BOOL GetTestStatus(void);
void SetTestStatus(BYTE testSt);



#endif //_BASE_STN_DEFS_H
