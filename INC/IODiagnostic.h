//******************************************************************************
//
//  IODiagnostics.h:
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
// Contains the wrapper for generating an arbitrary waveform (which will usually
// be a ramp function)
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  Version 1.0     AY         Dec 7, 2012
//
//******************************************************************************

#include "hardwareAbstraction.h"





 



void DiagnosticIO( WORD wSelect );

