//******************************************************************************
//
//  SPI.h: LPC17xx SPI Peripheral Module
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module provides configuration and control of the internal SPI peripheral.
//  Currently only supports master mode.  This does not cover the SSP peripheral
//  so there is no FIFO for receive or transmit.
//
//******************************************************************************

#ifndef SPI_H_
#define SPI_H_


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "typedefs.h"


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------

// PinEnables bit defines for SpiInit()
#define SPI_SCK_EN_BIT      (1<<0)
#define SPI_MISO_EN_BIT     (1<<1)
#define SPI_MOSI_EN_BIT     (1<<2)
#define SPI_SSEL_EN_BIT     (1<<3)

// CtrlRegValue defines for SpiInit()
#define SPI_WORD_LEN_8      0x00000804
#define SPI_WORD_LEN_9      0x00000904
#define SPI_WORD_LEN_10     0x00000a04
#define SPI_WORD_LEN_11     0x00000b04
#define SPI_WORD_LEN_12     0x00000c04
#define SPI_WORD_LEN_13     0x00000d04
#define SPI_WORD_LEN_14     0x00000e04
#define SPI_WORD_LEN_15     0x00000f04
#define SPI_WORD_LEN_16     0x00000004
#define SPI_CLK_PHASE       (1<<3)
#define SPI_CLK_POL         (1<<4)
#define SPI_MSTR_MODE       (1<<5)
#define SPI_LSB_FIRST       (1<<6)
#define SPI_IRQ_EN          (1<<7)


//------------------------------------------------------------------------------
//  TYPEDEF DECLARATIONS
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS PROTOTYPES
//------------------------------------------------------------------------------


//******************************************************************************
//
//  Function: SpiInit
//
//  Arguments: CtrlRegValue - value to be written to SPI ctrl reg
//                          b2 set to select word lengths other than 8 bits
//                          b3 clock phase ctrl
//                          b4 clock polarity ctrl
//                          b5 set to one to select master mode
//                          b6 0=MSb first, 1=LSb first
//                          b7 set to enable interrupt generation
//                          b11:8 word length select, 8-16 bits (0b1000=8, 0b1111=15, 0b0000=16)
//             SckFreq - desired SCK frequency in Hz
//                       (max is pclk/8, min is pclk/255, by default pclk = sysclk/4)
//             PinEnables - enables for each SPI pin (normally SCLK will always
//                          be enabled, but depending on usage others may not)
//                          b0 set to enable SCLK pin (SPI_SCK_EN_BIT)
//                          b1 set to enable MISO pin (SPI_MISO_EN_BIT)
//                          b2 set to enable MOSI pin (SPI_MOSI_EN_BIT)
//                          b3 set to enable SSEL pin (SPI_SSEL_EN_BIT)
//             IntPriority - SPI interrupt priority, 0 = highest, 31 = lowest
//             IsrCallBack - function pointer to SPI ISR callback function (use NULL for none)
//                           (newly received data is passed to this function)
//
//  Returns: TRUE if initialization successful, else FALSE
//
//  Description: This function will initialize the SPI port according to the
//               passed parameters.  Note that if SSEL can be left disabled if
//               not used, or if there are multiple slaves on bus.  In that case
//               the calling function will need to manually control the GPIO
//               driving each SSEL.
//
//******************************************************************************
BOOL SpiInit( DWORD CtrlRegValue, DWORD SckFreq, BYTE PinEnables, BYTE IntPriority, void (*IsrCallBack)( WORD SpiData ) );


//******************************************************************************
//
//  Function: SpiWriteData
//
//  Arguments: SpiData - data to be written to SPI data register
//
//  Returns: void
//
//  Description: This function will write to the SPI data register, resulting in
//               the data being transmitted on the SPI bus.  This function must
//               not be called during an active SPI transfer, or the current
//               transfer will be corrupted.
//
//******************************************************************************
void SpiWriteData( WORD SpiData );


//******************************************************************************
//
//  Function: SpiTransferDone
//
//  Arguments: none
//
//  Returns: TRUE if current SPI transfer is complete, else FALSE
//
//  Description: This function will check the transfer complete status of the
//               SPI bus.  SpiReadData or SpiWriteData must be called in order
//               to clear the transfer done status.
//
//******************************************************************************
BOOL SpiTransferDone( void );


//******************************************************************************
//
//  Function: SpiReadData
//
//  Arguments: none
//
//  Returns: Data read from SPI data register
//
//  Description: This function will read the current data in the SPI data register.
//               This will be the SPI receive data if a SPI transfer is complete.
//               This function should not be called during an active SPI transfer.
//
//******************************************************************************
WORD SpiReadData( void );


#endif /* SPI_H_ */
