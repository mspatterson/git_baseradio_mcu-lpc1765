//******************************************************************************
//
//  UartManager.h:
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  Provides a high level wrapper for the low-level hardware module 'Uart.c';
//  the manager provides software fifos for the Uart and implements a basic
//  interrupt handler
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-02-01      JK     Initial Implementation
//
//******************************************************************************


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

#ifndef _UART_MGR_H
#define _UART_MGR_H

#include "buildOpts.h"
#include "uartQueue.h"
#include "uart.h"


//******************************************************************************
// 
//******************************************************************************

typedef struct
UartMangerObject
{
	UART_Q   UartRxQueue;
	UART_Q*  pUartRxQueue;

    void (*UartRxDataCallBack)( BOOL bIntWasTimeOut );
    void (*UartTxDataCallBack)( void );

    const BYTE* ptrTxQ;
    WORD  wTxQIndex;
    WORD  wTxQSize;
    BOOL  bTxQSending;
    BYTE  byPortNum;

}UART_MGR_OBJECT;

typedef struct
{
   DWORD dwSpeed;
   WORD  wDataBits;
   WORD  wParity;
   WORD  wStopBits;
   WORD  wFlowCntl;
 } SERIAL_CFG;

 typedef enum
{
   PARITY_NONE,
   PARITY_EVEN,
   PARITY_ODD,
   PARITY_MARK,
   PARITY_SPACE
} PARITY_TYPES;

typedef enum
{
   ONE_STOP_BIT,
   ONE_AND_HALF_BITS,
   TWO_STOP_BITS
} STOP_BITS_TYPES;

typedef enum
{
   NO_FLOW_CNTL,
   XON_XOFF_FLOW_CNTL,
   RTS_CTS_FLOW_CNTL
} FLOW_CNTL_TYPES;


//******************************************************************************
//
//  Function: InitUartManager
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: This function must be the first to be called from 
//               this module to initialize static variables for all the modem
//               ports under. Here is what the caller must do:
//
//               1) declare a 'UART_MGR_OBJECT' object in memory - and pass the
//                  pointer in as a paramter to this function call
//               2) setup two 'wrapper' calls to the UartRxISR and UartTxISR
//                  handlers - and pass these in as parameters.
//
//               The reason for '2' above is as follows: we must register a
//               function call that takes no parameters with the interrupt
//               controller; but, the low level handlers in this routine
//               (UartRxISR and UartTxISR) are the interrupt handler for all the
//               uart ports - and they need to know what buffer-queue that they
//               can post data to. Therefore, the most straightforward option
//               is to force the caller to wrap these low level handlers (which
//               take the uart object as a pointer) in a 'void' function which
//               passes the uart object pointer to the ISR handler - then register
//               this 'wrapper' call with the interrupt controller.
//
//******************************************************************************
BOOL InitUartManager( UART_MGR_OBJECT* ptrUartManObj );



//******************************************************************************
//
//  Function: OpenUartSerialPort
//
//  Arguments:
//    cfgInfo   - A SERIAL_CFG data structure containing specific
//                information requiring the configuration
//                of the Uart port.
//
//  Returns: TRUE on success.
//           FALSE otherwise.
//
//  Description: This function configures the wireless Uart port based on
//               the passed SERIAL_CFG parameter.
//
//******************************************************************************
BOOL OpenUartSerialPort( UART_MGR_OBJECT* ptrUartManObj, SERIAL_CFG* cfgInfo );


//******************************************************************************
//
//  Function: GetUartSerialPortSettings
//
//  Arguments:
//    OUT cfgInfo - Pointer to a SERIAL_CFG data structure containing specific
//                  information requiring the configuration
//                  of the Uart port.
//
//  Returns: void.
//
//  Description: This function gets the configuration the Uart port and
//               populates the passed cfgInfo structure.
//
//******************************************************************************
void GetUartSerialPortSettings( SERIAL_CFG* cfgInfo );


//******************************************************************************
//
//  Function: FlushUartSerialRxQueue
//            FlushUartSerialTxQueue
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: Call this function to flush the respective data queue.
//
//******************************************************************************
void FlushUartSerialRxQueue( UART_MGR_OBJECT* ptrUartManObj );
void FlushUartSerialTxQueue( UART_MGR_OBJECT* ptrUartManObj );


//******************************************************************************
//
//  Function: GetUartPortChar
//
//  Arguments:
//    byData    - data received from the Uart's serial interface.
//
//  Returns: TRUE if data was pending - byte returned in byData
//           FALSE otherwise.
//
//  Description: Call this function to get a byte from the hardware's 
//               receiving FIFO and add it to a queue.  
//
//******************************************************************************
BOOL GetUartPortChar( UART_MGR_OBJECT* ptrUartManObj, BYTE* byData );


//******************************************************************************
//
//  Function: GetUartPortLSR
//
//  Arguments:
//    byPort - port number to get LSR for
//
//  Returns: DWORD value representing current LSR value
//
//  Description: This function gets a port's LSR
//
//******************************************************************************
DWORD GetUartPortLSR( BYTE portNbr );


//******************************************************************************
//
//  Function: UartRxIsr
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: This is the Uart Recieve ISR function. It gets called when an
//               RX interrupt is pending.
//
//******************************************************************************
void UartRxISR( UART_MGR_OBJECT* ptrUartManObj );


//******************************************************************************
//
//  Function: UartTxISR
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: This is the Uart Transmit ISR function. It gets called when an
//               TX interrupt is pending.
//
//******************************************************************************
void UartTxISR( UART_MGR_OBJECT* ptrUartManObj );


//******************************************************************************
//
//  Function: UartPortSendByte
//
//  Arguments:
//    portNbr   - UART port number
//    byData    - Byte to send
//
//  Returns: TRUE on success, false if the transmitter was busy or the port
//           was invalid
//
//  Description: Call this function to write a single byte directly
//               to the UART's transmit register
//
//******************************************************************************
BOOL UartPortSendByte( BYTE portNbr, BYTE byData );


//******************************************************************************
//
//  Function: UartPortSendBuffer
//
//  Arguments:
//    pBuffer   - An array of BYTE data to be shifted 
//                out to the transmitting port.
//    wLength   - Specifies the length of the pBuffer array.
//
//  Returns: void.
//
//  Description: Call this function to write the passed byte onto the 
//               hardware's transmitting FIFO. The function only writes to
//               the FIFO when hardware's transmitting port is ready for 
//               shifting data out.
//
//******************************************************************************
BOOL UartPortSendBuffer( UART_MGR_OBJECT* ptrUartManObj, const BYTE* pBuffer, WORD wLength );


//******************************************************************************
//
//  Function: uartTxQueueSending
//
//  Arguments: Uart Manager Object
//
//  Returns: Returns 'True' if the Tx Queue is being transmitted; returns false
//           otherwise
//
//  Description:
//
//******************************************************************************
BOOL UartTxQueueSending( UART_MGR_OBJECT* ptrUartManObj );


#endif  // _UART_MGR_H
