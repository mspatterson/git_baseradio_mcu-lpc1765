//******************************************************************************
//
//  CommRF1Interface.h:
//
//      Copyright (c) 2013, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//  Version 1.1     DD      Feb. 27, 2013
//
//******************************************************************************

#ifndef COMM_RF2_H
#define COMM_RF2_H

#include "typedefs.h"


void InitCommRF2( void );
BOOL WriteToRF2Port( const BYTE* pBuff, WORD buffLen );
BOOL RF2PortSending( void );
void clearRF2RxBuff(void);
void UpdateRF2ComRxBuf( void );
BOOL IsRF2RxPacketReady(void);
WORD ReadRF2RxPacket(BYTE *pByteData, WORD bufSize);

#endif //COMM_RF1_H
