//******************************************************************************
//
//  UART.h: LPC UART Access and control module
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  Provides setup and control of the LPC UART serial ports.  Currently the
//  following features are not supported - DMA, Autobaud, IrDA and Baud Rate
//  divisor calculations using the fractional divider register.
//
//******************************************************************************

#ifndef UART_H_
#define UART_H_


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "typedefs.h"
#include "clocksAndDivisors.h"


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------

#define NUM_OF_LPC_UARTS        5

// line status register mask defines
#define UART_RX_DATA_RDY        (1<<0)
#define UART_OVERRUN_ERROR      (1<<1)
#define UART_PARITY_ERROR       (1<<2)
#define UART_FRAMING_ERROR      (1<<3)
#define UART_BREAK_STATUS       (1<<4)
#define UART_TX_HOLD_REG_EMPTY  (1<<5)
#define UART_TX_IDLE            (1<<6)
#define UART_RX_ERROR_IN_FIFO   (1<<7)

#define UART_SYM_LEN_8_BITS 0x03
#define UART_PARITY_NONE    0x00
#define UART_PARITY_EVEN    0x18
#define UART_PARITY_ODD     0x08

#define ENABLE_RX_INT 0x00000001
#define ENABLE_TX_INT 0x00000002


//------------------------------------------------------------------------------
//  TYPEDEF DECLARATIONS
//------------------------------------------------------------------------------

// Initialization data structure
typedef struct
{
    // Desired/actual Baud Rate - if this is set to a non-zero value it will be
    // used to calculate the divisor latch value based on the system clk frequency.
    // If this is set to zero when passed to initialization function, the divider
    // latch value will be written directly from the init data structure.  After
    // initialization is completed, this will contain the actual integer Baud Rate.
    DWORD BaudRate;
    
    // Divisor Latch Value - if BaudRate is 0, this value will be written directly
    // to the Divisor Latch registers.  If the divisor value is calculated based on
    // BaudRate, this will contain the calculated divisor latch value on return from
    // initialization function.
    //      Baud Rate = pclk/(divisor latch value * 16),  default pclk = sysclk/4
    //          assumes fractional divider is not used
    WORD DivisorLatchValue;

    // Interrupt Enable Register value
        // b0 - Rx data available/timeout interrupt enable
        // b1 - Tx holding register empty interrupt enable
        // b2 - Line status interrupt enable
        // b3 - Modem status interrupt enable (UART1 only)
        // b7 - CTS interrupt enable (UART1 only)
        // b8 - end of autobaud interrupt enable
        // b9 - autobaud timeout interrupt enable
    DWORD IrqEnableRegValue;
    
    // UART interrupt priority, 0 = highest, 31 = lowest
    BYTE IntPriority;
    
    // Line status interrupt callback function, set to NULL if not required
    // Interrupt will already be cleared, Line status register value is passed
    // to the callback function.
    void (*LineStatusCallBack)( BYTE LineStatusRegValue );
    
    // Rx Data interrupt callback function, set to NULL if not required
    // Rx data must be read in callback function to clear the interrupt.  FifoTimeout
    // will be TRUE if interrupt was generated due to a Rx timeout (data is in Rx FIFO
    // but interrupt threshold has not been reached).
    void (*RxDataCallBack)( BOOL FifoTimeout );
    
    // Tx Data interrupt callback function, set to NULL if not required
    // Interrupt generated when Tx holding register is empty (no data left in FIFO)
    // Interrupt should already be cleared.  Callback can write new data if required
    // or do nothing if transmission is complete.
    void (*TxDataCallBack)( void );
    
    // Modem Status interrupt callback function (UART1 only), set to NULL if not required
    // See LPC Users Manual for more info
    void (*ModemStatusCallBack)( BYTE ModemStatusRegValue );
    
    // FIFO Control Register value
        // b0 - FIFO enable, set to enable FIFOs for Rx and Tx
        // b1 - Rx FIFO reset, FIFO will always be reset on initialization
        // b2 - Tx FIFO reset, FIFO will always be reset on initialization
        // b3 - DMA mode select
        // b7:6 - Rx FIFO trigger level, 00= 1 byte, 01= 4 bytes, 10= 8 bytes, 11= 14 bytes
    BYTE FifoCtrlRegValue;
    
    // Line Control Register value
        // b1:0 - word length select, 00= 5 bits, 01= 6 bits, 10= 7 bits, 11= 8 bits
        // b2 - stop bit select, 0= 1 stop bit, 1= 2 stop bits
        // b3 - parity enable
        // b5:4 - parity select, 00= odd, 01= even, 10= force 1, 11= force 0
        // b6 - break control
        // b7 - divisor latch access control (will be ignore, handled internally)
    BYTE LineCtrlRegValue;
    
    // Fractional Divider Register value - can be used to reduce baud rate error.
    // The fractional divder is currently not supported for calculated divider
    // latch values, so if the fractional dividor is to be used the divider latch
    // value must also be written manually.  See LPC user manual for info.
    BYTE FractionalDividerRegValue;
    
    // Modem Control Register value (UART1 only, see user manual)
    BYTE ModemCtrlRegValue;

    // RS485 Control Register value (UART1 only)
    BYTE RS485CtrlRegValue;
    
    // RS485 Address Match Register value (UART1 only)
    BYTE RS485AddrMatchRegValue;
    
    // RS485 Delay Register value (UART1 only)
    BYTE RS485DelayRegValue;
    
} LPC_UART_INIT_DATA;


//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS PROTOTYPES
//------------------------------------------------------------------------------


//******************************************************************************
//
//  Function: UartInit
//
//  Arguments: UartNum - UART number (0-3) to initialize
//             UartInitData - pointer to structure containing initialization data
//
//  Returns: TRUE if successful, else FALSE
//
//  Description: This function will configure and enable the selected UART.
//               Interrupt will only be registered with NVIC if one or more
//               interrupt sources are enabled in the interrupt enable register.
//               NOTE: this function does not enable any of the modem pins.
//               Verify that the desired Rx/Tx pins are selected in macros.
//
//******************************************************************************
BOOL UartInit( BYTE UartNum, LPC_UART_INIT_DATA *UartInitData );


//******************************************************************************
//
//  Function: UartResetFifos
//
//  Arguments: UartNum - UART number (0-3) selected
//             RstRxFifo - if TRUE, Rx fifo will be reset
//             RstTxFifo - if TRUE, Tx fifo will be reset
//
//  Returns: void
//
//  Description: This function will reset the Rx and/or Tx FIFOs as requested.
//
//******************************************************************************
void UartResetFifos( BYTE UartNum, BOOL RstRxFifo, BOOL RstTxFifo );


//******************************************************************************
//
//  Function: UartLineStatus
//
//  Arguments: UartNum - UART number (0-3) selected
//
//  Returns: Line status register value for selected UART
//
//  Description: This function will read the line status register value for the
//               selected UART.  Status bit mask macros are defined in UART.h.
//
//******************************************************************************
BYTE UartLineStatus( BYTE UartNum );


//******************************************************************************
//
//  Function: UartReadByte
//
//  Arguments: UartNum - UART number (0-3) selected
//
//  Returns: Byte from top of receive FIFO
//
//  Description: This function will read a byte from the receive FIFO.  Calling
//               function should only call this function if there is data in the
//               FIFO (Rx interrupt occurred or line status register checked).
//
//******************************************************************************
BYTE UartReadByte( BYTE UartNum );


//******************************************************************************
//
//  Function: UartWriteByte
//
//  Arguments: UartNum - UART number (0-3) selected
//             Data - byte to write to transmit FIFO
//
//  Returns: void
//
//  Description: This function writes one byte to the transmit FIFO.  Calling
//               function is responsible to track how much data can be written
//               to the transmit FIFO.
//
//******************************************************************************
void UartWriteByte( BYTE UartNum, BYTE Data );


//******************************************************************************
//
//  Function: UartTransmitEnable
//
//  Arguments: UartNum - UART number (0-3) selected
//
//  Returns: void
//
//  Description: Calling this function will re-enable the transmitter of the
//               selected UART if it was previously disable.
//
//******************************************************************************
void UartTransmitEnable( BYTE UartNum );


//******************************************************************************
//
//  Function: UartTransmitDisable
//
//  Arguments: UartNum - UART number (0-3) selected
//
//  Returns: void
//
//  Description: Calling this function will disable the transmitter of the selected
//               UART after the current character transmission is complete.
//
//******************************************************************************
void UartTransmitDisable( BYTE UartNum );


//******************************************************************************
//
//  Function: UartIsTransmitEnabled
//
//  Arguments: UartNum - UART number (0-3) selected
//
//  Returns: void
//
//  Description: Calling this function returns status of whether the transmit
//               is enabled or disabled
//
//******************************************************************************
BOOL UartIsTransmitEnabled( BYTE UartNum );


//******************************************************************************
//
//  Function: UartModemStatus
//
//  Arguments: UartNum - UART number selected (only supported for UART1)
//
//  Returns: byte containing modem status register value
//
//  Description: This function will read the modem status register.  This is
//               only valid for UART1.
//
//******************************************************************************
BYTE UartModemStatus( BYTE UartNum );


//******************************************************************************
//
//  Function: UartDtrCtrl
//
//  Arguments: UartNum - UART number selected (only supported for UART1)
//             DtrEnable - DTR is set active if TRUE, else inactive
//
//  Returns: void
//
//  Description: This function allows control of the DTR modem signal output.
//               This is only supported for UART1.
//
//******************************************************************************
void UartDtrCtrl( BYTE UartNum, BOOL DtrEnable );


//******************************************************************************
//
//  Function: UartRtsCtrl
//
//  Arguments: UartNum - UART number selected (only supported for UART1)
//             RtsEnable - RTS is set active if TRUE, else inactive
//
//  Returns: void
//
//  Description: This function allows control of the RTS modem signal output.
//               This is only supported for UART1.  Will also have no effect
//               if auto-RTS flow control is enabled.
//
//******************************************************************************
void UartRtsCtrl( BYTE UartNum, BOOL RtsEnable );


#endif /* UART_H_ */
