//******************************************************************************
//
//  buildOpts.h: 
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//  Version 1.0     JK      July 6, 2012
//
//******************************************************************************

#include "typedefs.h"


//#define BUILD_VERSION 0v00
#define MAJOR_VER	0
#define MINOR_VER	0
#define SUB_MINOR_VER	0
#define BUILD_VER	9

//******************************************************************************
// OTEC
//******************************************************************************
#define DEFAULT_OTEC_SP_VLIM       3500
#define DEFAULT_OTEC_SP_ILIMH      2500
#define DEFAULT_OTEC_SP_ILIMC      1500
#define DEFAULT_OTEC_SP_COLD_LIMIT  298
#define DEFAULT_OTEC_SP_HOT_LIMIT   308

//******************************************************************************
// LTEC Limits
//******************************************************************************
#define DEFAULT_LTEC_SP_VLIM      3500
#define DEFAULT_LTEC_SP_ILIMH     1250
#define DEFAULT_LTEC_SP_ILIMC     1250
#define DEFAULT_LTEC_SP_IN2P       288


//******************************************************************************
// ITEC Limits
//******************************************************************************
#define DEFAULT_ITEC_SP_VLIM     2000
#define DEFAULT_ITEC_SP_ILIMH     900
#define DEFAULT_ITEC_SP_ILIMC     900
#define DEFAULT_ITEC_SP_IN2P      288


//******************************************************************************
// Ramp Defaults
//******************************************************************************
#define RAMP_SAMPLE_COUNT_DEFAULT 20
#define RAMP_PERIOD_DEFAULT       200
//******************************************************************************
// Ramp DC Default and Peak-to-Peak are expressed as fraction of 16-bits/2.5V
// Reference:e.g., for value=1310, dac output voltage = (1310/65535)*2.5
//******************************************************************************
// #define RAMP_DC_DEFAULT         (1310)
// #define RAMP_PEAK2PEAK_DEFAULT  (2621)
#define RAMP_DC_DEFAULT         (50)
#define RAMP_PEAK2PEAK_DEFAULT  (100)



//******************************************************************************
// Modulator Defaults
//******************************************************************************
// the modulator DC offset expressed as fraction of 16-bits/2.5V Reference
// Reference:e.g., for value=2254, dac output voltage = (2254/65535)*2.5
#define MOD_DC_OFFSET_DEFAULT    86

// below: the PGA2311 gain expressed in DB
#define MOD_DDS_GAIN_DEFAULT       -6

//******************************************************************************
// produces 80KHz from 1MHz input clock:
// #define MOD_PERIOD_LSB_DEFAULT 11796
// #define MOD_PERIOD_MSB_DEFAULT 1310
//******************************************************************************
// produces 80KHz from 4MHz input clock:
#define MOD_PERIOD_LSB_DEFAULT 11141
#define MOD_PERIOD_MSB_DEFAULT 327









//******************************************************************************
// Queue Sizes
//******************************************************************************
#define UART_Q_SIZE 128
#define BYTE_Q_SIZE 128

//******************************************************************************
// UART Port Allocations
//******************************************************************************
#define UART_USB_PN	1
#define UART_RF1_PN	2
#define UART_RF2_PN	3

#define UART_DEBUG_PN 1
#define CMD_COMMS_PN  0

#ifdef TO_COMPLETE
    // note: need to fix the cmd comms baud rate port fudge below:
#endif

#define CMD_COMMS_BAUD_RATE 19200;

//******************************************************************************
// SSP Port Allocations
//
// Note: Some SSP ports are shared; this is achieved by generating a dedicated
//       /CS signal to each target which goes active only for that target,
//       while SCLK and SDATA are shared
//******************************************************************************
#define ARB_WAVE_RAMP_SSP_PORT 0

#define SSP1_PORTNUM 1
#define RAMP_GAIN_SSP_PORT     SSP1_PORTNUM
#define MODULATOR_DDS_SSP_PORT SSP1_PORTNUM



//******************************************************************************
// Timer Allocations
//******************************************************************************
#define LOCAL_RAMP_SAMPLE_TIMER 3



//******************************************************************************
//
//******************************************************************************
#define MCPWM_IRQ_PRIORITY 0



//******************************************************************************
// Debug Options / Enable Options
//******************************************************************************
// #define EMULATE_MCLK_WITH_LOCAL_PCLK

#define ENABLE_DDS                          0
#define ENABLE_LOGGING                      0
#define ENABLE_CMD_COMMS                    0
#define ENABLE_ARB_RAMP                     0
#define ENABLE_RAMP_SAMPLE_INTERRUPT        1
#define GENERATE_RAMP_SAMPLE_TIMING_LOCALLY 0

#define ENFORCE_DEFAULT_PARAM_OVERRIDE      1


//******************************************************************************
// System Timing Parameters
//*****************************************************************************
#define SSP1_FREQUENCY         60000
#define RAMP_SSP_FREQUENCY   10000000


//******************************************************************************
//
//******************************************************************************
#ifdef TO_FINISH
move this to appropriate h file when testing done
#endif
void processArbWaveRampSampleInt2( void );

// TO_COMPLETE



