 /*****************************************************************************
 *
 * Project          :
 * Subproject       : 
 * Name             : flasher.h
 * Function         : for burning the LPC2378 internal flash
 * Designer         : J. Kniss
 * Creation date    : 29/05/2007
 * Compiler         : GNU ARM
 * Processor        : LPC2378
 * Version          :
 *
 *****************************************************************************
 *
 ****************************************************************************/
//*****************************************************************************
// *) The boot block is present at addresses 0x0007 E000 to 0x0007 FFFF 
//    in all devices; this is contained in sector #7 (4K) (from '0' based index)
//    therefore, we should disallow writes to sector 7
//
// *) For the LPC2378 sector '8' (32K) is not accessible
//
// *) the sectors count up from 0-26, with those 'offlimits' as noted above
//*****************************************************************************


#ifndef FLASHER_H
#define FLASHER_H

#include "typedefs.h"
#include <stdio.h>
// #include <target.h>



//*****************************************************************************
//
//*****************************************************************************
#define USER_DATA_SECTOR1 19
#define USER_DATA_SECTOR2 20
#define USER_DATA_SECTOR3 21


#define USER_DATA_SECT1_ADDR 0x00028000
#define USER_DATA_SECT2_ADDR 0x00030000
#define USER_DATA_SECT3_ADDR 0x00038000


//*****************************************************************************
// Print IAP error message.
//*****************************************************************************
void iap_err(char *s);


//*****************************************************************************
// Read part ID.
//*****************************************************************************
void part_id(void);

//*****************************************************************************
// Read boot code version.
//*****************************************************************************
void version(void);

//*****************************************************************************
// Write sector.
//*****************************************************************************
BOOL s_write(unsigned char* ptrBuf, int iBuffSize, BYTE bySector);


//******************************************************************************
// Erase sector.
//******************************************************************************
BOOL s_erase(BYTE bySect);


//******************************************************************************
// Run Diagnostic Test
//******************************************************************************
BOOL doIAPFlashDiagnostic( void );


//******************************************************************************
//
//******************************************************************************
BOOL writeUserFlashSpace( BYTE* ptrTheBUffer, int iBufferSize, BYTE theSector );



#endif




