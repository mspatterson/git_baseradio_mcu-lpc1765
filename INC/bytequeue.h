//******************************************************************************
//
//  ByteQueue.h
//
//      Copyright (c) 2006, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module implements a generic queue for bytes
//
//  Date            Author      Comment
//  ---------------------------------------------------------------------------
//  Mar. 3, 2007    JK          Version 1v1
//******************************************************************************



#ifndef _BYTE_QUEUE_H
    #define _BYTE_QUEUE_H  1
    
    #include "typedefs.h"
    #include "buildOpts.h"

    





    typedef struct {
                    WORD wReadIndex;
                    WORD wWriteIndex;
                    BYTE theData[BYTE_Q_SIZE + 1];               // extra space for Q handling overhead
                    BYTE boolQOverFlow;
                    BYTE boolQOverFlowPending;
    } BYTE_Q;

    void initByteQ( BYTE_Q* theQ );
    BYTE Add2ByteQ( BYTE newData, BYTE_Q* theQ );
    BYTE blockWriteToByteQ( BYTE_Q* theQ, BYTE* ptrByteBuffer, WORD wAmountToAdd );
    BYTE ReadByteQ( BYTE_Q* theQ );
    BYTE PeekAtByteQ( BYTE_Q* theQ );
    BYTE ByteQHasData( BYTE_Q* theQ );
    BYTE GetReadIndexByteQ( BYTE_Q* theQ );
    void ResetReadIndexByteQ(BYTE byNewReadIndex,BYTE_Q* theQ );
    BYTE getByteQOverFlowStatus( BYTE_Q* theQ );






#endif
