//******************************************************************************
//
//  HardwareAbstraction.h: 
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//  Version 1.0     JK      Jan 2012
//
//******************************************************************************

#ifndef HARDWARE_ABSTRACTION_H
#define HARDWARE_ABSTRACTION_H

#include "target.h"
#include "gpio_cmsis_17xx.h"
#include "system_LPC17xx.h"
#include "clocksAndDivisors.h"
#include "LPC17xx.h"


//#define		Power_G_LED			1,0
//#define		Power_R_LED			1,1
//#define		Processor_G_LED		1,4
//#define		Processor_R_LED		1,8
//#define		RF1_G_LED			1,9
//#define		RF1_R_LED			1,10
//#define		RF2_G_LED			1,14
//#define		RF2_R_LED			1,15

// there is a mistake on the schematic 1V0.
// actually the G_LED controls the RED and the R_LED controls the GREEN
// the following defines reflect the actual LED control i.e. G_LED control the GREEN, and R_LED controls the RED
// the active state is '0' -> LED ON


#define 	VERSION_IN0			1,9
#define 	VERSION_IN1			1,10
#define 	VERSION_IN2			1,14
#define 	VERSION_IN3			1,15
#define 	PROG_DD_1			0,6
#define 	PROG_DC_1			0,7
#define 	PROG_DD_2			0,8
#define 	PROG_DC_2			0,9

#define		ZC_UART_CTS			2,5
#define		ZC_UART_RI			2,8
#define 	Contact_Closure4	2,9
#define		ZC_UART_TX			0,16
#define		ZC_UART_RX			0,15
#define		ZC_UART_RTS			0,17
#define 	Contact_Closure3	0,18
#define 	Contact_Closure2	0,19
#define 	Contact_Closure1	0,20
#define		LED4				0,21
#define		LED3				0,22
#define 	RESET_N1			2,11
#define		ZB_UART_CTS1		2,12
#define 	ZB_UART_RTS1		2,13
#define		ZB_UART_RX_1		0,11
#define		ZB_UART_TX_1		0,10
#define		ZB_UART_RX_2		0,1
#define		ZB_UART_TX_2		0,0
#define		ZB_UART_CTS2		1,29
#define 	ZB_UART_RTS2		1,28
#define 	RESET_N2			1,27
#define		USB_CONN			1,25
#define		SUSPEND_PC			1,24
#define		LED2				1,23
#define		LED1				1,22
#define		RF2LED				1,21
#define		RF1LED				1,20
#define		RF_LINK_2			1,19
#define 	RF_LINK_1			1,18
#define 	USB_PROG_DISABLE	0,24
#define		USB_PROG_EN			0,25
#define		PROG_WDOG			0,26
#define 	ZC_UART_TX_2		0,3
#define 	ZC_UART_RX_2		0,2



//#define		RF1_G_LED			1,10
//#define		RF1_R_LED			1,9
//#define		RF2_G_LED			1,15
//#define		RF2_R_LED			1,14
//#define		Contact_Closure1	1,20
//#define		Contact_Closure2	1,21
//#define		Contact_Closure3	1,22
//#define		Contact_Closure4	1,23
//#define		RF_LINK_1			1,18
//#define		RF_LINK_2			1,19
//#define		SUSPEND_PC			1,24
//#define		USB_CONN			1,25
//#define		ZB_UART_TX_2		0,0
//#define		ZB_UART_RX_2		0,1
//#define		ZB_UART_TX_1		0,10
//#define		ZB_UART_RX_1		0,11
//#define		ZB_UART_CTS_2		2,13
//#define		ZB_UART_RTS_2		2,12
//#define		ZB_UART_CTS_1		2,11
//#define		ZB_UART_RTS_1		2,10
//#define		ZC_UART_RTS			0,17
//#define		ZC_UART_RX			0,15
//#define		ZC_UART_TX			0,16
//#define		ZC_UART_CTS			2,5
//#define		MSP_TX				0,2
//#define		MSP_RX				0,3







//******************************************************************************
//
//  Function: EnableInts
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: This function enables the system interrupts using inline
//               Assembly ocde.
//
//******************************************************************************
void EnableInts( void );


//******************************************************************************
//
//  Function: DisableInts
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: This function disables the system interrupts using
//               inline Assembly code.
//
//******************************************************************************
void DisableInts( void );


//******************************************************************************
// UART Port Designations
//******************************************************************************
// #define UART_DEBUG_PN     0
// #define UART_IRIDIUM_PN   1
// #define UART_WIFI_PN      2
// #define UART_BTOOH_PN     3
// #define UART_GPS          4


//******************************************************************************
// Overall Processor Peripheral/GPIO Mapping
//******************************************************************************
void setGPIODir( void );


void resetExtWD(void);

//******************************************************************************
//
//******************************************************************************
void inlineDelay(DWORD dwNOPCount );

//******************************************************************************
//
//******************************************************************************
WORD divByUsingAddition( DWORD dwNumerator, WORD wDenominator );


//******************************************************************************
//
//******************************************************************************
WORD convertVoltsTo16BitDacOn2500milliVoltReference( WORD wMilliVoltsOut );


BYTE GetContactStatus(void);
void SetContactClosure(BYTE bOutputs);
void ContactTimeoutTask(void);
void InitContactClosuresTimers(void);
BOOL ComPortClosed(void);

void KickWatchDog( void );
void InitWDT(void);
BYTE GetLastReset(void);
void GetResetReason( void );


#endif
