//******************************************************************************
//
//  ADC.h: LPC17xx ADC module
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module provides access and control of the LPC17xx on board A/D converter.
//
//******************************************************************************

#ifndef ADC_H_
#define ADC_H_


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "typedefs.h"


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------

#define NUM_ADC_CHANNELS        8

#define ADC_GLOBAL_DATA         0xff

#define ADC_CHANNEL_0           (1<<0)
#define ADC_CHANNEL_1           (1<<1)
#define ADC_CHANNEL_2           (1<<2)
#define ADC_CHANNEL_3           (1<<3)
#define ADC_CHANNEL_4           (1<<4)
#define ADC_CHANNEL_5           (1<<5)
#define ADC_CHANNEL_6           (1<<6)
#define ADC_CHANNEL_7           (1<<7)
#define ADC_BURST_MODE          (1<<16)
#define ADC_ENABLE              (1<<21)
#define ADC_START_NOW           (1<<24)
#define ADC_FALLING_EDGE        (1<<27)

#define ADC_IS_DONE             (1<<31)
#define ADC_RESULT_LSB          4


//------------------------------------------------------------------------------
//  TYPEDEF DECLARATIONS
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS PROTOTYPES
//------------------------------------------------------------------------------

//******************************************************************************
//
//  Function: AdcInit
//
//  Arguments: CtrlRegVal - value to be programmed to ADCR
//                          b7:0 channel select bits
//                          b15:8 pclk divider (this value + 1)
//                                a single conversion will take 65 of the divided clks
//                          b16 set to enable burst mode (continuous conversions)
//                          b21 must be set to enable ADC
//                          b26:24 start mode select (if not burst mode)
//                          b27 start edge select
//             IntEnRegVal - Interrupt enable register value
//                           b7:0 channel convert done interrupt enables
//                           b8 global convert done interrupt enable
//             IntPriority - ADC interrupt priority, 0 = highest, 31 = lowest
//             AdcCallBack - function pointer for ADC ISR callback function
//
//  Returns: TRUE if initialization successful, else FALSE
//
//  Description: Call this function to initialise the ADC module.  Any channels
//               selected in channel select bits will have input pins configured.
//
//******************************************************************************
BOOL AdcInit( DWORD CtrlRegVal, DWORD IntEnRegVal, BYTE IntPriority, void (*AdcCallBack)( void ) );


//******************************************************************************
//
//  Function: AdcStartConversion
//
//  Arguments: ChnlSelect - channel to perform conversion (0 to 7)
//
//  Returns: TRUE if successful, else FALSE
//
//  Description: This function will perform a manual start conversion on the
//               selected ADC channel.  Note the selected channel must have been
//               enabled in AdcInit and the ADC must not be in burst mode.
//
//******************************************************************************
BOOL AdcStartConversion( BYTE ChnlSelect );


//******************************************************************************
//
//  Function: AdcReadChannel
//
//  Arguments: ChnlSelect - channel to read (0 to 7, or ADC_GLOBAL_DATA)
//
//  Returns: ADC conversion value in format:
//                  b15:4   - 12 bit ADC conversion value
//                  b26:24  - channel number (global data only)
//                  b30     - Set if overrun occurred (a conversion value was overwitten)
//                  b31     - Conversion done, set when new conversion value is available
//
//  Description: This function will read the last ADC conversion value for the
//               selected ADC channel or if ADC_GLOBAL_DATA is select, the ADC
//               conversion value from the most recent channel conversion.  If
//               the selected channel is not valid, 0xffffffff will be returned.
//
//******************************************************************************
DWORD AdcReadChannel( BYTE ChnlSelect );


#endif /* ADC_H_ */
