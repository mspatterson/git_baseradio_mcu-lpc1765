//******************************************************************************
//
//  CommRF1Interface.h:
//
//      Copyright (c) 2013, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//  Version 1.1     DD      Feb. 27, 2013
//
//******************************************************************************

#ifndef COMM_RF1_H
#define COMM_RF1_H

#include "typedefs.h"


void InitCommRF1( void );
BOOL WriteToRF1Port( const BYTE* pBuff, WORD buffLen );
BOOL RF1PortSending( void );
void clearRF1RxBuff(void);
void UpdateRF1ComRxBuf( void );
BOOL IsRF1RxPacketReady(void);
WORD ReadRF1RxPacket(BYTE *pByteData, WORD bufSize);

#endif //COMM_RF1_H
