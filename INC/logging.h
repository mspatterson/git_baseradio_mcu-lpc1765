//---------------------------------------------------------------------------
#ifndef loggingH
#define loggingH

    #include "typedefs.h"

    void InitLogging( void );

    BOOL WriteToDebugPort( const BYTE* pBuff, WORD buffLen );
    BOOL DebugPortSending( void );

#endif
