//******************************************************************************
//
//  CRC-CCITT.C
//
//      Copyright (c) 2006, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module implements a calculator for the CCITT 16-bit CRC
//
//  Date            Author      Comment
//  ---------------------------------------------------------------------------
//  Aug. 30, 2007    JK          Version 1v0
//******************************************************************************


#include "typedefs.h"







//******************************************************************************/
//
//  Function:    calcCCITTCRC
//
//  Arguments:    BYTE* - pointer to buffer for CRC calc;
//                WORD  - Length of the buffer
//
//  Returns:      Calculated CRC
//
//  Description:  Calculates the CCITTCRC
//
//******************************************************************************/
WORD calcCCITTCRC( BYTE* ptrBuffer, WORD wBuffLen );

