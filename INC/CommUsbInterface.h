//******************************************************************************
//
//  CommUsbInterface.h: 
//
//      Copyright (c) 2013, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//  Version 1.1     DD      Feb. 26, 2013
//
//******************************************************************************

#ifndef COMM_USB_H
#define COMM_USB_H

#include "typedefs.h"


void InitCommUsb( void );
BOOL WriteToUsbPort( const BYTE* pBuff, WORD buffLen );
BOOL UsbPortSending( void );
void clearUsbRxBuff(void);
void UpdateUsbComRxBuf( void );
BOOL IsUsbRxPacketReady(void);
WORD ReadUsbRxPacket(BYTE *pByteData, WORD bufSize);

#endif //COMM_USB_H
