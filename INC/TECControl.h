//******************************************************************************
//
//  TECControl.h
//
//      Copyright (c) 2006, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module implements a generic queue for bytes
//
//  Date            Author      Comment
//  ---------------------------------------------------------------------------
//  Jul. 13, 2012    JK          Version 1v0
//******************************************************************************


#ifndef TEC_CONTROL_H
#define TEC_CONTROL_H

#include "typedefs.h"
#include "hardwareAbstraction.h"
#include "cmdCommDefs.h"


//******************************************************************************
//TEC DAC SPI Interface
//******************************************************************************
#define OTEC_SP_VLIM       0x00
#define OTEC_SP_ILIMH      0x01
#define OTEC_SP_ILIMC      0x02
#define OTEC_SP_COLD_LIMIT 0x03
#define OTEC_SP_HOT_LIMIT  0x04

#define ITEC_SP_VLIM       0x05
#define ITEC_SP_ILIMH      0x06
#define ITEC_SP_ILIMC      0x07
#define ITEC_SP_IN2P       0x08

#define LTEC_SP_VLIM       0x09
#define LTEC_SP_ILIMH      0x0A
#define LTEC_SP_ILIMC      0x0B
#define LTEC_SP_IN2P       0x0C




//******************************************************************************
//
//******************************************************************************
void setTECSetPoint( BYTE byDACAndChan, WORD wThePoint );




//******************************************************************************
//
//******************************************************************************
void initTECs( LD_OP_PARAMS* ptrLdSysOpParams );



#endif






