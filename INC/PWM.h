//******************************************************************************
//
//  PWM.h: LPC PWM Control Module
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module provides control of the LPC internal PWM hardware.  Currently
//  supports only single edge controlled PWMs with no interrupt support.
//
//******************************************************************************

#ifndef PWM_H_
#define PWM_H_


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "typedefs.h"



//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//  TYPEDEF DECLARATIONS
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS PROTOTYPES
//------------------------------------------------------------------------------


//******************************************************************************
//
//  Function: PwmInit
//
//  Arguments: PwmChnlEnables - b0:5 set corresponding bits to enable PWM chnl 1-6
//             PwmCounterFrequency - desired counter frequency for PWM in Hz
//                                   the minimum pulse length is 1/PwmCounterFrequency
//             CounterRstValue - Value at which PWM counter is reset, this determines
//                               the PWM period as: CounterRstValue/PwmCounterFrequency
//                               (counter starts from 1)
//
//  Returns: TRUE if successful, else FALSE
//
//  Description: This function will configure and enable the selected PWM channels.
//
//
//******************************************************************************
BOOL PwmInit( WORD PwmChnlEnables, DWORD PwmCounterFrequency, DWORD CounterRstValue );


//******************************************************************************
//
//  Function: PwmChannelSet
//
//  Arguments: PwmChannel - channel to update (1-6)
//             PwmDutyCount - sets the duty cycle to be equal to:
//                            PwmDutyCount/CounterRstValue (CounterRstValue as set in PwmInit)
//
//  Returns: void
//
//  Description: This function will update the duty cycle setting for the selected
//               PWM channel.  Note that the PWM outputs are active high by default
//               so set PwmDutyCount to (CounterRstValue - DesiredValue) for an
//               active low equivalent output.
//
//******************************************************************************
void PwmChannelSet( BYTE PwmChannel, DWORD PwmDutyCount );


#endif /* PWM_H_ */
