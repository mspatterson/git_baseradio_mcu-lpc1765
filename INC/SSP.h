//******************************************************************************
//
//  SSP.h: LPC17xx SSP Peripheral Module
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module provides configuration and control of the internal SSP peripheral.
//  Currently only supports master mode.  This does not cover the SSP peripheral
//  so there is no FIFO for receive or transmit.
//
//******************************************************************************

#ifndef SSP_H_
#define SSP_H_


//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#include "typedefs.h"


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------

// PinEnables bit defines for SSSPnit()
#define SSP_SCK_EN_BIT      (1<<0)
#define SSP_MISO_EN_BIT     (1<<1)
#define SSP_MOSI_EN_BIT     (1<<2)
#define SSP_SSEL_EN_BIT     (1<<3)


// Control Reg Defines for SSSPnit()
#define SSP_FORMAT_SPI            0x00
#define SSP_FORMAT_TI             0x01
#define SSP_FORMAT_MICRO_WIRE     0x02

#define SSP_CPOL_INTERFRAME_LOW   0x00
#define SSP_CPOL_INTERFRAME_HIGH  0x01


#define SSP_CPHA_1ST_EDGE         0x00
#define SSP_CPHA_2ND_EDGE         0x01

#define SSP_LOOPBACK_ENABLE       0x00000001
#define SSP_IS_SLAVE              0x00000004
#define SSP_SLAVE_OUT_DISABLE     0x00000008




//------------------------------------------------------------------------------
//  TYPEDEF DECLARATIONS
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS PROTOTYPES
//------------------------------------------------------------------------------


//******************************************************************************
//
//  Function: SSPInit
//
//  Arguments:  byDataSize              - integer: 4 - 16 bits
//              byFrameFormat           - SSP_FORMAT_SPI;SSP_FORMAT_TI;SSP_FORMAT_MICRO_WIRE
//              byClockOutPolarity      - SSP_CPOL_INTERFRAME_LOW;SSP_CPOL_INTERFRAME_HIGH
//		        BYTE byClockOutPhase    - SSP_CPHA_1ST_EDGE; SSP_CPHA_2ND_EDGE
//              BOOL bLoopBackEnable    - set 'TRUE' to enable loopback;
//              BOOL bSSPIsMaster       - set 'TRUE' for Master; 'FALSE' for slave
//              BOOL bSlaveOutDisable   - set 'TRUE' to disable slave output (MISO)
//              DWORD dwInterruptEnable - TBD - not currently enabled
//
//             SckFreq - desired SCK frequency in Hz
//                       (max is pclk/2, min is pclk/254 )
//             PinEnables - enables for each SPI pin (normally SCLK will always
//                          be enabled, but depending on usage others may not)
//                          b0 set to enable SCLK pin (SPI_SCK_EN_BIT)
//                          b1 set to enable MISO pin (SPI_MISO_EN_BIT)
//                          b2 set to enable MOSI pin (SPI_MOSI_EN_BIT)
//                          b3 set to enable SSEL pin (SPI_SSEL_EN_BIT)
//             IntPriority - SPI interrupt priority, 0 = highest, 31 = lowest
//             IsrCallBack - function pointer to SPI ISR callback function (use NULL for none)
//                           (newly received data is passed to this function)
//
//  Returns: TRUE if initialization successful, else FALSE
//
//  Description: This function will initialize the SPI port according to the
//               passed parameters.  Note that if SSEL can be left disabled if
//               not used, or if there are multiple slaves on bus.  In that case
//               the calling function will need to manually control the GPIO
//               driving each SSEL.
//
//******************************************************************************
BOOL SSPInit( BYTE bySSPPort,
		      BYTE byDataSize,
              BYTE byFrameFormat,
              BYTE byClockOutPolarity,
		      BYTE byClockOutPhase,
              BOOL bLoopBackEnable,
              BOOL bSSPIsMaster,
              BOOL bSlaveOutDisable,
		      DWORD dwInterruptEnable,
		      DWORD SckFreq,
		      BYTE PinEnables,
		      BYTE IntPriority,
		      void (*IsrCallBack)( WORD SpiData ) );


//******************************************************************************
//
//  Function: SSPWriteData
//
//  Arguments:BYTE byPortNum - the SSP Port being selected
//                 SSPData - data to be written to SSP data register
//
//  Returns: void
//
//  Description: This function will write to the SSP data register, resulting in
//               the data being transmitted on the SSP bus.  This function must
//               not be called during an active SSP transfer, or the current
//               transfer will be corrupted.
//
//******************************************************************************
void SSPWriteData( BYTE bySSPPort, WORD SSPData );


//******************************************************************************
//
//  Function: SSPTransferDone
//
//  Arguments:BYTE byPortNum - the SSP Port being selected
//
//  Returns:  TRUE if current SSP transfer is complete, else FALSE
//
//  Description: This function will check the transfer complete status of the
//               SSP bus.  SSPReadData or SSPWriteData must be called in order
//               to clear the transfer done status.
//
//******************************************************************************
BOOL SSPTransferDone( BYTE bySSPPort );


//******************************************************************************
//
//  Function: SSPReadData
//
//  Arguments:BYTE byPortNum - the SSP Port being selected
//
//  Returns:  Data read from SSP data register
//
//  Description: This function will read the current data in the SSP data register.
//               This will be the SSP receive data if a SSP transfer is complete.
//               This function should not be called during an active SSP transfer.
//
//******************************************************************************
WORD SSPReadData( BYTE bySSPPort );



//******************************************************************************
//
//  Function: SSPEnable
//
//  Arguments: BYTE byPortNum - the SSP Port being selected
//
//  Returns: void
//
//  Description: This function enables the port for tx/rx capability; this function
//               should be called after 'SSPInit(...);' above, and before
//               attempting to send or receive data on the port.
//
//******************************************************************************
void SSPEnable( BYTE byPortNum );



//******************************************************************************
//
//  Function: SSPSendAndReceive
//
//  Arguments: BYTE bySSPPort    - the SSP Port
//             BYTE* ptrSendData - the send data buffer
//             BYTE* ptrRxBuffer - the receive data buffer
//             WORD wTransSize   - the transaction size
//
//  Returns:   None
//
//  Description: This function will send the transmit buffer (of size wTransSize)
//               and while that send is ongoing it will receive a buffer (again
//               of wTransSize).
//
//******************************************************************************
void SSPSendAndReceive( BYTE bySSPPort, BYTE* ptrSendData, BYTE* ptrRxBuffer, WORD wTransSize );



#endif /* SSP_H_ */
