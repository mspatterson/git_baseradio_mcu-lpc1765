//******************************************************************************
//
//  typedef.h: Module Title
//
//      Copyright (c) 2001-2008, Aeromechanical Services Ltd.
//      ALL RIGHTS RESERVED
//
//  This module specifies the type definitions for the AFIRS firmware
//  project 
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2008-Nov-30     ZJ          Code Review
//  2009-Jun-26     JK          Code Review
//
//******************************************************************************


#ifndef _TYPEDEFS_H
#define _TYPEDEFS_H 

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

#if defined(WIN32) || defined(__BORLANDC__)
    // We need to includes <windows.h> in the Windows/Borland world
    #ifndef _WINDOWS_
        #include <windows.h>
    #endif
#endif

//------------------------------------------------------------------------------
//  TYPEDEF DECLARATIONS
//------------------------------------------------------------------------------


#ifndef TRUE
    #define TRUE   1
#endif

#ifndef FALSE
    #define FALSE  0
#endif

#undef NULL
#define NULL   0

// Under Windows, BOOL type is already defined, so only define it for the embedded environment.
#if !defined(WIN32) && !defined(__BORLANDC__)
    #ifndef BOOL
        typedef unsigned char    BOOL;
    #endif
#endif

#ifndef BYTE
	typedef unsigned char   BYTE;
	typedef BYTE *          PBYTE;
#endif

#ifndef SBYTE
	typedef char			SBYTE;
	typedef SBYTE *			PSBYTE;
#endif
                                 
#ifndef WORD
    typedef unsigned short  WORD;
    typedef WORD *          PWORD;
#endif

#ifndef SWORD
    typedef signed short    SWORD;
    typedef SWORD *         PSWORD;
#endif

#ifndef DWORD
    typedef unsigned long   DWORD;
    typedef DWORD *         PDWORD;
#endif

#ifndef SDWORD
    typedef signed long     SDWORD;
    typedef SDWORD *        PSDWORD;
#endif


#ifndef HOTBOOL
    typedef volatile BOOL   HOTBOOL;
#endif

#ifndef HOTBYTE
    typedef volatile BYTE   HOTBYTE;
#endif

#ifndef HOTWORD
    typedef volatile WORD   HOTWORD;
#endif

#ifndef HOTDWORD
    typedef volatile DWORD  HOTDWORD;
#endif


// When working with DWORDs and WORDs, the following union
// facilitates conversion to / from bytes.

typedef union 
{
    DWORD dwData;
    WORD  wData[2];
    BYTE  byData[4];
} DWORD_BUFF;

typedef union 
{
    WORD  wData;
    BYTE  byData[2];
} WORD_BUFF;


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------

// Some handy macros for the date conversion work...

#ifndef LOBYTE
    #define LOBYTE(w)       ((BYTE)(w))
#endif

#ifndef HIBYTE
    #define HIBYTE(w)       ((BYTE)(((WORD)(w) >> 8) & 0xFF))
#endif

#ifndef LOWORD
    #define LOWORD(l)       ((WORD)(l))
#endif

#ifndef HIWORD
    #define HIWORD(l)       ((WORD)((((DWORD)(l)) >> 16) & 0xFFFF))
#endif


#define BIT_SIZEOF_BYTE 8


#endif   // _TYPEDEFS_H

