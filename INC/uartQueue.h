//******************************************************************************
//
//  UartQueue.h
//
//      Copyright (c) 2006, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This module implements a fixed-size bytequeue intended for the exclusive
//  use of the 'uartManager.c' module
//
//  Date            Author      Comment
//  ---------------------------------------------------------------------------
//  Feb. 1, 2011    JK          Version 1v0
//******************************************************************************



#ifndef _UART_QUEUE_H
    #define _UART_QUEUE_H  1
    
    #include "typedefs.h"
    #include "buildOpts.h"

    





    typedef struct {
                    WORD wReadIndex;
                    WORD wWriteIndex;
                    BYTE theData[UART_Q_SIZE + 1];               // extra space for Q handling overhead
                    BYTE boolQOverFlow;
                    BYTE boolQOverFlowPending;
    } UART_Q;

    void initUartQ( UART_Q* theQ );
    BYTE Add2UartQ( BYTE newData, UART_Q* theQ );
    BYTE blockWriteToUartQ( UART_Q* theQ, BYTE* ptrByteBuffer, WORD wAmountToAdd );
    BYTE ReadUartQ( UART_Q* theQ );
    BYTE PeekAtUartQ( UART_Q* theQ );
    BYTE UartQHasData( UART_Q* theQ );
    BYTE GetReadIndexUartQ( UART_Q* theQ );
    void ResetReadIndexUartQ(BYTE byNewReadIndex, UART_Q* theQ );
    BYTE getUartQOverFlowStatus( UART_Q* theQ );






#endif
