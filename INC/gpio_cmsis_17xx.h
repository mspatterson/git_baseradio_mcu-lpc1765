//******************************************************************************
//
//  GPIO_CMSIS.h: NXP ARM Cortex M3 GPIO Macros
//
//      Copyright (c) 2011, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This header file contains macros to simplify handling of GPIO on LPC Arm
//  micros as illustrated below:
//
//      // use a single define for each pin
//      #define LED0    0,8     // format: port,bit
//      #define BUTTON0 2,4
//
//      // use PIN_DIR_OUT( pin ) and PIN_DIR_IN( pin ) for pin direction control
//      PIN_DIR_OUT( LED0 );
//      PIN_DIR_IN( BUTTON0 );
//
// **NOTE** these macros do not configure the IOCON to GPIO mode - this must 
//          done if pins do not default to GPIO (not all do)
//
//      // use PIN_STATUS( pin ) to read pin status
//      // use PIN_SET( pin ) and PIN_CLR( pin ) to control output
//      if( PIN_STATUS( BUTTON0 ) )
//          PIN_SET( LED0 );
//      else
//          PIN_CLR( LED0 );
//
//  Version 1.0     JD      Apr 2011
//
//******************************************************************************


#ifndef _GPIO_CMSIS_H

    #define _GPIO_CMSIS_H

//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------


#define GPIO_SET( PORT, BIT )       LPC_GPIO##PORT->FIOSET = ( 1 << (BIT) )
#define GPIO_CLR( PORT, BIT )       LPC_GPIO##PORT->FIOCLR = ( 1 << (BIT) )
#define GPIO_STATUS( PORT, BIT )    ((LPC_GPIO##PORT->FIOPIN & ( 1 << (BIT) )) != 0)
#define GPIO_DIR_OUT( PORT, BIT )   LPC_GPIO##PORT->FIODIR |= ( 1 << (BIT) )
#define GPIO_DIR_IN( PORT, BIT )    LPC_GPIO##PORT->FIODIR &= ~( 1 << (BIT) )


#define PIN_SET( PIN )              GPIO_SET( PIN )
#define PIN_CLR( PIN )              GPIO_CLR( PIN )
#define PIN_STATUS( PIN )           GPIO_STATUS( PIN )
#define PIN_DIR_OUT( PIN  )         GPIO_DIR_OUT( PIN )
#define PIN_DIR_IN( PIN  )          GPIO_DIR_IN( PIN )


#endif // _GPIO_CMSIS_H
